/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 12 Jun 2012, 15:49:53
 */
package com.microbasecloud.webui.plugins.summary;

import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebuiPlugin;
import com.vaadin.ui.Component;

/**
 * 
 * @author Keith Flanagan
 */
public class SystemSummaryPlugin 
    implements WebuiPlugin {
  private Configuration config;

  @Override
  public void setConfiguration(Configuration config) {
    this.config = config;
  }

  @Override
  public Component getMenuComponent() {
    return new SystemSummaryMenuSection(config);
  }

  @Override
  public String getMenuComponentTitle() {
    return "System summary";
  }

}
