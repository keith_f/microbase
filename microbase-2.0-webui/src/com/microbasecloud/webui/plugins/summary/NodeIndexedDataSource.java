/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 10 Jun 2012, 22:10:26
 */
package com.microbasecloud.webui.plugins.summary;

import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.runtime.ClientRuntime;

import com.microbasecloud.webui.Configuration;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;

/**
 * 
 * @author Keith Flanagan
 */
public class NodeIndexedDataSource
    extends IndexedContainer {

  private static final String COL_HOSTNAME = "Hostname";
  private final Configuration config;
  
  public NodeIndexedDataSource(Configuration config) {
    this.config = config;
    addContainerProperty(COL_HOSTNAME, String.class, null);
    addContainerProperty("Foo", String.class, null);
    addContainerProperty("Bar", Integer.class, null);

    refresh();
  }

//  public void refresh() {
//    for (int i = 0; i < 500; i++) {
//      Item item = addItem(i);
//      item.getItemProperty(COL_HOSTNAME).setValue(
//          "hostname-" + System.currentTimeMillis());
//      item.getItemProperty("Foo").setValue("blah");
//      item.getItemProperty("Bar").setValue(i);
//    }
//
//    sort(new Object[] { COL_HOSTNAME }, new boolean[] { true });
//  }

  public void refresh() {
    for (int i = 0; i < 500; i++) {
      Item item = addItem(i);
      item.getItemProperty(COL_HOSTNAME).setValue(
          "hostname-" + System.currentTimeMillis());
      item.getItemProperty("Foo").setValue("blah");
      item.getItemProperty("Bar").setValue(i);
    }

    sort(new Object[] { COL_HOSTNAME }, new boolean[] { true });
  }
  
  //	public void ref

  //	private static void fillIso3166Container(IndexedContainer container) {
  //    container.addContainerProperty(iso3166_PROPERTY_NAME, String.class,
  //            null);
  //    container.addContainerProperty(iso3166_PROPERTY_SHORT, String.class,
  //            null);
  //    container.addContainerProperty(iso3166_PROPERTY_FLAG, Resource.class,
  //            null);
  //    for (int i = 0; i < iso3166.length; i++) {
  //        String name = iso3166[i++];
  //        String id = iso3166[i];
  //        Item item = container.addItem(id);
  //        item.getItemProperty(iso3166_PROPERTY_NAME).setValue(name);
  //        item.getItemProperty(iso3166_PROPERTY_SHORT).setValue(id);
  //        item.getItemProperty(iso3166_PROPERTY_FLAG).setValue(
  //                new ThemeResource("../sampler/flags/" + id.toLowerCase()
  //                        + ".gif"));
  //    }
  //    container.sort(new Object[] { iso3166_PROPERTY_NAME },
  //            new boolean[] { true });
  //}
}
