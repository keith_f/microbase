/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 14 Jun 2012, 12:00:28
 */
package com.microbasecloud.webui.plugins.notification;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import uk.org.microbase.notification.spi.MicrobaseNotification;

import com.microbasecloud.webui.Configurable;
import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebUiException;
import com.vaadin.ui.Table;

/**
 * 
 * @author Keith Flanagan
 */
public class MessageListTable
  extends Table
  implements Configurable {
  /**
   * Load message content from the database as well as metadata
   */
  private static final boolean LOAD_CONTENT = false;
  /**
   * Expand tree nodes by default when they are added
   */
  private static final boolean ON_ADD_EXPAND = false;

  private Configuration config;
  private MicrobaseNotification notification;

  private final Map<String, Object> msgIdToTableId;

  public MessageListTable()
	{
	  this.msgIdToTableId = new HashMap<String, Object>();

    addContainerProperty(MessageListIndexedDataSource.COL_PUBLISHED_DATE, Date.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_TOPIC, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_GUID, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_PARENT, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_PUBLISHER, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_WORKFLOW_EXE, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_WORKFLOW_STEP, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_USER_DESCR, String.class,  null);
      
    setSelectable(true);
    setMultiSelect(true);
    setColumnCollapsingAllowed(true);
    setColumnReorderingAllowed(true);
    
    setSizeFull();
    
	}

  public void setConfiguration(Configuration config) throws WebUiException {
    this.config = config;
    this.notification = config.getRuntime().getNotification();

    setContainerDataSource(new MessageListIndexedDataSource(config));
  }


}
