/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 20 Jun 2012, 14:48:34
 */
package com.microbasecloud.webui.plugins.notification;

import java.util.List;

import uk.org.microbase.dist.responder.ResponderInfo;

import com.microbasecloud.webui.Configurable;
import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebUiException;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.ListSelect;

/**
 * An implementation of ListSelect that displays a list of currently 
 * installed responder ids.
 * 
 * @author Keith Flanagan
 */
public class ResponderListSelect
    extends ListSelect
    implements Configurable, Property.ValueChangeListener  { //, Property.ValueChangeListener {
	
  
  private Configuration config;
  
  public ResponderListSelect() {
    super("Installed responders");
    setNullSelectionAllowed(false);
    setMultiSelect(false);
    setImmediate(true); // send the change to the server at once
    
    addListener(new ValueChangeListener() {
      @Override
      public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
        Object val = event.getProperty().getValue();
        getWindow().showNotification("Property ValueChangeEvent Selected responder: " + val);
      }
    });

  }



  @Override
  public void setConfiguration(Configuration config) throws WebUiException {
    this.config = config;
    refreshItems();
  }
  
  public void refreshItems()
  {
    removeAllItems();
    
    try
    {
      List<ResponderInfo> responders = 
          config.getRuntime().getRegistration().getRegisteredResponders();
      for (ResponderInfo info : responders)
      {
        addItem(info);
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      config.getWebui().showException("Failed to refresh responder list", e);
    }
    
  }
  
  public void addItem(ResponderInfo info)
  {
    String responderId = info.getResponderId();
    Item item = addItem(responderId);    
  }

}
