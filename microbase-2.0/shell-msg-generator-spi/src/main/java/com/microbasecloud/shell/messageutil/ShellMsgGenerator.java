/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.shell.messageutil;

import java.util.Map;

/**
 * An SPI used by the Microbase Shell project in order to ease the construction
 * of notification messages. 
 * 
 * Responder developers can optionally provide an implementation of this
 * interface as part of their responder project. If there is an appropriate
 * <code>ShellMsgGenerator</code> implementation provided by a responder 
 * project, then the Microbase Shell will discover it at runtime. These
 * classes allow users to generate example/test messages on the command line and
 * have them serialised to a JSON text format, ready for publication to the
 * notification system.
 * 
 * @author Keith Flanagan
 */
public interface ShellMsgGenerator<T>
{
  /**
   * Returns the classname for the <code>MessageProcessor</code> implementation
   * that this generator is capable of generating message content for. 
   * @return 
   */
  public String getMessageProcessorClassname();
  
  /**
   * Returns a Map of name -> description for each property name required in
   * order to generate message content. The properties specified here must be
   * present in the map passed to <code>generateJson</code> or <code>generateMsgBean</code>.
   * 
   * @return a set of property names and human readable descriptions. The 
   * property names are the names expected by the <code>generateJson</code> and
   * <code>generateMsgBean</code> methods.
   */
  public Map<String, String> getReqPropertyNames();
  
  /**
   * Takes a set of parameters and generates a job message bean. The bean is
   * then serialised to JSON.
   * @return a JSON serialisation of the notification message content bean.
   */
  public String generateJson(Map<String, String> properties)
          throws ShellMsgGeneratorException;
  
  /**
   * Takes a set of parameters and generates a job message bean.
   * @param properties
   * @return 
   */
  public T generateMsgBean(Map<String, String> properties)
          throws ShellMsgGeneratorException;
}
