/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package}.microbase.workshop;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;
import uk.org.microbase.util.file.zip.ZipUtils;

/**
 * This class implements a simple fully-functional Microbase responder capable
 * of executing generic command lines. The command lines and their requirements
 * (input files, etc) should be encoded in a notification message. This 
 * responder then obtains the necessary input files from the notification system,
 * executes the command line, and uploads any specified output files.
 * See the full tutorial for more details:
 * http://www.microbasecloud.com/developer-documentation/getting-started
 * 
 * Alternatively, this responder can be used as a superclass for a more 
 * specific responder implementation. If you subclass this responder, be sure to
 * change (at the minimum) the responder ID, and incoming/outgoing topic names.
 * 
 * @author Keith Flanagan
 */
public class BlastResponder
    extends AbstractMessageProcessor
{
  private static final String TOPIC_IN = "PAIRWISE-BLAST-JOB";
  private static final String TOPIC_OUT = "PAIRWISE-BLAST-COMPLETED";
  
  /**
   * An array of two FileMetaData objects that represent the two input files
   * to be compared using BLAST.
   */
  private FileMetaData[] pair;
  
  /*
   * The remote filesystem location that will be used to upload the output files
   */
  private FileMetaData blastReportS3File;
  private String logFileS3Bucket;
  private String logFileS3Path;
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(300);
    info.setMaxLocalCores(50);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(BlastResponder.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(TOPIC_IN);
    return info;
  }


  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    System.out.println("Doing preRun");
    pair = parseMessage(message);
    
    /*
     * Set the remote S3 location of where output file from this responder
     * will be uploaded to.
     */
    String remoteS3Bucket = pair[0].getBucket();
    String remoteS3Path = "pairwise-blasts/"
                              +pair[0].getName()+"-vs-"+pair[1].getName();
    String remoteS3Name = pair[0].getName()+"-"+pair[1].getName()+".blast";
    blastReportS3File = new FileMetaData(
        remoteS3Bucket, remoteS3Path, remoteS3Name);
    
    logFileS3Bucket = remoteS3Bucket;
    logFileS3Path = remoteS3Path;
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    System.out.println("Doing cleanup");
  }
  
  @Override
  public Set<Message> processMessage(Message message)
      throws ProcessingException
  {
    System.out.println("Performing computational work");
    try
    {
      File subjectFile = downloadInputFile(pair[0]);
      File queryFile = downloadInputFile(pair[1]);
      
      File blastResult = runBlast(subjectFile, queryFile);
      uploadBlastReport(blastResult);
      
      Message blastCompleteMsg = generateSuccessNotification(message);
      
      Set<Message> messages = new HashSet<Message>();
      messages.add(blastCompleteMsg);
      // ... we could put additional messages here if necessary ...

      return messages;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run one or more commands: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
  }
  
  private FileMetaData[] parseMessage(Message m)
      throws ProcessingException
  {
    try
    {
      FileMetaData subjectFile = new FileMetaData(
          m.getContent().get("subject_bucket"),
          m.getContent().get("subject_path"),
          m.getContent().get("subject_filename"));
      FileMetaData queryFile = new FileMetaData(
          m.getContent().get("target_bucket"),
          m.getContent().get("target_path"),
          m.getContent().get("target_filename"));
      
      return new FileMetaData[] {subjectFile, queryFile};
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to parse message", e);
    }
  }
  
  private File downloadInputFile(FileMetaData remoteFile)
      throws ProcessingException
  {
    try
    {
      // Get a reference to the temporary directory for this job
      File tmpDir = getWorkingDirectory();
      
      //Download GBK file
      File destinationFile = new File(tmpDir, remoteFile.getName());
      
      getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
          remoteFile.getBucket(), remoteFile.getPath(), remoteFile.getName(), 
          destinationFile, true);
      
      return destinationFile;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to download remote file: " 
          + remoteFile.getBucket()+", "
          + remoteFile.getPath() + ", "
          + remoteFile.getName(), e);
    }
  }
  
  
  private File runBlast(File subjectFile, File queryFile)
      throws ProcessingException
  {
    try
    {
      String[] fastaCommand = new String[]
      { "formatdb", "-p", "F", "-i", subjectFile.getAbsolutePath() };
      executeCommand(fastaCommand, "formatdb");

      File blastResultFile = new File(getWorkingDirectory(), blastReportS3File.getName());
      String[] blastCommand = new String[]
      { "blastall",  //The BLAST executable file (must be on PATH)
            "-p", "blastn", //Specifies nucleotide BLAST
            "-m", "8",  // Specifies that BLAST creates output in tabular format
            "-i", queryFile.getAbsolutePath(),   //FASTA-format query sequences
            "-d", subjectFile.getAbsolutePath(), //Database
            "-o", blastResultFile.getAbsolutePath()}; //Output file
      executeCommand(blastCommand, "blast");
   
      return blastResultFile;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to execute BLAST", e);
    }
  }
  
  private void executeCommand(String[] command, String stdOutPrefix)
      throws ProcessingException
  {
    File workDir = getWorkingDirectory();
    File stdOutFile = new File(workDir, stdOutPrefix+"-stdout.txt");
    File stdErrFile = new File(workDir, stdOutPrefix+"-stderr.txt");
    try {
      FileWriter stdOut = new FileWriter(stdOutFile);
      FileWriter stdErr = new FileWriter(stdErrFile);

      int exitStatus = NativeCommandExecutor.executeNativeCommand(
          workDir, command, stdOut, stdErr);

      stdOut.flush();
      stdOut.close();
      stdErr.flush();
      stdErr.close();

      if (exitStatus != 0) {
        throw new ProcessingException("Exit status of the command: "
            + Arrays.asList(command)
            + " \nwas "+exitStatus
            + ". Assuming that non-zero means that execution failed.");
      }
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to execute command: "+command[0], e);
    }
    finally {
      try {
        getRuntime().getMicrobaseFS().upload(
            stdOutFile, logFileS3Bucket, logFileS3Path, stdOutFile.getName(), null);
        getRuntime().getMicrobaseFS().upload(
            stdErrFile, logFileS3Bucket, logFileS3Path, stdErrFile.getName(), null);
      }
      catch(Exception e) {
        throw new ProcessingException(
            "Failed to upload STDOUT/STDERR content", e);
      }
    }
  }
  
  private void uploadBlastReport(File blastResultLocalFile)
      throws ProcessingException
  {
    try {
      getRuntime().getMicrobaseFS().upload(
          blastResultLocalFile, 
          blastReportS3File.getBucket(), blastReportS3File.getPath(), 
          blastReportS3File.getName(), null);
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to upload output file ", e);
    }
  }
  
  private Message generateSuccessNotification(Message parent)
  {
    Message successMsg = createMessage(
        parent,                 //The message to use as a parent
        TOPIC_OUT,              //The topic of the new message
        parent.getWorkflowStepId(),   //The workflow step ID
        //An optional human-readable description
        "Successfully completed a pairwise BLAST-N between sequences: "
        + pair[0].getName() + " and " + pair[1].getName());
    
    successMsg.getContent().put("subject_bucket", pair[0].getBucket());
    successMsg.getContent().put("subject_path", pair[0].getPath());
    successMsg.getContent().put("subject_filename", pair[0].getName());

    successMsg.getContent().put("target_bucket", pair[1].getBucket());
    successMsg.getContent().put("target_path", pair[1].getPath());
    successMsg.getContent().put("target_filename", pair[1].getName());
    
    successMsg.getContent().put("blast_result_bucket", blastReportS3File.getBucket());
    successMsg.getContent().put("blast_result_path", blastReportS3File.getPath());
    successMsg.getContent().put("blast_result_filename", blastReportS3File.getName());
    
    return successMsg;
  }
  
  
}
