/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.notification.spi;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.data.Message.State;
import uk.org.microbase.notification.data.MessageStateInfo;

/**
 * A set of Notification System operations that can be guaranteed to
 * execute atomically. These operations are composed of one or more operations
 * defined in <code>MicrobaseNotification</code>.
 *
 * This interface can be used to as the basis for implementations where it is
 * not desirable to expose transactions / locks to the caller, eg, generic
 * clients that should not need to know detail implementation details, or
 * Web Service clients.
 *
 * @author Keith Flanagan
 */
public interface MicrobaseNotification
{
  /**
   * Publishes a new Message to the notification system. This method will
   * create a Message object to hold all the provided information. A unique ID 
   * will be assigned to the Message object before it is published to the 
   * notification system database. The current server time will also be
   * used as the publication date.
   * 
   * @param publisherGuid the ID of the publisher to report to the system (optional).
   * @param topicGuid the topic ID to set for this message. Topic IDs are used
   * to determine which responders should receive which message types.
   * @param content a set of key -> value pairs that make up this message's
   * content (may be empty set).
   * @param binaryContent allows a binary blob to be set as the message's
   * content (optional).
   * @throws NotificationException 
   */
  public void publish(
      String publisherGuid, String topicGuid,
      Map<String, String> content,
      byte[] binaryContent)
      throws NotificationException;


  /**
   * Publishes a new Message to the notification system. This method allows you
   * to pass a Message object for publication. The majority of the Message
   * object content is published to the notification system unmodified. 
   * Note that two properties of the Message object get special treatment:
   * <ul>
   * <li>guid - If you specify your own guid field on the Message object passed
   * to this method, then it will be used with no modifications. In this case,
   * you <b>must</b> ensure that the guid is truely unique, otherwise the 
   * publication of the message will fail. Alternatively, if the guid field on
   * the Message object passed is NULL, then a unique identifier will be 
   * generated and set for you by the notification system.</li>
   * <li>publishedTimestamp - This property will be overwritten on the Message
   * object passed to this method, regardless of its current value. 
   * The publishedTimestamp field will be set to the current time on 
   * the server.</li>
   * </ul>
   * 
   * @param message
   * @throws NotificationException 
   */
  public void publish(Message message)
      throws NotificationException;

  /**
   * Publishes a collection of Message objects to the notification system.
   * The semantics for this method are identical to
   * <code>publish(Message message)</code>. The entire collection of messages
   * are published atomically. If there is a problem with the publication of
   * one of them, then all messages in the collection are rolled back.
   * @param messages
   * @throws NotificationException 
   */
  public void publish(Collection<Message> messages)
      throws NotificationException;

  /**
   * Publishes a set of messages, <code>messages</code>, while also setting the
   * state of a specific message <code>messageGuid</code> to <code>newState</code>.
   * Both the publication of <code>messages</code> and the state change for
   * <code>messageGuid</code> are performed atomically.
   * This method can be used to publish messages that result from a unit of
   * computation, while simultaneously setting the state of the message whose
   * computation has been performed to 'complete'.
   *
   * @param messages the message(s) to publish
   * @param messageGuid
   * @param responderGuid
   * @param newState
   * @throws NotificationException
   */
  public void publishAndSetState(Collection<Message> messages,
      String messageGuid, String responderGuid, Message.State newState)
      throws NotificationException;

  /**
   * Finds a block of messages in the specified state for the specified
   * responder.
   *
   * @param responderGuid the responder to return messages for.
   * @param state the state of messages to return
   * @param maxMessages the maximum number of messages to return
   *
   * @return a set of message IDs of messages in the READY state for the
   * specified responder
   *
   * @throws NotificationException
   */
  public Set<String> findMessagesInState(String responderGuid,
      Message.State state, int maxMessages)
      throws NotificationException;

  /**
   * Returns the specified message, whilst also setting its state
   * to <code>state</code>.
   *
   * @param messageGuid the message to return
   * @param responderGuid the responder whose message state is to be set
   * @param state the state to set it to
   * @return the specified message
   * @throws NotificationException
   */
  public Message getMessageAndSetState(String messageGuid, 
      String responderGuid, Message.State state)
      throws NotificationException;

  public Message getMessage(String messageGuid)
      throws NotificationException;

  public Map<String, Message> getMessages(Set<String> messageGuids)
      throws NotificationException;

  public void setMessageState(
      String messageGuid, String responderGuid, State newState)
      throws NotificationException;

  /**
   * Given a message ID and a responder ID, returns the current Message.State
   * for the message WRT the responder.
   * @param messageGuid the message ID to query.
   * @param responderGuid the responder ID.
   * @return the state of the message WRT the specified responder, or NULL if
   * no such state exists. 
   * @throws NotificationException 
   */
  public State getMessageState(
      String messageGuid, String responderGuid)
      throws NotificationException;
  
  /**
   * Given a responder ID, finds new messages of interest to it - i.e., messages
   * that have published since the last time this
   * method was called and currently have no status wrt to responder
   * <code>responderGuid</code>. Any new messages that are found are marked
   * as 'READY' for processing by the responder.
   * 
   * @param responderGuid the ID of the responder that new messages will be
   * found for.
   * @param topicGuid
   * @return the number of new messages found (and subsequently marked READY)
   * @throws NotificationException
   */
  public int prepareNewMessagesForResponder(
      String responderGuid, String topicGuid)
      throws NotificationException;

  public List<Message> listMessagesInTimestampOrder(
      int offset, int limit)
      throws NotificationException;

  public List<Message> listMessagesInTimestampOrder(
      String topicId, long afterTimestamp, int preferredLimit)
      throws NotificationException;

  public List<Message> listMessagesByTopic(
      String topicGuid, int offset, int limit)
      throws NotificationException;

  public List<Message> listMessagesByPublisher(
      String publisherId, int offset, int limit)
      throws NotificationException;

  public long count()
      throws NotificationException;

  public long countMessagesWithTopic(String topicGuid)
      throws NotificationException;

  
  /**
   * Lists all current message state entries by ascending timestamp. 
   * 
   * @param offset
   * @param limit
   * @return the current state for each message/responder pair
   * @throws NotificationException 
   */
  public List<MessageStateInfo> listCurrentMessageStates(int offset, int limit)
      throws NotificationException;
  
  /**
   * Lists the message state log in order for all messages and all responders.
   * @param afterTimestamp only messages after this timestamp are returned.
   * @param preferredLimit limit the number of messages retrieved
   * @return
   * @throws NotificationException 
   */
  public List<MessageStateInfo> listMessageStateLogEntriesInTimestampOrder(
      int offset, int preferredLimit)
      throws NotificationException;
  
  
  /**
   * Returns all messages with no parent - the initial root messages signaling
   * that start of the workflow for a particular data item.
   * 
   * @param offset
   * @param limit
   * @return 
   */
  public List<String> listRootMessageIds(int offset, int limit)
      throws NotificationException;
  
  public List<Message> listRootMessages(boolean includeContent, int offset, int limit)
      throws NotificationException;
  
  public int getRootMessageCount()
      throws NotificationException;
  
  /**
   * Returns the message IDs of the direct children of the specified parent 
   * message.
   * 
   * @param offset
   * @param limit
   * @return a list of child message IDs
   * @throws NotificationException 
   */
  public List<String> listChildMessageIds(String parentId, int offset, int limit)
      throws NotificationException;
  
  public List<Message> listChildMessages(String parentId, 
      boolean includeContent, int offset, int limit)
      throws NotificationException;
  
  /**
   * Returns the number of direct child messages for the specified parent ID.
   * @param parentId
   * @return
   * @throws NotificationException 
   */
  public int getChildMessageCount(String parentId)
      throws NotificationException;
  
  public List<Integer> getChildMessageCounts(List<String> parentIds)
      throws NotificationException;
}
