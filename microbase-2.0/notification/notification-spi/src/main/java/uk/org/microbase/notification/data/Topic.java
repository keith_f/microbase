/*
 * Copyright 2010 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.org.microbase.notification.data;

import java.io.Serializable;

/**
 * A Topic represents a type of message. Each Topic has a unique ID to
 * distinguish it. Optionally, a responder name and human-readable description
 * may be specified.
 *
 * A responder name may be specified in order to group topics together if they
 * are fired from the same responder. This name is entirely optional, but
 * maybe used by GUIs etc, in order to group topics together for display
 * purposes.
 *
 * @author Keith Flanagan
 */
public class Topic
    implements Serializable
{
  private String guid;
  private String responderName;
  private String description;

  public Topic()
  {
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public String getResponderName()
  {
    return responderName;
  }

  public void setResponderName(String responderName)
  {
    this.responderName = responderName;
  }
}
