/*
 * Copyright 2010 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.microbase.notification.db;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.util.UidGenerator;
import uk.org.microbase.util.db.DBException;

/**
 * Defines various 'canned' read-only queries over the set of all stored
 * messages.
 *
 * @author Keith Flanagan
 */
public class MessageDAO
//    implements Lockable
{
  private static final Logger logger =
      Logger.getLogger(MessageDAO.class.getName());
  private final ObjectMapper jsonMapper;


  private DistributedJdbcPool pool;

  public MessageDAO()
  {
    jsonMapper = new ObjectMapper();
  }

  
  public void setDbPool(DistributedJdbcPool dbPool)
  {
    this.pool = dbPool;
  }
  public DistributedJdbcPool getDbPool()
  {
    return pool;
  }

  public void save(Connection txn, Message bean)
      throws DBException
  {
    List<Message> beans = new LinkedList<Message>();
    beans.add(bean);
    save(txn, beans);
  }

  public void save(Connection txn, List<Message> beans)
      throws DBException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "INSERT INTO messages "
          + "(guid, published_timestamp, expires_timestamp,"
          + "topic_guid, publisher_guid, parent_msg_guid,"

          + "workflow_exe_id, workflow_step_id, user_desc,"

          + "content,"
          + "binary_content) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

      stmt = txn.prepareStatement(sql);

      for (Message msg : beans)
      {
        int col = 1;
        stmt.setString(col++, msg.getGuid());
        stmt.setTimestamp(col++, new Timestamp(msg.getPublishedTimestamp()));
        stmt.setTimestamp(col++, new Timestamp(msg.getExpiresTimestamp()));

        stmt.setString(col++, msg.getTopicGuid());
        stmt.setString(col++, msg.getPublisherGuid());
        stmt.setString(col++, msg.getParentMessage());

        stmt.setString(col++, msg.getWorkflowExeId());
        stmt.setString(col++, msg.getWorkflowStepId());
        stmt.setString(col++, msg.getUserDescription());

        //Content
        byte[] contentJson = jsonMapper.writeValueAsBytes(msg.getContent());
//        String[] contentKeys = new String[msg.getContent().size()];
//        String[] contentVals = new String[msg.getContent().size()];
//        Iterator<String> keyItr = msg.getContent().keySet().iterator();
//        for (int i=0; i<contentKeys.length; i++)
//        {
//          String key = keyItr.next();
//          contentKeys[i] = key;
//          contentVals[i] = msg.getContent().get(key);
//        }
//        Array contentKeysArr = txn.createArrayOf("varchar", contentKeys);
//        Array contentValsArr = txn.createArrayOf("text", contentVals);
//        stmt.setArray(col++, contentKeysArr);
//        stmt.setArray(col++, contentValsArr);
        
        stmt.setBytes(col++, contentJson);
        stmt.setBytes(col++, msg.getBinaryContent());

        stmt.addBatch();
//        stmt.executeUpdate();
      }
      stmt.executeBatch();

    }
    catch(IOException e)
    {
      throw new DBException("Failed to insert "+beans.size()+" messages due to "
          + "an internal JSON serialisation problem: "+e.getMessage(), e);
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new DBException("Failed to insert "+beans.size()+" messages", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  public Message publish(Connection txn,
      String publisherGuid, String topicGuid, 
      Map<String, String> content, byte[] binaryContent)
      throws DBException
  {
//    long expires = Long.MAX_VALUE;
    long now = System.currentTimeMillis();

    Message message = new Message();
    message.setContent(content);
    message.setBinaryContent(binaryContent);
//    message.setExpiresTimestamp(expires);
    message.setGuid(UidGenerator.generateUid());
    message.setPublishedTimestamp(now);
    message.setPublisherGuid(publisherGuid);
    message.setTopicGuid(topicGuid);
    

    try
    {
      save(txn, message);
      return message;
    }
    finally
    {
//      lock.releaseLock();
    }
  }

  public Message publish(Connection txn, Message message)
      throws DBException
  {
    //Generate a unique ID, if one is not already set
    if (message.getGuid() == null)
    {
      message.setGuid(UidGenerator.generateUid());
    }
    
    //We're publishing, rather than saving a message - so set current time
    long now = System.currentTimeMillis();
    message.setPublishedTimestamp(now);

    save(txn, message);
    return message;
  }

  public boolean exists(Connection txn, String messageGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM messages WHERE guid = ?");
      stmt.setString(1, messageGuid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute exists() for GUID: "+messageGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public Message getMessage(Connection txn, String guid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages WHERE guid = ?");
      stmt.setString(1, guid);
      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, true);
      if (msgs.size() == 1)
      {
        return msgs.get(0);
      }
      else if (msgs.isEmpty())
      {
        return null;
      }
      else
      {
        throw new SQLException(msgs.size()+" Message instances had "
            + "the same GUID: "+guid + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getByGuid() for: "+guid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public long count(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM messages");

      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute count()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public long countMessagesWithTopic(Connection txn, String topicGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM messages WHERE topic_guid = ?");
      stmt.setString(1, topicGuid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute count() for TopicGUID: "+topicGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  /**
   *
   * @return a list of messages in timestamp order
   */
  public List<Message> listMessagesByTopic(
      Connection txn,
      String topicGuid, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages WHERE topic_guid = ? LIMIT ? OFFSET ?");
      stmt.setString(1, topicGuid);
      stmt.setInt(2, limit);
      stmt.setInt(3, offset);

      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, true);
      return msgs;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listMessagesByTopic() for topic: "+topicGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public List<Message> listMessagesByPublisher(Connection txn,
      String publisherId, int offset, int limit) throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages WHERE publisher_guid = ? LIMIT ? OFFSET ?");
      stmt.setString(1, publisherId);
      stmt.setInt(2, limit);
      stmt.setInt(3, offset);

      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, true);
      return msgs;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listMessagesByPublisher() for topic: "+publisherId
          + " offset: "+offset+", limit: "+limit, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public List<Message> listMessagesInTimestampOrder(
      Connection txn, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages ORDER BY published_timestamp LIMIT ? OFFSET ?");
      stmt.setInt(1, limit);
      stmt.setInt(2, offset);

      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, true);
      return msgs;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listMessagesInTimestampOrder() for"
          + " offset: "+offset+", limit: "+limit, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  /**
   * Lists messages in ascending timestamp order from the millisecond after
   * <code>afterTimestamp</code>, for all messages of the specified
   * <code>topicId</code>.
   * This method is typically useful for responders for determining if any
   * messages are relevant to them since their last check.
   *
   * @param topicId the topic ID of interest
   * @param afterTimestamp returns messages with a timestamp greater than
   * <code>afterTimestamp</code>.
   * @param preferredLimit the maximum number of messages to return. In rare
   * cases, the actual number of messages might be <b>greater</b> than
   * <code>preferredLimit</code>. This might happen in the case where the
   * message return limit has been reached, but there are still more
   * messages with the <b>same</b> timestamp (i.e. the messages happened to be
   * published in the same millisecond). In this case, the additional messages
   * with the same timestamp will be returned as well in order to avoid 'losing'
   * messages in the next call to this query.
   * @return
   * @throws DBException
   */
  public List<Message> listMessagesInTimestampOrder(
      Connection txn,
      String topicId, long afterTimestamp, int preferredLimit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages WHERE published_timestamp > ?"
        + " ORDER BY published_timestamp LIMIT ?");
      stmt.setLong(1, afterTimestamp);
      stmt.setInt(2, preferredLimit);

      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, true);
      return msgs;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listMessagesInTimestampOrder() for:"
          + " afterTimestamp: "+afterTimestamp
          + ", limit: "+preferredLimit, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public List<String> listMessageIDsInTimestampOrder(
      Connection txn, String topicId, long afterTimestamp)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT guid FROM messages WHERE published_timestamp > ?"
        + " ORDER BY published_timestamp");
      stmt.setLong(1, afterTimestamp);

      ResultSet rs = stmt.executeQuery();
      List<String> ids = new ArrayList<String>();
      while(rs.next())
      {
        ids.add(rs.getString("guid"));
      }
      return ids;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listMessagesInTimestampOrder() for:"
          + " afterTimestamp: "+afterTimestamp, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  /**
   * Provides a list of all distinct topic IDs used in messages published to
   * the notification system.
   *
   * @return a list of all topic names
   * @throws DBException
   */
  public List<String> listTopicGuids(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT DISTINCT(topic_guid) FROM messages");

      ResultSet rs = stmt.executeQuery();
      List<String> ids = new ArrayList<String>();
      while(rs.next())
      {
        ids.add(rs.getString("topic_guid"));
      }
      return ids;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listTopicGuids()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public List<String> listRootMessageIds(Connection txn, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT guid FROM messages "
          + "WHERE parent_msg_guid is null "
          + "ORDER BY published_timestamp "
          + "LIMIT ? OFFSET ?");
      int col = 1;
      stmt.setInt(col++, limit);
      stmt.setInt(col++, offset);
      
      ResultSet rs = stmt.executeQuery();
      List<String> ids = new ArrayList<String>();
      while(rs.next())
      {
        ids.add(rs.getString("guid"));
      }
      return ids;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listRootMessageIds()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  /**
   * Returns root messages, with or without populating potentially large 
   * content fields
   * @param txn
   * @param offset
   * @param limit
   * @return
   * @throws DBException 
   */
  public List<Message> listRootMessages(Connection txn, 
      boolean includeContent, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages "
          + "WHERE parent_msg_guid is null "
          + "ORDER BY published_timestamp "
          + "LIMIT ? OFFSET ?");
      int col = 1;
      stmt.setInt(col++, limit);
      stmt.setInt(col++, offset);
      
      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, includeContent);
      return msgs;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listRootMessageIds()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  public int getRootMessageCount(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM messages "
          + "WHERE parent_msg_guid is null ");
      
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getRootMessageCount()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  
  
  public List<String> listChildMessageIds(Connection txn, 
      String parentGuid, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT guid FROM messages "
          + "WHERE parent_msg_guid = ? "
          + "ORDER BY published_timestamp "
          + "LIMIT ? OFFSET ?");
      int col = 1;
      stmt.setString(col++, parentGuid);
      stmt.setInt(col++, limit);
      stmt.setInt(col++, offset);
      
      ResultSet rs = stmt.executeQuery();
      List<String> ids = new ArrayList<String>();
      while(rs.next())
      {
        ids.add(rs.getString("guid"));
      }
      return ids;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listChildMessageIds()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public List<Message> listChildMessages(Connection txn, 
      String parentGuid, boolean includeContent, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM messages "
          + "WHERE parent_msg_guid = ? "
          + "ORDER BY published_timestamp "
          + "LIMIT ? OFFSET ?");
      int col = 1;
      stmt.setString(col++, parentGuid);
      stmt.setInt(col++, limit);
      stmt.setInt(col++, offset);
      
      ResultSet rs = stmt.executeQuery();
      List<Message> msgs = resultSetToMessages(rs, includeContent);
      return msgs;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listChildMessageIds()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public int getChildMessageCount(Connection txn, String parentGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM messages "
          + "WHERE parent_msg_guid = ? ");
      int col = 1;
      stmt.setString(col++, parentGuid);
      
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getChildMessageCount()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public List<Integer> getChildMessageCounts(Connection txn, List<String> parentGuids)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM messages "
          + "WHERE parent_msg_guid = ? ");
      
      List<Integer> counts = new ArrayList<Integer>(parentGuids.size());
      for (String parentGuid : parentGuids)
      {
        stmt.setString(1, parentGuid);

        ResultSet rs = stmt.executeQuery();
        rs.next();
        counts.add(rs.getInt("count"));
      }
      return counts;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getChildMessageCounts()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  private List<Message> resultSetToMessages(ResultSet rs, boolean includeContent)
      throws SQLException, IOException
  {
    List<Message> list = new ArrayList<Message>();
    while(rs.next())
    {
      Message msg = new Message();

//      msg.setContent(null);
      msg.setExpiresTimestamp(rs.getTimestamp("expires_timestamp").getTime());
      msg.setGuid(rs.getString("guid"));
      msg.setParentMessage(rs.getString("parent_msg_guid"));
      msg.setPublishedTimestamp(rs.getTimestamp("published_timestamp").getTime());
      msg.setPublisherGuid(rs.getString("publisher_guid"));
      msg.setWorkflowExeId(rs.getString("workflow_exe_id"));
      msg.setWorkflowStepId(rs.getString("workflow_step_id"));
      msg.setTopicGuid(rs.getString("topic_guid"));
      msg.setUserDescription(rs.getString("user_desc"));
      
      if (includeContent)
      {
        msg.setBinaryContent(rs.getBytes("binary_content"));
        
        byte[] contentStrsAsBytes = rs.getBytes("content");
        Map<String, String> contentStings = jsonMapper.readValue(
            contentStrsAsBytes, 0, contentStrsAsBytes.length, Map.class);
        msg.setContent(contentStings);
      }

      list.add(msg);
    }
    return list;
  }

}
