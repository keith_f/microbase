/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.ArrayList;
import java.util.List;
import uk.org.microbase.dist.processes.ExecutionResult;

/**
 * Queries for notification messages of all topics.
 * 
 * @author Keith Flanagan
 */
public class JobCompletionListRefreshThread
  implements Runnable
{
  private final SharedState state;
  private final JobCompletionListTableModel model;
  
  public JobCompletionListRefreshThread(
      SharedState state, JobCompletionListTableModel model)
  {
    this.state = state;
    this.model = model;
  }
  
  
  @Override
  public void run()
  {
    try
    {
      try
      {
        //FIXME - for now return all state entries, but this will become too slow
        int offset = 0;
        int limit = Integer.MAX_VALUE;
        
        //FIXME - write this query in the DAO
        List<ExecutionResult> entries = new ArrayList<ExecutionResult>();
        
//        List<JobCompletionLogEntry> entries = state.getRuntime().
//            getProcessLog().
//            listMessageStatesInTimestampOrder(offset, limit);
//        state.getLog().append("Found "+entries.size()+" job completion entries");
        
        model.add(entries);
      }
      catch(Exception e)
      {
        e.printStackTrace();
        state.getLog().append(
            "Failed to query for message state entries: "+e.getMessage());
      }
    }
    catch(Exception e)
    {
      System.out.println("Unexpected error occurred while processing");
      e.printStackTrace();
    }
  }
  
}
