/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 5, 2012, 1:20:14 PM
 */

package uk.org.microbase.ui2;

/**
 *
 * @author Keith Flanagan
 */
public class MyBean
{
  private int foo;
  private String foo2;

  public MyBean()
  {
  }

  public int getFoo()
  {
    return foo;
  }

  public void setFoo(int foo)
  {
    this.foo = foo;
  }

  public String getFoo2()
  {
    return foo2;
  }

  public void setFoo2(String foo2)
  {
    this.foo2 = foo2;
  }
  
}
