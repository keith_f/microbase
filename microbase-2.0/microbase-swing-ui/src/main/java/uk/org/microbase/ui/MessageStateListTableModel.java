/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.swing.table.DefaultTableModel;
import uk.org.microbase.notification.data.MessageStateInfo;

/**
 *
 * @author Keith Flanagan
 */
public class MessageStateListTableModel
    extends DefaultTableModel
{
  
  private final Set<MessageStateInfo> current;
  
  public MessageStateListTableModel()
  {
    this.current = new HashSet<MessageStateInfo>();
    addColumn("Timestamp");
    addColumn("Responder");
    addColumn("Message ID");
    addColumn("State");
    addColumn("Description");
    
    fireTableStructureChanged();
  }

  public void add(MessageStateInfo state)
  {
    Set<MessageStateInfo> msgStateSet = new HashSet<MessageStateInfo>();
    msgStateSet.add(state);
    add(msgStateSet);
  }
  
  private boolean _add(MessageStateInfo msgState)
  {
    if (current.contains(msgState))
    {
      return false;
    }
    current.add(msgState);


    Object[] rowData = new Object[] {
      msgState.getTimestamp().toString(),
      msgState.getResponderGuid(),
      msgState.getMessageGuid(),
      msgState.getState().toString(),
      msgState.getDescription() 
    };
    
    this.addRow(rowData);
    return true;
  }
  
  public void add(Collection<MessageStateInfo> msgStates)
  {
    int oldRowCount = getRowCount();
    int numAdded = 0;
    for (MessageStateInfo msgState : msgStates)
    {
      if (_add(msgState))
      {
        numAdded ++;
      }
    }
    int newRowCount = getRowCount();
    
    if (newRowCount > oldRowCount)
    {
      if (oldRowCount > 0)
      {
        oldRowCount--;
      }
      fireTableRowsInserted(oldRowCount, newRowCount);
    }
    
  }
  
}
