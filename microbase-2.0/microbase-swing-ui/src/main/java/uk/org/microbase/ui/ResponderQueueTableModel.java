/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.HashMap;
import java.util.Map;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Keith Flanagan
 */
public class ResponderQueueTableModel
    extends DefaultTableModel
{
  
  private final Map<String, Integer> responderToQueueSize;
  
  public ResponderQueueTableModel()
  {
    this.responderToQueueSize = new HashMap<String, Integer>();
    addColumn("Responder");
    addColumn("Distributed queue size");
    
    fireTableStructureChanged();
  }

  
  private void _regenerateTableData()
  {
    for (int i=0; i<getRowCount(); i++)
    {
      this.removeRow(0);
    }
    for (String responderId : responderToQueueSize.keySet())
    {
      int queueSize = responderToQueueSize.get(responderId);
      Object[] rowData = new Object[] {
        responderId, queueSize
      };
      this.addRow(rowData);
    }
    
  }
  
  public void update(Map<String, Integer> items)
  {
    responderToQueueSize.clear();
    responderToQueueSize.putAll(items);
    _regenerateTableData();
//    fireTableDataChanged();
    fireTableRowsUpdated(0, items.size());
  }
  
}
