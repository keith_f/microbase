/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * MessageEditorPanel.java
 *
 * Created on Aug 21, 2011, 1:29:23 AM
 */
package uk.org.microbase.ui.admin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.util.UidGenerator;

/**
 *
 * @author keith
 */
public class MessageEditorPanel
    extends javax.swing.JPanel
{
  private static final Logger logger = 
      Logger.getLogger(Message.class.getName());
  
  private Message message;

  /** Creates new form MessageEditorPanel */
  public MessageEditorPanel()
  {
//    Message initialMsg = new Message();
//    initialMsg.setGuid(UidGenerator.generateUid());
//    initialMsg.setPublishedTimestamp(System.currentTimeMillis());
//    initialMsg.setExpiresTimestamp(Long.MAX_VALUE);
//    
//    setMessage(initialMsg);
    
    initComponents();
  }

  public Message getMessage()
  {
    message.setGuid(guidTextField.getText());
    message.setParentMessage(parentMsgTextField.getText());
    message.setTopicGuid(topicTextField.getText());
    message.setPublisherGuid(publisherTextField.getText());
    message.setWorkflowExeId(resultSetTextField.getText());
    message.setWorkflowStepId(stepIdTextField.getText());
    message.setUserDescription(userDescTextArea.getText());
    message.setContent(getDisplayedContentAsMap());
    return message;
  }

  public void setMessage(Message message)
  {
    this.message = message;
    if (message.getGuid() == null)
    {
      message.setGuid(UidGenerator.generateUid());
    }
    if (message.getPublishedTimestamp() <= 0)
    {
      message.setPublishedTimestamp(System.currentTimeMillis());
    }
      
      
    guidTextField.setText(message.getGuid());
    parentMsgTextField.setText(message.getParentMessage());
    publishedTextField.setText(
        new Date(message.getPublishedTimestamp()).toString());
    topicTextField.setText(message.getTopicGuid());
    expiresTextField.setText(
        new Date(message.getPublishedTimestamp()).toString());
    publisherTextField.setText(message.getPublisherGuid());
    resultSetTextField.setText(message.getWorkflowExeId());
    stepIdTextField.setText(message.getWorkflowStepId());
    userDescTextArea.setText(message.getUserDescription());
    if (message.getBinaryContent() == null)
    {
      binContentLenTextField.setText("NULL");
    }
    else
    {
      binContentLenTextField.setText(
          String.valueOf(message.getBinaryContent().length));
    }
//    contentTextArea.setText(
//        propsToString(msgContentToProps(message)));
    contentTextArea.setText(mapToString(message.getContent()));
//    System.out.println("message content: "+message.getContent());
  }
  
  
  public Map<String, String> getDisplayedContentAsMap()
  {
    Map<String, String> content = new HashMap<String, String>();
    
    ByteArrayInputStream bais = 
        new ByteArrayInputStream(contentTextArea.getText().getBytes());
    Properties props = new Properties();
    try
    {
      props.load(bais);
    }
    catch(Exception e)
    {
      e.printStackTrace();
      logger.info("Failed to create Properties object from "
          + "displayed message content.");
    }
    for (String key : props.stringPropertyNames())
    {
      content.put(key, props.getProperty(key));
    }
    
    return content;
  }
  
  public Properties msgContentToProps(Message msg)
  {
    Properties props = new Properties();
    
    for (String key : msg.getContent().keySet())
    {
      String val = msg.getContent().get(key);
      props.setProperty(key, val);
    }
    return props;
  }
  
  public String propsToString(Properties props)
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try
    {
      props.store(baos, null);
    }
    catch(Exception e)
    {
      e.printStackTrace();
      logger.info("Failed to create Properties object from message content.");
    }
    return new String(baos.toByteArray());
  }
  
  public String mapToString(Map<String, String> content)
  {
    StringBuilder text = new StringBuilder();
    for (String key : content.keySet())
    {
      String value = content.get(key);
      text.append(key).append(" : ");
      text.append(value);
      text.append("\n");
    }
    return text.toString();
  }

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        guidTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        publishedTextField = new javax.swing.JTextField();
        expiresTextField = new javax.swing.JTextField();
        publisherTextField = new javax.swing.JTextField();
        resultSetTextField = new javax.swing.JTextField();
        stepIdTextField = new javax.swing.JTextField();
        binContentLenTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        contentTextArea = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        topicTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        userDescTextArea = new javax.swing.JTextArea();
        parentMsgTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();

        guidTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guidTextFieldActionPerformed(evt);
            }
        });

        jLabel1.setText("GUID");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, guidTextField, org.jdesktop.beansbinding.ObjectProperty.create(), jLabel1, org.jdesktop.beansbinding.BeanProperty.create("labelFor"));
        bindingGroup.addBinding(binding);

        jLabel2.setLabelFor(publishedTextField);
        jLabel2.setText("Published");

        jLabel3.setLabelFor(publisherTextField);
        jLabel3.setText("Publisher");

        jLabel4.setLabelFor(expiresTextField);
        jLabel4.setText("Expires");

        jLabel5.setLabelFor(resultSetTextField);
        jLabel5.setText("Result set");

        jLabel6.setLabelFor(stepIdTextField);
        jLabel6.setText("Step");

        jLabel7.setLabelFor(contentTextArea);
        jLabel7.setText("Content");

        jLabel8.setLabelFor(binContentLenTextField);
        jLabel8.setText("Binary content");

        publishedTextField.setEnabled(false);

        expiresTextField.setEnabled(false);

        publisherTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                publisherTextFieldActionPerformed(evt);
            }
        });

        binContentLenTextField.setEnabled(false);
        binContentLenTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                binContentLenTextFieldActionPerformed(evt);
            }
        });

        contentTextArea.setColumns(20);
        contentTextArea.setRows(5);
        jScrollPane1.setViewportView(contentTextArea);

        jLabel9.setLabelFor(topicTextField);
        jLabel9.setText("Topic");

        jLabel10.setText("User description");

        userDescTextArea.setColumns(20);
        userDescTextArea.setRows(5);
        jScrollPane2.setViewportView(userDescTextArea);

        parentMsgTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parentMsgTextFieldActionPerformed(evt);
            }
        });

        jLabel11.setText("Parent message");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(jLabel11)
                                .add(0, 0, Short.MAX_VALUE))
                            .add(layout.createSequentialGroup()
                                .add(66, 66, 66)
                                .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(guidTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
                            .add(parentMsgTextField)))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 103, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel6)
                            .add(jLabel5)
                            .add(jLabel3)
                            .add(jLabel4)
                            .add(jLabel9)
                            .add(jLabel2)
                            .add(jLabel8)
                            .add(jLabel7))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, resultSetTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, publisherTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                            .add(expiresTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                            .add(topicTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                            .add(stepIdTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, publishedTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, binContentLenTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(guidTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(parentMsgTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel11))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(publishedTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(topicTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel9))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(expiresTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(publisherTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel3))
                .add(2, 2, 2)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(resultSetTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel5))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(stepIdTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel10)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 63, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(binContentLenTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel8))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel7)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE))
                .addContainerGap())
        );

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents

private void publisherTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_publisherTextFieldActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_publisherTextFieldActionPerformed

private void binContentLenTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_binContentLenTextFieldActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_binContentLenTextFieldActionPerformed

private void parentMsgTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_parentMsgTextFieldActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_parentMsgTextFieldActionPerformed

private void guidTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guidTextFieldActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_guidTextFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField binContentLenTextField;
    private javax.swing.JTextArea contentTextArea;
    private javax.swing.JTextField expiresTextField;
    private javax.swing.JTextField guidTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField parentMsgTextField;
    private javax.swing.JTextField publishedTextField;
    private javax.swing.JTextField publisherTextField;
    private javax.swing.JTextField resultSetTextField;
    private javax.swing.JTextField stepIdTextField;
    private javax.swing.JTextField topicTextField;
    private javax.swing.JTextArea userDescTextArea;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
