/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * RegisterResponderDialog.java
 *
 * Created on Aug 22, 2011, 10:20:41 PM
 */
package uk.org.microbase.ui.admin;

import uk.org.microbase.dist.responder.MicrobaseRegistration;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.ui.MessageBox;
import uk.org.microbase.ui.SharedState;

/**
 *
 * @author keith
 */
public class RegisterResponderDialog
    extends javax.swing.JDialog
{

  /** Creates new form RegisterResponderDialog */
  public RegisterResponderDialog(java.awt.Frame parent, boolean modal)
  {
    super(parent, modal);
    initComponents();
    
    ResponderInfo defaultInfo = new ResponderInfo();
    defaultInfo.setMaxDistributedCores(10);
    defaultInfo.setMaxLocalCores(10);
    defaultInfo.setEnabled(true);
    responderEditorPanel.setResponderInfo(defaultInfo);
    setTitle("Register a new responder");
  }

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    registerButton = new javax.swing.JButton();
    jSeparator1 = new javax.swing.JSeparator();
    responderEditorPanel = new uk.org.microbase.ui.admin.ResponderEditorPanel();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    registerButton.setText("Register");
    registerButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        registerButtonActionPerformed(evt);
      }
    });

    org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
      .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
        .addContainerGap()
        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
          .add(org.jdesktop.layout.GroupLayout.LEADING, responderEditorPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
          .add(jSeparator1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
          .add(registerButton))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
      .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
        .addContainerGap()
        .add(responderEditorPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
        .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
        .add(registerButton)
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

private void registerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerButtonActionPerformed
  try
  {
    Appendable log = SharedState.getState().getLog();
    MicrobaseRegistration reg = 
        SharedState.getState().getRuntime().getRegistration();
    ResponderInfo info = responderEditorPanel.getResponderInfo();
    log.append("Attempting to register responder: "+info.getResponderId());
    log.append(info.getResponderId()+" : "+info);
    reg.registerResponder(info);
    dispose();
  }
  catch(Exception e)
  {
    e.printStackTrace();
    MessageBox.error("Failed to register a responder", e.getMessage());
  }
}//GEN-LAST:event_registerButtonActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[])
  {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
     * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try
    {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
      {
        if ("Nimbus".equals(info.getName()))
        {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    }
    catch (ClassNotFoundException ex)
    {
      java.util.logging.Logger.getLogger(RegisterResponderDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    catch (InstantiationException ex)
    {
      java.util.logging.Logger.getLogger(RegisterResponderDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      java.util.logging.Logger.getLogger(RegisterResponderDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    catch (javax.swing.UnsupportedLookAndFeelException ex)
    {
      java.util.logging.Logger.getLogger(RegisterResponderDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the dialog */
    java.awt.EventQueue.invokeLater(new Runnable()
    {

      public void run()
      {
        RegisterResponderDialog dialog = new RegisterResponderDialog(new javax.swing.JFrame(), true);
        dialog.addWindowListener(new java.awt.event.WindowAdapter()
        {

          @Override
          public void windowClosing(java.awt.event.WindowEvent e)
          {
            System.exit(0);
          }
        });
        dialog.setVisible(true);
      }
    });
  }
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JButton registerButton;
  private uk.org.microbase.ui.admin.ResponderEditorPanel responderEditorPanel;
  // End of variables declaration//GEN-END:variables
}
