/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.*;
import javax.swing.table.DefaultTableModel;
import uk.org.microbase.dist.responder.ResponderInfo;

/**
 *
 * @author Keith Flanagan
 */
public class ResponderListTableModel
    extends DefaultTableModel
{
  
  private final Set<ResponderInfo> current;
  
  public ResponderListTableModel()
  {
    this.current = new HashSet<ResponderInfo>();
    addColumn("GUID");
    addColumn("Topic");
    addColumn("Preferred CPUs");
    addColumn("Populator last run");
    addColumn("Populator run every");
    addColumn("Enabled");
    addColumn("Max cores (local)");
    addColumn("Max cores (dist)");
    
    fireTableStructureChanged();
  }
  
  public List<String> getResponderIds()
  {
    List<String> ids = new LinkedList<String>();
    for (ResponderInfo info : current)
    {
      ids.add(info.getResponderId());
    }
    
    return ids;
  }

  public void add(ResponderInfo info)
  {
    Set<ResponderInfo> set = new HashSet<ResponderInfo>();
    set.add(info);
    add(info);
  }
  
  private boolean _add(ResponderInfo info)
  {
    if (current.contains(info))
    {
      return false;
    }
    current.add(info);

    Object[] rowData = new Object[] {
      info.getResponderId(),
      info.getTopicOfInterest(),
      info.getPreferredCpus(),
      new Date(info.getQueuePopulatorLastRunTimestamp()).toString(),
      info.getQueuePopulatorRunEveryMs(),
      info.isEnabled(),
      info.getMaxLocalCores(),
      info.getMaxDistributedCores()
      
    };
    
    this.addRow(rowData);
    return true;
  }
  
  public void add(Collection<ResponderInfo> items)
  {
    int oldRowCount = getRowCount();
    int numAdded = 0;
    for (ResponderInfo item : items)
    {
      if (_add(item))
      {
        numAdded ++;
      }
    }
    int newRowCount = getRowCount();
    
    if (newRowCount > oldRowCount)
    {
      if (oldRowCount > 0)
      {
        oldRowCount--;
      }
      fireTableRowsInserted(oldRowCount, newRowCount);
    }
    
  }
  
  public void clear()
  {
    int size = current.size();
    current.clear();

    if (size > 0)
    {
      for (int i=0; i<size; i++)
      {
        this.removeRow(0);
      }
      fireTableRowsDeleted(0, size);
    }
  }
  
}
