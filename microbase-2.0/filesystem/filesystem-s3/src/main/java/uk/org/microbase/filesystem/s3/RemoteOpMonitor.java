/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 26, 2012, 5:53:20 PM
 */

package uk.org.microbase.filesystem.s3;

import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 * Monitors file operations (transfers to and from remote locations). Ensures
 * that only one remote operation per file is possible simultaneously.
 * 
 * @author Keith Flanagan
 */
public class RemoteOpMonitor
{
  private static final long DEFAULT_WAIT_TIME = 5 * 1000;
  private final Set<String> currentOperations;
  
  public RemoteOpMonitor()
  {
    currentOperations = new HashSet<>();
  }
  private String createId(MBFile remoteFile)
  {
    StringBuilder id = new StringBuilder();
    id.append(remoteFile.getBucket()).append(":");
    id.append(remoteFile.getPath()).append(":");
    id.append(remoteFile.getName());
    
    return id.toString();
  }
  
  public void notifyOperationStarting(MBFile remoteFile)
  {
    String id = createId(remoteFile);
        
    synchronized(currentOperations)
    {
      while(currentOperations.contains(id))
      {
        try
        {
          System.out.println(Thread.currentThread().getName()
              +": A simultaneous remote operation on: " + id
              + " is already in progress. Waiting until it completes.");
          currentOperations.wait(DEFAULT_WAIT_TIME);
        }
        catch(InterruptedException e)
        {
          e.printStackTrace();
        }
      }
      currentOperations.add(id);
    } 
  }
  
  public void notifyOperationComplete(MBFile remoteFile)
  {
    String id = createId(remoteFile);
        
    synchronized(currentOperations)
    {
      currentOperations.remove(id);
      currentOperations.notifyAll();
    }
  }
}
