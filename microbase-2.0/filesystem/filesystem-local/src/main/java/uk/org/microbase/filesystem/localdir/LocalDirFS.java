/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */
package uk.org.microbase.filesystem.localdir;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBDirectory;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.util.file.FileUtils;

/**
 * Implements the Microbase Filesystem SPI using a local directory. This
 * implementation is useful for running and debugging jobs on the same machine.
 * Alternatively, it may also be useful if you have a cluster of machines
 * sharing the same network filesystem. It may also find uses if you have a
 * Microbase installation on a single, large multi-CPU machine.
 *
 * The local directory implementation of the MicrobaseFS amalgamates the
 * 'bucket' and 'path' fields into a single directory path.
 *
 * @author Keith Flanagan
 */
public class LocalDirFS
    implements MicrobaseFS
{

  private static final Logger logger =
      Logger.getLogger(LocalDirFS.class.getName());
  private static final String CONFIG_ENTRY =
      LocalDirFS.class.getSimpleName() + ".properties";
  private static final String PROP_DIR_ROOT = "data_directory";
  private File rootDir;
  private boolean enabled;

  public LocalDirFS()
  {
  }
  
  @Override
  public void configure(ClassLoader cl)
          throws FSException
  {
    logger.log(Level.INFO, "{0} initialising", LocalDirFS.class.getName());
    loadDefaultConfig(cl);
    logger.log(Level.INFO, "{0} ready", LocalDirFS.class.getName());
  }

  /**
   * Loads the configuration for this provider from a file on the classpath.
   * This file configures the root directory to use for local file storage
   *
   * @throws FSException
   */
  private void loadDefaultConfig(ClassLoader cl)
      throws FSException
  {
    try
    {
      InputStream configStream = cl.getResourceAsStream(CONFIG_ENTRY);
      if (configStream == null)
      {
        throw new FSException("Configuration file: " + CONFIG_ENTRY
            + " could not be found on the CLASSPATH");
      }

      logger.log(Level.INFO,
          "Loading filesystem provider config from "
          + "CLASSPATH entry {0}", CONFIG_ENTRY);
      Properties config = new Properties();
      config.load(configStream);


      String rootDirProperty = config.getProperty(PROP_DIR_ROOT);
      if (rootDirProperty == null)
      {
        throw new FSException("Config file "+CONFIG_ENTRY
            + " had no value for property: " + PROP_DIR_ROOT);
      }
      rootDir = new File(rootDirProperty);
      logger.log(Level.INFO, "Using local directory: {0}",
                              rootDir.getAbsolutePath());
      if (!rootDir.exists())
      {
        logger.log(Level.INFO,
            "Attempting to create directory: {0}",
            rootDir.getAbsolutePath());
        rootDir.mkdirs();
      }
    }
    catch (IOException e)
    {
      throw new FSException(
          "Failed to load configuration from classpath entity: "
          + CONFIG_ENTRY, e);
    }

    setEnabled(true);
  }

  @Override
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  @Override
  public boolean isEnabled()
  {
    return enabled;
  }

  @Override
  public InputStream downloadToCache(MBFile remoteFile, 
      boolean forceRedownload)
      throws FSOperationNotSupportedException, FSException
  {
    File bucket = getBucketPath(remoteFile.getBucket(), remoteFile.getPath());
    if (bucket == null || !bucket.exists())
    {
      //return null;
      throw new FSException("No such bucket: " + remoteFile.getBucket());
    }
    File file = new File(bucket, remoteFile.getName());
    if (!file.exists())
    {
      //return null;
      throw new FSException("No such file: " + remoteFile);
    }
    try
    {
      FileInputStream fis = new FileInputStream(file);
      return fis;
    }
    catch (IOException e)
    {
      throw new FSException("Failed to retreive: " + remoteFile, e);
    }
  }

  @Override
  public void downloadFileToSpecificLocation(MBFile remoteFile,
      File destinationFile, boolean useCache)
      throws FSOperationNotSupportedException, FSException
  {
    File bucket = getBucketPath(remoteFile.getBucket(), remoteFile.getPath());
    if (bucket == null)
    {
      throw new FSException("No such bucket: " + remoteFile.getBucket());
    }
    File file = new File(bucket, remoteFile.getName());
    if (!file.exists())
    {
      throw new FSException("No such file: " + remoteFile);
    }
    try
    {
      logger.info("Copying " + file.getAbsolutePath() + " ---> "
          + destinationFile.getAbsolutePath());
      FileUtils.copyFile(file, destinationFile);
    }
    catch (IOException e)
    {
      throw new FSException("Failed to retreive: " + remoteFile, e);
    }
  }

  @Override
  public boolean exists(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException
  {
    File bucket = getBucketPath(remoteFile.getBucket(), remoteFile.getPath());
    if (bucket == null)
    {
      return false;
    }
    File file = new File(bucket, remoteFile.getName());
    return file.exists();
  }
  
  @Override
  public boolean existsCached(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException
  {
    return true;
  }

  @Override
  public Set<String> listRemoteFilenames(MBDirectory remoteDir,
      final boolean includeDirectories, boolean listFullPathname)
      throws FSOperationNotSupportedException, FSException
  {
    File bucket = getBucketPath(remoteDir.getBucket(), remoteDir.getPath());
    if (bucket == null)
    {
      return new HashSet<String>();
    }
    Set<String> filenames = new HashSet<String>();
    File[] files = bucket.listFiles(new FileFilter() {
      @Override
      public boolean accept(File file)
      {
        return file.isFile() || (includeDirectories && file.isDirectory());
      }
    });
    
    for (File file : files)
    {
      if (listFullPathname)
      {
        //Get the path relative to the bucket location.
        String filenameStr = 
            file.getAbsolutePath().replace(bucket.getAbsolutePath(), "");
        
        StringBuilder filename = new StringBuilder(filenameStr);
        //Remove leading slash, if any
        if (filenameStr.startsWith("/"))
        {
          filename.deleteCharAt(0);
        }
        //Add a trailing slash for directories
        if (file.isDirectory() && !filenameStr.endsWith("/"))
        {
          filename.append("/");
        }
        filenames.add(filename.toString());
      }
      else
      {
        filenames.add(file.getName());
      }
    }
    return filenames;
  }

  @Override
  public Set<FileMetaData> listRemoteFileMetaData(MBDirectory remoteDir)
      throws FSOperationNotSupportedException, FSException
  {    
    try
    {
      //Get filenames, excluding directories
      Set<String> names = listRemoteFilenames(remoteDir, false, false);
      Set<FileMetaData> metaData = new HashSet<FileMetaData>();
      
      for (String remoteName : names)
      {
        MBFile remoteFile = new MBFile(remoteDir, remoteName);
        metaData.add(getRemoteFileMetaData(remoteFile));
      }
      
      return metaData;
    }
    catch(Exception e)
    {
      throw new FSException("Failed to obtain file metadata for: " + remoteDir, e);
    }
  }
  
  @Override
  public FileMetaData getRemoteFileMetaData(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException
  {
    try
    {
      File bucket = getBucketPath(remoteFile.getBucket(), remoteFile.getPath());
      if (bucket == null)
      {
        return null;
      }
      File file = new File(bucket, remoteFile.getName());
      file.length();

      FileMetaData fsData = new FileMetaData(remoteFile);
      fsData.setLength(file.length());
      fsData.setLastModified(new Date(file.lastModified()));

      return fsData;
    }
    catch(Exception e)
    {
      throw new FSException("Failed to obtain file metadata for: " + remoteFile, e);
    }
  }

  /**
   * Locates a directory representing a path within a bucket.
   * @param bucketName the bucket to find
   * @param path the directory path required within the bucket. This may be
   * null or an empty string if you just require the root path to the bucket.
   * @return a File object pointing to the appropriate directory
   * @throws FSException
   */
  private File getBucketPath(String bucketName, String path)
      throws FSException
  {
    StringBuilder fullPath = new StringBuilder();
    fullPath.append(rootDir.getAbsolutePath())
        .append(File.separator)
        .append(bucketName);
    if (path != null && !path.isEmpty())
    {
      fullPath.append(File.separator).append(path);
    }
    //File bucket = new File(rootDir, bucketName);
    File bucket = new File(fullPath.toString());
    if (bucket.exists() && bucket.isDirectory())
    {
      return bucket;
    }
    
    return null;
  }

  private File createBucketPath(String bucketName, String path)
      throws FSException
  {
    StringBuilder fullPath = new StringBuilder();
    fullPath.append(rootDir.getAbsolutePath())
        .append(File.separator)
        .append(bucketName);
    if (path != null && !path.isEmpty())
    {
      fullPath.append(File.separator).append(path);
    }

    File bucketPath = new File(fullPath.toString());
    if (!bucketPath.exists())
    {
      bucketPath.mkdirs();
    }
    if (!bucketPath.exists() || !bucketPath.isDirectory())
    {
      throw new FSException("Failed to create bucket: " + bucketName
          + " at " + bucketPath.getAbsolutePath());
    }
    return bucketPath;
  }

  @Override
  public void upload(File dataFile, MBFile remoteFile,
      Map<String, String> tags)
      throws FSOperationNotSupportedException, FSException
  {
    FileInputStream fis = null;
    try
    {
      fis = new FileInputStream(dataFile);
      _uploadStream(fis, remoteFile, tags);
    }
    catch (Exception e)
    {
      throw new FSException(
          "Failed to write local file: " + dataFile.getAbsolutePath()
          + " to remote bucket: " + remoteFile, e);
    }
    finally
    {
      FileUtils.closeInputStreams(fis);
    }
  }
  
  private void _uploadStream(InputStream dataStream, MBFile remoteFile,
      Map<String, String> tags)
      throws FSOperationNotSupportedException, FSException
  {
    File bucket = getBucketPath(remoteFile.getBucket(), remoteFile.getPath());
    if (bucket == null)
    {
      bucket = createBucketPath(remoteFile.getBucket(), remoteFile.getPath());
    }
    File destFile = new File(bucket, remoteFile.getName());

    //TODO remove this if/else block - only here for testing purposes
    if (destFile.exists())
    {
      logger.log(Level.INFO,
          "Overwriting existing file: {0}",
          destFile.getAbsolutePath());
    }
    else
    {
      logger.log(Level.INFO,
          "Creating new file: {0}", destFile.getAbsolutePath());
    }


    try
    {
      FileOutputStream destStream = new FileOutputStream(destFile);
      FileUtils.copyStream(dataStream, destStream);
    }
    catch (IOException e)
    {
      throw new FSException("Failed to write stream to: " + remoteFile, e);
    }

    //TODO store tags?
  
    
  }

    @Override
    public void deleteRemoteCopy(MBFile remoteFile) throws FSOperationNotSupportedException, FSException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteLocalCopy(MBFile remoteFile) throws FSOperationNotSupportedException, FSException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
