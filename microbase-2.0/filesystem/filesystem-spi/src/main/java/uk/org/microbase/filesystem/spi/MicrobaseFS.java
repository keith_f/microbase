/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */
package uk.org.microbase.filesystem.spi;

import java.io.File;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

/**
 * Defines the operations of the Microbase FileSystem. This is a Service
 * Provider Interface. Implementations of this interface may support a subset
 * of the methods defined here. Unsupported functionality should return
 * gracefully and appropriately to method.
 *
 * 'Buckets' allow files to be organised into directory-like groups. The exact
 * details of how files are organised are implementation-specific; they are
 * essentially names that can be used to group a selection of related files
 * together.
 * 
 * Generally, implementations should be thread-safe (except where indicated 
 * below). E.g., N processes that request a download of a file at the same time
 * should not cause a problem.
 *
 * @author Keith Flanagan
 */
public interface MicrobaseFS
{
  /**
   * Enables the filesystem implementation to perform any setup operations
   * before it can begin file operations. This method must be called before
   * any file operations are attempted.
   * 
   * @param cl
   * @throws FSException 
   */
  public void configure(ClassLoader cl)
          throws FSException;
  
  public boolean isEnabled();
  public void setEnabled(boolean enabled);

  /**
   *
   * @param dataFile
   * @param remoteBucket
   * @param filename
   * @param tags
   * @throws FSOperationNotSupportedException if this operation is not
   * implemented or is not possible to implement using the transport
   * mechanism used by the provider.
   * @throws FSException if an error occurred while transferring the file.
   * FSException is usually thrown when a transient error occurs (such as
   * a network problem or a disk becomes full)
   */
  public void upload(File dataFile, MBFile remoteFile,
      Map<String, String> tags)
      throws FSOperationNotSupportedException, FSException;

  /**
   * Downloads a file (in its entirety) from the remote file store to a local 
   * cache directory before returning an InputStream to the caller. This method
   * does not support streaming downloads - such functionality may be provided
   * in a later release. Depending on the implementation, the 'download' stage
   * may not actually occur (e.g., the local filesystem implementation does not
   * first have to copy the file in order to read from it.).
   * 
   * Files are obtained in their entirety before any data is provided to the
   * caller. Therefore, the caller can be relatively sure that if an InputStream
   * is successfully obtained, that the only IOExceptions that can occur while
   * using that stream are due to local filesystem problems.
   * 
   * If a problem occurs while obtaining a local copy of the file, then any
   * partial copy obtained is removed.
   * 
   * @param remoteBucket
   * @param remotePath 
   * @param remoteName
   * @param forceRedownload set to true if you wish to re-download a file,
   * regardless of whether it is present in the local cache or not. The cached
   * version (if any) will be replaced with the newly downloaded copy.
   * @return
   * @throws FSOperationNotSupportedException if this operation is not
   * implemented or is not possible to implement using the transport
   * mechanism used by the provider.
   * @throws FSException if an error occurred while transferring the file.
   * FSException is usually thrown when a transient error occurs (such as
   * a network problem or a disk becomes full).
   * An FSException is also thrown if the requested file could not be found.
   */
  public InputStream downloadToCache(
      MBFile remoteFile, boolean forceRedownload)
      throws FSOperationNotSupportedException, FSException;

  /**
   * Downloads a file in its entirety to a specific location 
   * (<code>destinationFile</code>). This method is useful in situations where
   * you wish to obtain a copy of a remote file, but save it directly to your
   * own specified location. This method will attempt to overwrite the 
   * destination file if it already exists. In the event of an error occurring
   * during file transfer, you will need to perform the necessary tidy-up
   * operations manually (e.g., removing partially-downloaded files).
   * 
   * @param remoteBucket
   * @param remotePath 
   * @param remoteName
   * @param destinationFile
   * @param useCache set true if you wish this operation to check first whether
   * a locally-cached copy of the file is available, and if so to simply copy
   * it to the specified <code>destinationFile</code>. 
   * Set false if you wish this method to download a fresh copy of the file to
   * the specified <code>destinationFile</code>, independent of any cached copy.
   * @throws FSOperationNotSupportedException if this operation is not
   * implemented or is not possible to implement using the transport
   * mechanism used by the provider.
   * @throws FSException if an error occurred while transferring the file.
   * FSException is usually thrown when a transient error occurs (such as
   * a network problem or a disk becomes full).
   * An FSException is also thrown if the requested file could not be found.
   */
  public void downloadFileToSpecificLocation(
      MBFile remoteFile, File destinationFile, boolean useCache)
      throws FSOperationNotSupportedException, FSException;

  /**
   * Returns true if the specified file exists at the remote location.
   * 
   * @param remoteBucket
   * @param remoteName
   * @return
   * @throws FSOperationNotSupportedException if this operation is not
   * implemented or is not possible to implement using the transport
   * mechanism used by the provider.
   * @throws FSException if an error occurred while transferring the file.
   * FSException is usually thrown when a transient error occurs (such as
   * a network problem or a disk becomes full)
   */
  public boolean exists(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException;
  
  public boolean existsCached(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException;

  /**
   * Returns a set of filenames of all the files in a specified bucket / path
   *
   * @param remoteBucket the remote bucket to scan
   * @param remotePath an optional path string. Use NULL to list the bucket root.
   * @param includeDirectories set true to include subdirectories in the listing
   * name. Directory entries can be recognised via their trailing '/'.
   * Set false to return the names of files only.
   * @param listFullPathname if set 'true', then returned path name strings will
   * be prepended by <code>remotePath</code>, otherwise only the file/directory
   * name will be returned.
   * @return a set of filenames present in the remote bucket
   * @throws FSOperationNotSupportedException if this operation is not
   * implemented or is not possible to implement using the transport
   * mechanism used by the provider.
   * @throws FSException if an error occurred while transferring the file.
   * FSException is usually thrown when a transient error occurs (such as
   * a network problem or a disk becomes full)
   */
  public Set<String> listRemoteFilenames(MBDirectory remoteDir,
      boolean includeDirectories, boolean listFullPathname)
      throws FSOperationNotSupportedException, FSException;
  
  /**
   * Similar to <code>listFilenames</code>, but returns a set of 
   * <code>FileMetaData</code> objects for each file (NOT subdirectory)
   * found at the specified bucket / path. The data includes bucket/path/name,
   * plus other properties such as file size.
   * 
   * @param remoteBucket
   * @param remotePath
   * @return
   * @throws FSOperationNotSupportedException
   * @throws FSException 
   */
  public Set<FileMetaData> listRemoteFileMetaData(MBDirectory remoteDir)
      throws FSOperationNotSupportedException, FSException;
  
  /**
   * Returns a <code>FileMetaData</code> object for a specific file on the
   * remote server. 
   * 
   * @param remoteBucket
   * @param remotePath
   * @param remoteName
   * @return
   * @throws FSOperationNotSupportedException
   * @throws FSException 
   */
  public FileMetaData getRemoteFileMetaData(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException;
  
  
  /**
   * Deletes the specified file from the remote storage.
   * 
   * @param remoteBucket
   * @param remotePath
   * @param remoteName
   * @throws FSOperationNotSupportedException
   * @throws FSException 
   */
  public void deleteRemoteCopy(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException;
  
  /**
   * Deletes the specified file from the local storage cache.
   * 
   * @param remoteBucket
   * @param remotePath
   * @param remoteName
   * @throws FSOperationNotSupportedException
   * @throws FSException 
   */
  public void deleteLocalCopy(MBFile remoteFile)
      throws FSOperationNotSupportedException, FSException;
}
