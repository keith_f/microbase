/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 27, 2012, 3:39:50 PM
 */

package uk.org.microbase.filesystem.spi;


/**
 * Represents a location within the Microbase filesystem. MBFS plugins are
 * capable of mapping an abstract location (represented by objects such as this),
 * to a physical location, such as a local disk, or an Amazon S3 bucket.
 * 
 * A file location consists of three parts:
 * 1) A 'bucket', that provides a means to address different locations, such as 
 * different disk drives, or different network locations.
 * 2) A 'path' is a forward-slash separated directory path within (1).
 * 3) Optionally, the name of a file within (2).
 * 
 * If the filename (3) is NOT specified, then the location object is taken to 
 * refer to a directory location rather than a file.
 * 
 * @author Keith Flanagan
 */
public class MBFile
{
  private String bucket;
  private String path;
  private String name;
  
  public MBFile()
  {
  }
  
  public MBFile(String bucket, String path, String name)
  {
    this.bucket = bucket;
    this.path = path;
    this.name = name;
  }  
  
  public MBFile(MBDirectory directory, String name)
  {
    this.bucket = directory.getBucket();
    this.path = directory.getPath();
    this.name = name;
  }
  
  public MBDirectory toDirectoryLocation()
  {
    return new MBDirectory(bucket, path);
  }


  @Override
  public String toString()
  {
    return "FileLocation{" + "bucket=" + bucket + ", path=" + path 
        + ", name=" + name + '}';
  }

  public String getBucket()
  {
    return bucket;
  }

  public void setBucket(String bucket)
  {
    this.bucket = bucket;
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

}
