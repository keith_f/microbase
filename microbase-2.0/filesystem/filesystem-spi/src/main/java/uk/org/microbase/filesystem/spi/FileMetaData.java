/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 27, 2012, 3:39:50 PM
 */

package uk.org.microbase.filesystem.spi;

import java.util.Date;

/**
 * Contains information relating to a file present in a particular Microbase
 * filesystem location.
 * 
 * @author Keith Flanagan
 */
public class FileMetaData
{
  private MBFile location;
  
  private long length;
  private Date lastModified;
  
  public FileMetaData()
  {
  }
  
  public FileMetaData(String bucket, String path, String name)
  {
    this(bucket, path, name, -1, null);
  }
  
  public FileMetaData(MBFile mbfsLocation)
  {
    this(mbfsLocation, -1, null);
  }

  public FileMetaData(String bucket, String path, String name, 
      long length, Date lastModified)
  {
    this(new MBFile(bucket, path, name), length, lastModified);
  }
  
  public FileMetaData(MBFile mbfsLocation, 
      long length, Date lastModified)
  {
    this.location = mbfsLocation;
    this.length = length;
    this.lastModified = lastModified;
  }

  @Override
  public String toString()
  {
    return "FileMetaData{" + "location=" + location + ", length=" + length + ", lastModified=" + lastModified + '}';
  }

  public MBFile getLocation()
  {
    return location;
  }

  public void setLocation(MBFile location)
  {
    this.location = location;
  }
  
  public long getLength()
  {
    return length;
  }

  public void setLength(long length)
  {
    this.length = length;
  }

  public Date getLastModified()
  {
    return lastModified;
  }

  public void setLastModified(Date lastModified)
  {
    this.lastModified = lastModified;
  }
}
