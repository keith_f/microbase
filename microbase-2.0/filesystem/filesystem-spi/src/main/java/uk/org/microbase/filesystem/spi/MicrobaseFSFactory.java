/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.org.microbase.filesystem.spi;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 *
 * @author Keith Flanagan
 */
public class MicrobaseFSFactory
{
//  public static MicrobaseFS getDefaultProvider()
//  {
//    return new AnyWriteMultiReadAggregatedFSProvider();
//  }
  
  public static MicrobaseFS getProviderByClassName(String classname)
      throws FSException
  {
    ServiceLoader<MicrobaseFS> fsLoader = ServiceLoader.load(MicrobaseFS.class);
    Iterator<MicrobaseFS> itr = fsLoader.iterator();
    while(itr.hasNext())
    {
      MicrobaseFS impl = itr.next();
      System.out.println("Found MBFS implementation: "+impl.getClass().getName());
      if (impl.getClass().getName().equals(classname))
      {
        return impl;
      }
    }
    throw new FSException("A MBFS plugin with classname: "+classname
        +" could not be found. Please check your classpath and or classname.");
  }
}
