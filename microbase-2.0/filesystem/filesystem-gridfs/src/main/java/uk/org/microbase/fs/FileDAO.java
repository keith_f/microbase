/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 08-Nov-2010, 13:03:13
 */

package uk.org.microbase.fs;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import me.z80.mongodb.DBException;
import uk.org.microbase.util.UidGenerator;

/**
 *
 * @author Keith Flanagan
 */
public class FileDAO
   // extends CollectionDAO
{
  private static final Logger logger =
      Logger.getLogger(FileDAO.class.getName());

  //private static final int DEFAULT_CHUNK_SIZE_BYTES = 1024 * 1024 * 3; //3 MB

  /*
   * DON'T change this yet - with 1.6.4 at least, files are stored with the
   * correct chunk size, but are read with 256kb!
   */
  private static final int DEFAULT_CHUNK_SIZE_BYTES = 262144; //256kb

  private static final String DEFAULT_COLLECTION = "fs";

  private final DB db;
  private String bucket;
  private int chunkSize;
  ///protected final DBCollection collection;
  //protected final ObjectMapper jsonMapper;


  private GridFS fs;

  public FileDAO(DB db)
  {
    this(db, DEFAULT_COLLECTION);
  }

  public FileDAO(DB db, String bucket)
  {
    this.db = db;
    this.bucket = bucket;
    this.chunkSize = DEFAULT_CHUNK_SIZE_BYTES;
    fs = new GridFS(db, bucket);
  }


  /**
   * Stores a file in GridFS.
   *
   * @param file the file whose data is to be stored. The filename of
   * <code>file</code> will be used.
   *
   * @throws DBException
   */
  public void upload(File file)
      throws DBException
  {
    upload(file, new HashMap<String, Object>());
  }

  /**
   * Stores a file in GridFS, along with an additional set of user-defined
   * properties that may be used for querying at a later time.
   *
   * @param file
   * @param properties
   * @throws DBException
   */
  public void upload(File file, Map<String, ?> properties)
      throws DBException
  {
    upload(file, file.getName(), properties);
  }

  /**
   * Stores a file in GridFS, but writes it with a filename of
   * <code>renameTo</code>, rather than the name of the file whose data is to be
   * uploaded. A set of user-defined <code>properties</code> may also optionally
   * be uploaded.
   *
   * In addition, the following Microbase-assigned properties are stored:
   * <ul>
   * <li>timestamp - a Java numerical timestamp (Long)</li>
   * <li>uniqueId - a globally-unique identifier that can be used to query a
   * specific file (filenames themselves are not necessarily unique if >1
   * version has been uploaded</li>
   * </ul>
   *
   * @param file
   * @param renameTo
   * @param properties
   * @throws DBException
   */
  public void upload(File file, String renameTo, Map<String, ?> properties)
      throws DBException
  {
    try
    {
      logger.info("Writing GridFS file: "+file.getAbsolutePath()
          + ", length: " + file.length()
          + ". Writing with chunk size: " + chunkSize);
      GridFSInputFile gridFile = fs.createFile(file);

      //Add user-defined properties
      for (String key : properties.keySet())
      {
        gridFile.put(key, properties.get(key));
      }
      gridFile.put("timestamp", System.currentTimeMillis());
      gridFile.put("uniqueId", UidGenerator.generateUid());


      gridFile.setFilename(renameTo);
      gridFile.save(chunkSize);
      gridFile.validate();
    }
    catch(IOException e)
    {
      throw new DBException("Failed to read input file: "
          +file.getAbsolutePath(), e);
    }
  }

  private void download(GridFSDBFile gridFile, File downloadTo)
      throws DBException
  {
    File destinationDir = downloadTo.getParentFile();
    if (!destinationDir.exists())
    {
      throw new DBException("Specified destination directory does not exist: "
          + destinationDir.getAbsolutePath());
    }
    else if (!destinationDir.isDirectory())
    {
      throw new DBException("Specified destination 'directory' is not "
          + "actually a directory: "
          + destinationDir.getAbsolutePath());
    }
    else if (!destinationDir.canWrite())
    {
      throw new DBException("Specified destination directory is not writable: "
          + destinationDir.getAbsolutePath());
    }
    else if (downloadTo.exists())
    {
      throw new DBException("File already exists: "
          + downloadTo.getAbsolutePath());
    }

    try
    {
      logger.info("Writing: " + gridFile.getContentType()
          + ", date: " + gridFile.getUploadDate().toString()
          + ", type: " + gridFile.getContentType()
          + ", len: " + gridFile.getLength()
          + ", MD5: " + gridFile.getMD5()
          + ", Chunk size: " + gridFile.getChunkSize()
          + ", aliases: " + gridFile.getAliases()
//          + ", properties: " + gridFile.toMap()
          + ", to: " + downloadTo.getAbsolutePath());
      gridFile.writeTo(downloadTo);
    }
    catch(IOException e)
    {
      throw new DBException("Failed to retreive file: " 
          + gridFile.getFilename()+", MD5: "+gridFile.getMD5(), e);
    }
  }

  private GridFSDBFile findLatestGridFileWithName(String filename)
      throws DBException
  {
    
    try
    {
      BasicDBObject query = new BasicDBObject();
      query.put("filename", filename);

      List<GridFSDBFile> results = fs.find(query);
      GridFSDBFile gridFile = null;
      /*
       * We need to iterate over these manually, rather than performing a
       * generic query with sortBy(), because GridFSDBFile.putAll currently
       * throws an UnsupportedOperationException
       */
      for (GridFSDBFile file : results)
      {
        if (gridFile == null)
        {
          gridFile = file;
        }
        else
        {
          if ((Long) gridFile.get("timestamp") < (Long) file.get("timestamp"))
          {
            gridFile = file;
          }
        }
      }
      return gridFile;
    }
    catch(Exception e)
    {
      throw new DBException("GridFS query failure for file: " + filename, e);
    }
  }

  private GridFSDBFile findEarliestGridFileWithName(String filename)
      throws DBException
  {

    try
    {
      BasicDBObject query = new BasicDBObject();
      query.put("filename", filename);

      List<GridFSDBFile> results = fs.find(query);
      GridFSDBFile gridFile = null;
      /*
       * We need to iterate over these manually, rather than performing a
       * generic query with sortBy(), because GridFSDBFile.putAll currently
       * throws an UnsupportedOperationException
       */
      for (GridFSDBFile file : results)
      {
        if (gridFile == null)
        {
          gridFile = file;
        }
        else
        {
          if ((Long) gridFile.get("timestamp") > (Long) file.get("timestamp"))
          {
            gridFile = file;
          }
        }
      }
      return gridFile;
    }
    catch(Exception e)
    {
      throw new DBException("GridFS query failure for file: " + filename, e);
    }
  }

  /**
   * Downloads the specified file to a directory. <code>fileGuid</code> is the
   * GridFS filename to download, and the same name will be used for the local
   * copy.
   *
   * GridFS filenames are not necessarily unique. If there is more than one
   * GridFS entry with the same name, the latest (by timestamp) file will be
   * retrieved.
   *
   * @param destinationDir local directory to download the file to.
   * @param filename the GridFS filename
   * @throws DBException
   */
  public void downloadFile(File destinationDir, String filename)
      throws DBException
  {
    GridFSDBFile gridFile = null;
    try
    {
      //gridFile = fs.findOne(filename);
      gridFile = findLatestGridFileWithName(filename);
    }
    catch(Exception e)
    {
      throw new DBException("GridFS query failure for file: " + filename, e);
    }

    if (gridFile == null)
    {
      throw new DBException("Failed to find a file with the name: "+filename);
    }
    File output = new File(destinationDir, filename);
    download(gridFile, output);
  }


  /**
   *
   * @param destinationDir
   * @param filename
   * @param renameTo
   * @throws DBException
   */
  public void downloadFile(File destinationDir, String filename, String renameTo)
      throws DBException
  {
    GridFSDBFile gridFile = null;
    try
    {
//      gridFile = fs.findOne(filename);
      gridFile = findLatestGridFileWithName(filename);
    }
    catch(Exception e)
    {
      throw new DBException("GridFS query failure for file: " + filename, e);
    }

    if (gridFile == null)
    {
      throw new DBException("Failed to find a file with the name: "+filename);
    }
    File output = new File(destinationDir, renameTo);
    download(gridFile, output);
  }

  public int size()
      throws DBException
  {
    try
    {
      return fs.getFileList().count();
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to count files in DB: "+db.getName()+", bucket: "+bucket, e);

    }
  }

  public DBCursor list()
      throws DBException
  {
    try
    {
      //For now, just return the cursor - we could do something more abstract here though
      return fs.getFileList();
//      for (DBCursor cursor = fs.getFileList();
//           cursor.hasNext();)
//      {
//        DBObject gridFile = cursor.next();
//        gridFile.toMap();
//      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to list files in DB: "+db.getName()+", bucket: "+bucket, e);
  }
  }

}
