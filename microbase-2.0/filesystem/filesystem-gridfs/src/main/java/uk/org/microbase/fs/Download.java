/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 11-Nov-2010, 16:17:49
 */

package uk.org.microbase.fs;

import java.io.File;
import java.util.concurrent.Callable;

/**
 *
 * @author Keith Flanagan
 */
public class Download
    implements Callable<File>
{
  private final FileDAO fileDAO;
  private final File downloadDir;
  private String fileGuid;


  public Download(FileDAO fileDAO, File downloadDir)
  {
    this.fileDAO = fileDAO;
    this.downloadDir = downloadDir;
  }

  public Download(FileDAO fileDAO, File downloadDir, String fileGuid)
  {
    this.fileDAO = fileDAO;
    this.downloadDir = downloadDir;
    this.fileGuid = fileGuid;
  }

  @Override
  public File call()
      throws Exception
  {
    File tmpFile = new File(downloadDir, "-"+fileGuid);
//    fileDAO.downloadFileToAbsolutePath(fileGuid, tmpFile);
    fileDAO.downloadFile(tmpFile.getParentFile(), fileGuid, tmpFile.getName());

    //Once the file has downloaded correctly, rename to the proper GUID
    File destFile = new File(downloadDir, fileGuid);
    tmpFile.renameTo(destFile);
    return destFile;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final Download other = (Download) obj;
    if ((this.fileGuid == null) ? (other.fileGuid != null) : !this.fileGuid.equals(other.fileGuid))
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 3;
    hash = 37 * hash + (this.fileGuid != null ? this.fileGuid.hashCode() : 0);
    return hash;
  }

  public String getFileGuid()
  {
    return fileGuid;
  }

  public void setFileGuid(String fileGuid)
  {
    this.fileGuid = fileGuid;
  }
}
