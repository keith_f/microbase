#!/bin/bash

PROG_HOME="."
CLASSPATH=$PROG_HOME/target/classes
MVN="mvn $MVN_OPTS"

if [ ! -d $PROG_HOME/target/dependency ] ; then
  $MVN dependency:copy-dependencies
fi


for LIB in `find $PROG_HOME/target/dependency -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done


#echo $CLASSPATH

java -Xmx760M -Djava.net.preferIPv4Stack=true -cp $CLASSPATH uk.org.microbase.fs.FSClient $@

