/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */
package uk.org.microbase.filesystem.cli;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Logger;
import org.apache.commons.cli.*;
import uk.org.microbase.filesystem.s3.AmazonFS;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.filesystem.spi.MicrobaseFSFactory;

import static org.apache.commons.cli.OptionBuilder.*;
import uk.org.microbase.filesystem.spi.MBDirectory;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author Keith Flanagan
 */
public class FSClient
{
  private static final String DEFAULT_PROVIDER = AmazonFS.class.getName();

  private static ServiceLoader<MicrobaseFS> fsLoader = 
      ServiceLoader.load(MicrobaseFS.class);
  
  private static Logger logger = 
      Logger.getLogger(FSClient.class.getName());
  
  private static MicrobaseFS fs;
  
  private static void printHelpExit(Options options)
  {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "mb-filesystem.sh";
    String header = "";
    String footer = "";
    int width = 80;
    //formatter.printHelp( "notification.sh", options );
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) throws IOException, FSException
  {
    
    
    CommandLineParser parser = new PosixParser();

    Options options = new Options();
    options.addOption("u", "upload", false,
        "Upload a file or entire directory to the specified bucket/path");

    options.addOption("d", "download", false,
        "Download a file from the specified bucket/path to the local cache directory");
    
    options.addOption("D", "download-to-file", false,
        "Download a file from the specified bucket/path to a specified "
        + "path. This path must specify the destination path and filename");

    options.addOption("e", "exists-remote", false,
        "Determines whether a bucket/path/name exists at the remote data source");

    options.addOption("l", "list-remote", false,
        "Lists a remote bucket. If a path is given too, lists contents of "
        + "that specific path. "
        + "Directories are denoted by a trailing slash: foo/bar/");
    
    options.addOption("m", "list-remote-meta", false,
        "Lists the metadata of a file or files in a remote bucket/path. "
        + "If a 'name' is fiven too, only the details of that specific file are"
        + "returned. "
        + "Directories are not present in the returned result set.");
    
    /*
     * Configuration options for the above commands - some may be optional
     */
    
    options.addOption("b", "bucket", true,
        "Specifies a remote bucket to use");
    
    options.addOption("p", "path", true,
        "Specifies a remote path");
    
    options.addOption("n", "name", true,
        "Specifies a remote filename");
    
    options.addOption("f", "local-file-path", true,
        "Specifies a local file (used for upload/download target)");
    
    options.addOption("F", "force-redownload", false,
        "When downloading to a cache, forces re-download of existing files");
    options.addOption("C", "use-cache", false,
        "When downloading to a specific location, determines whether the "
        + "local file cache is used, or whether files are re-downloaded from"
        + "the remote location.");

    options.addOption("T", "tidy-paths", false,
        "Tidy and simplify paths.");

    /*
     * Amazon-specific options - allows specification of S3 access key and 
     * secret key via a command line option, rather than reading it from a 
     * file.
     */
    options.addOption("k", "aws-access-key", true, 
        "Allows an Amazon access key to be specified on the command line for "
        + "cases where a configuration file is not appropriate "
        + "(optional, applies only to Amazon S3 plugin)");
    options.addOption("s", "aws-secret-key", true, 
        "Allows an Amazon secret key to be specified on the command line for "
        + "cases where a configuration file is not appropriate "
        + "(optional, applies only to Amazon S3 plugin)");

    options.addOption(
         withLongOpt("tags")
        .withArgName("property=value" )
        .hasArgs(2)
        .withValueSeparator()
        .withDescription("used to tag files when uploading.")
        .create( "t" ));

    if (args.length == 0)
    {
      printHelpExit(options);
    }
    
    try
    { 
      CommandLine line = parser.parse(options, args);
     
      //FIXME this temporary initialisation needs to go elsewhere at some point
      if (line.hasOption("aws-access-key")) {
        String accessKey = getOptionValue(line);
        String secretKey = getSecretKey(line);
        
        Properties customConfig = new Properties();
        customConfig.setProperty("access_key", accessKey);
        customConfig.setProperty("secret_key", secretKey);
        customConfig.setProperty("cache_directory", ".");
        
        fs = new AmazonFS(customConfig);
        fs.configure(FSClient.class.getClassLoader());
      }
      else
      {
        fs = MicrobaseFSFactory.getProviderByClassName(DEFAULT_PROVIDER);
        printImpls();
      }

      // validate that block-size has been set
      if(line.hasOption("upload"))
      {
        upload(line);
      }
      else if (line.hasOption("download"))
      {
        downloadToCache(line);
      }
      else if (line.hasOption("download-to-file"))
      {
        downloadToSpecificLocation(line);
      }
      else if (line.hasOption("list-remote"))
      {
        listRemote(line);
      }
      else if (line.hasOption("list-remote-meta"))
      {
        listRemoteMeta(line);
      }
    }
    catch(ParseException e)
    {
      e.printStackTrace();
      printHelpExit(options);
    }
//    finally
//    {
//      System.out.println("\n\n\nDisconnecting from Microbase...");
//      runtime.shutdown();
//    }
  }

    private static void printImpls()
  {
    System.out.println("SPI Test **************************************");
    System.out.println("Found the following implementations:");
    for (MicrobaseFS impl : fsLoader) {
      System.out.println("Classname: " + impl.getClass().toString());
    }
     
    System.out.println("Using implementation: "+fs.getClass().getName());
  }
  
  private static void upload(CommandLine line) 
      throws FSException
  {
    logger.info("Uploading file");
    line.getOptionProperties("content").list(System.out);
    
    String bucket = getBucket(line);
    String path = getTidiedPath(line);
    String name = getTidiedName(line);
    File local = new File(getLocalFilePath(line));
    
    if (local.isFile())
    {
      logger.info("Uploading file: "+local.getAbsolutePath());
      if (name == null)
      {
        name = local.getName();
      }
      MBFile remoteFile = new MBFile(bucket, path, name);
      fs.upload(local, remoteFile, null);
    }
    else if (local.isDirectory())
    {
      logger.info("Uploading directory: "+local.getAbsolutePath());
      for (File f : listRecursively(local))
      {
        //Obtain just the part of the file path that occurs after the local
        //directory specified for upload
        String relativeLocalPath = f.getParentFile().getAbsolutePath().
            replace(local.getAbsolutePath(), "");
        relativeLocalPath = stripLeadingTrailingSlashes(relativeLocalPath);
        String remotePath = (path != null) 
            ? path+"/"+relativeLocalPath : relativeLocalPath;
        System.out.println("RemotePath: "+remotePath);
        remotePath = stripLeadingTrailingSlashes(remotePath);
        System.out.println("RemotePath2: "+remotePath);

        System.out.println("Uploading: "+f.getAbsolutePath() 
            + " to: "+bucket+"; "+remotePath+"; "+f.getName());
        
        MBFile remoteFile = new MBFile(bucket, path, f.getName());
        fs.upload(f, remoteFile, null);
      }
    }
    else
    {
      throw new FSException("Specified local path was not a file or a directory");
    }
  }

    private static String stripLeadingTrailingSlashes(String txt)
  {
    //Remove possible leading '/'
    txt = (txt.startsWith("/")) ? txt.substring(1) : txt;
    //Remove possible trailing '/'
    txt = (txt.endsWith("/")) ? txt.substring(0, txt.length()-1) : txt;
    return txt;
  }

  private static Set<File> listRecursively(File dir)
  {
    logger.info("Looking for files in: "+dir.getAbsolutePath());
    final File[] children = dir.listFiles();
    if(children != null)
    {
      Set<File> files = new HashSet<File>();
      for (File f : children)
      {
        if (f.isDirectory())
        {
          files.addAll(listRecursively(f));
        }
        else if (f.isFile())
        {
          files.add(f);
        }
      }
      return files;
    }
    else
    {
      return Collections.emptySet();
    }
  }
  
  private static void downloadToCache(CommandLine line) throws FSException, IOException
  {
    logger.info("Downloading file");
    line.getOptionProperties("content").list(System.out);
    
    String bucket = getBucket(line);
    String path = getPath(line);
    String name = getName(line);
//    File local = new File(line.getOptionValue("local-file-path", null));
    boolean forceRedownload = line.hasOption("force-redownload");
    
    MBFile remoteFile = new MBFile(bucket, path, name);
    InputStream is = fs.downloadToCache(remoteFile, forceRedownload);
    is.close();
  }
  
  private static void downloadToSpecificLocation(CommandLine line) 
      throws FSException
  {
    logger.info("Downloading file");
    line.getOptionProperties("content").list(System.out);
    
    String bucket = getBucket(line);
    String path = getPath(line);
    String name = getName(line);
    File local = new File(getLocalFilePath(line));
    boolean useCache = line.hasOption("use-cache");
    
    MBFile remoteFile = new MBFile(bucket, path, name);
    fs.downloadFileToSpecificLocation(remoteFile, local, useCache);
  }
  
  private static void listRemote(CommandLine line) throws FSException
  {
    logger.info("Listing remote bucket");
    line.getOptionProperties("content").list(System.out);
    
    String bucket = getBucket(line);
    String path = getPath(line);
    
    MBDirectory remoteDir = new MBDirectory(bucket, path);
    Set<String> names = fs.listRemoteFilenames(remoteDir, true, true);
    System.out.println("Files in bucket: "+bucket+", path: "+path);
    for (String name : names)
    {
      System.out.println("  * "+name);
    }
  }
  
  private static void listRemoteMeta(CommandLine line) throws FSException
  {
    logger.info("Listing remote bucket");
    line.getOptionProperties("content").list(System.out);
    
    String bucket = getBucket(line);
    String path = getPath(line);
    String name = getName(line);
    Set<FileMetaData> filesData = new HashSet<FileMetaData>();
    
    if (name == null)
    {
      MBDirectory remoteDir = new MBDirectory(bucket, path);
      filesData.addAll(fs.listRemoteFileMetaData(remoteDir));
    }
    else
    {
      MBFile remoteFile = new MBFile(bucket, path, name);
      filesData.add(fs.getRemoteFileMetaData(remoteFile));
    }

    System.out.println("Files in bucket: "+bucket+", path: "+path);
    for (FileMetaData fileData : filesData)
    {
      StringBuilder sb = new StringBuilder();
      sb.append("File: ").append(fileData.getLocation())
          .append(", Size: ").append(fileData.getLength())
          .append(", Modified: ").append(fileData.getLastModified().toString());
      System.out.println(sb);
    }
  }

  private static String tidyPath(CommandLine line, String path, boolean stripLeading, boolean stripTrailing) {
    if(getTidyPaths(line)) {
      return doTidyPath(path, stripLeading, stripTrailing);
    } else {
      return path;
    }
  }

  private static String stripLeading(String path) {
    return (path.startsWith("/")) ? stripLeading(path.substring(1)) : path;
  }

  private static String stripTrailing(String path) {
    return (path.endsWith("/")) ? stripTrailing(path.substring(0, path.length() - 1)) : path;
  }

  private static String doTidyPath(String path, boolean stripLeading, boolean stripTrailing) {
    if(stripLeading) path = stripLeading(path);
    if(stripTrailing) path = stripTrailing(path);

    return path.replaceAll("\\./", "").replaceAll("/+", "/");
  }

  private static boolean getTidyPaths(CommandLine line) {
    return line.hasOption("tidy-paths");
  }

  private static String getLocalFilePath(CommandLine line) {
    return line.getOptionValue("local-file-path", null);
  }

  @SuppressWarnings("unused")
  private static String getTidiedLocalFilePath(CommandLine line) {
    return tidyPath(line, getLocalFilePath(line), false, true);
  }

  private static String getPath(CommandLine line) {
    return line.getOptionValue("path", null);
  }

  private static String getTidiedPath(CommandLine line) {
    return tidyPath(line, getPath(line), true, true);
  }

  private static String getName(CommandLine line) {
    return line.getOptionValue("name", null);
  }

  private static String getTidiedName(CommandLine line) {
    return tidyPath(line, getName(line), true, true);
  }

  private static String getBucket(CommandLine line) {
    return line.getOptionValue("bucket", null);
  }

  private static String getSecretKey(CommandLine line) {
    return line.getOptionValue("aws-secret-key", null);
  }

  private static String getOptionValue(CommandLine line) {
    return line.getOptionValue("aws-access-key", null);
  }
}
