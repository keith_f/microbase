/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.shell;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;

/**
 * A custom classloader to dynamically load responders and their dependencies
 * from a Microbase shell.
 * 
 * @author Keith Flanagan
 */
public class ResponderClassLoaderFactory
{
  
  public static ClassLoader createClassLoader(File jarBaseDir)
          throws MalformedURLException, IOException
  {
    if (!jarBaseDir.exists() || !jarBaseDir.isDirectory()) {
      throw new IOException("Either "+jarBaseDir.getAbsolutePath()+
              " does not exist, or is not a directory");
    }
    File[] jars = jarBaseDir.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".jar");
      }
    });
    
    List<URL> urls = new LinkedList<>();
    urls.add(jarBaseDir.getAbsoluteFile().toURI().toURL());
    for (File jar  : jars) {
      urls.add(jar.toURI().toURL());
    }

    /*
     * This ensures that the any core Microbase jars (such as filesystem
     * implementation jars) that are not necessarilly direct dependencies of
     * a responder are also present on the classpath.
     */
    ClassLoader parentClassloader = ResponderClassLoaderFactory.class.getClassLoader();
    
    /*
     * This is required in order to dynamically classload things from the 
     * specified directory
     */
    URLClassLoader classLoader = new URLClassLoader(urls.toArray(new URL[0]), parentClassloader);
    return classLoader;
  }
}
