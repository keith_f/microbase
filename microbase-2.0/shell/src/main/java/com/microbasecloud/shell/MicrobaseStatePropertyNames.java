/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.shell;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Keith Flanagan
 */
public class MicrobaseStatePropertyNames
{
  /*
   * Special state property names
   */
  public static String PROP_RCLF_RESPONDER_JAR_DIR = "responder_classpath";
  public static String PROP_MP_CPU_PERMITS = "MessageProcessor.cpu_permits";
  
  
  public static String PROP_S3_ACCESS_KEY = "aws.access_key";
  public static String PROP_S3_SECRET_KEY = "aws.secret_key";
  
  public static String PROP_MB_NOT_DB = "microbase.notification.jdbc";
  public static String PROP_MB_NOT_DB_USER = "microbase.notification.jdbc.user";
  public static String PROP_MB_NOT_DB_PASSWORD = "microbase.notification.jdbc.password";

  public static String PROP_MB_MBFS_IMPLEMENTATION = "microbase.mbfs.classname";
  
  public static Map<String, String> getDefaultPropertySettings()
  {
    Map<String, String> defaults = new HashMap<>();
    defaults.put(PROP_RCLF_RESPONDER_JAR_DIR, "./jars");
    defaults.put(PROP_MP_CPU_PERMITS, "1");

    defaults.put(PROP_S3_ACCESS_KEY, "<your key here>");
    defaults.put(PROP_S3_SECRET_KEY, "<your key here>");
    
    defaults.put(PROP_MB_NOT_DB, "uk.org.microbase.filesystem.s3.AmazonFS");
    defaults.put(PROP_MB_NOT_DB_USER, "microbase");
    defaults.put(PROP_MB_NOT_DB_PASSWORD, "microbase");
    
    defaults.put(PROP_MB_MBFS_IMPLEMENTATION, "uk.org.microbase.filesystem.s3.AmazonFS");

    
    return defaults;
  }
  
}
