/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.shell;

import static com.microbasecloud.shell.MicrobaseStatePropertyNames.*;
import static uk.org.microbase.responder.spi.AbstractMessageProcessor.PROP__MAIN_MSG_CONTENT;

import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;
import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import com.torrenttamer.jdbcutils.DatabaseConfiguration;
import com.torrenttamer.jdbcutils.DatabaseConfigurationException;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.DistributedJdbcPoolFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;
import uk.org.microbase.client.MicrobaseClientException;
import uk.org.microbase.client.ResponderLoader;
import uk.org.microbase.configuration.ComputeNodeUtils;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.dist.processes.DistributedProcessList;
import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.processes.ProcessListException;
import uk.org.microbase.dist.queues.MessageQueueException;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.db.MicrobaseNotificationSQLImpl;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.notification.spi.NotificationException;
import uk.org.microbase.responder.spi.MessageProcessor;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.runtime.NodeInfo;

/**
 * A simple interactive command line shell for Microbase. This program may be
 * useful for debugging responders and/or monitoring a live system.
 * 
 * @author Keith Flanagan
 */
public class MicrobaseShell
{
  private static final Logger logger =
          Logger.getLogger(MicrobaseShell.class.getName());
  
  private static final String NOTIFICATION_CONFIG_RESOURCE = "NotificationSqlConfiguration.properties";
  
  private final String nodeId;
  
  private final ShellState state;
  private final Map<String, Object> storedObjects;
  
  private ClassLoader responderClassloader;
  private ClientRuntime runtime;
  private MicrobaseNotification notif;
  
  public static void main(String[] args) throws IOException, ConfigurationException
  {
    String nodeId = ComputeNodeUtils.getHostname()+"."+Math.random()+".MicrobaseShell";
//    runtime = GlobalConfiguration.getDefaultRuntime();
    StateUtils stateUtils = new StateUtils();
    ShellState state = stateUtils.loadStateIfExists();
    
    MicrobaseShell mbsh = new MicrobaseShell(nodeId, state);
    
    
    Shell shell = ShellFactory.createConsoleShell(
            "microbase", "Microbase 2.0", mbsh);
    shell.setDisplayTime(true);
    shell.commandLoop();
    
  }
  
  public MicrobaseShell(String nodeId, ShellState state)
  {
    storedObjects = new HashMap<>();
    this.nodeId = nodeId;
    this.state = state;
  }
  
  /*
   * Commands.
   */
  @Command
  public String help() {
    return "\n\n"
      + "--------------------------------------------------------------------------------\n"
      + "Microbase 2.0 Shell supported commands\n"
      + "======================================\n"
      + "\n"
      + "Notification system\n"
      + "-------------------\n"
      + "  publish <topic> [key=val ... ]\n"
      + "\n\n";
    
  }
  
  @Command
  public String save() throws IOException {
    StateUtils stateUtils = new StateUtils();
    stateUtils.saveState(state);
    
    return "Saved.";
  }
  
  @Command
  public String propSet(String name, String value) {
    String old = state.getProperties().put(name, value);
    
    if (old == null) {
      return "Setting "+name+" --> "+value;
    } else {
      return "Setting "+name+" --> "+value + " (old value was: "+old+")";
    }
  }
  
  @Command
  public String propUnset(String name) {
    String old = state.getProperties().remove(name);
    
    if (old == null) {
      return "Unsetting" + name;
    } else {
      return "Unsetting " + name + " (old value was: "+old+")";
    }
  }
  
  @Command
  public String propGet(String name) {
    return state.getProperties().get(name);
  }
  
  @Command
  public String propList() {
    StringBuilder text = new StringBuilder();
    List<String> names = new ArrayList<>(state.getProperties().keySet());
    Collections.sort(names);
    for (String name : names) {
      String val = state.getProperties().get(name);
      text.append("    ").append(name).append(" ---> ").append(val).append("\n");
    }
    text.append("Total properties set: ").append(state.getProperties().size()).append("\n");
    return text.toString();  
  }
  
  @Command
  public String objectGet(String name) {
    return storedObjects.get(name).toString();
  }
  
  @Command
  public String objectList() {
    StringBuilder text = new StringBuilder();
    List<String> names = new ArrayList<>(storedObjects.keySet());
    Collections.sort(names);
    for (String name : names) {
      Object val = storedObjects.get(name);
      text.append("    ").append(name).append(" ---> ").append(val).append("\n");
    }
    text.append("Total stored objects: ").append(storedObjects.size()).append("\n");
    return text.toString();  
  }

  /*
   * ---------------------------------------------------------------------------
   * Notification commands
   * ---------------------------------------------------------------------------
   */
  @Command
  public void setManualNotificationConfiguration()
          throws ConfigurationException, DatabaseConfigurationException, UnknownHostException
  {
    System.out.println("Setting notification database settings based on "
            + "in-memory properties: \n"
            + " URL: " + state.getProperties().get(PROP_MB_NOT_DB));
    Properties props = new Properties();
    //These property names are expected by DistributedJdbcPoolFactory
    props.setProperty("db_url_readonly", state.getProperties().get(PROP_MB_NOT_DB));
    props.setProperty("db_url_writable", state.getProperties().get(PROP_MB_NOT_DB));
    props.setProperty("username", state.getProperties().get(PROP_MB_NOT_DB_USER));
    props.setProperty("password", state.getProperties().get(PROP_MB_NOT_DB_PASSWORD));
    
    DatabaseConfiguration dbConf = DistributedJdbcPoolFactory.readConfigurationFromProperties(props);
    DistributedJdbcPool pool = DistributedJdbcPoolFactory.createSingleWriteMultipleReplicaPool(dbConf);
    
    notif = new MicrobaseNotificationSQLImpl(nodeId, pool);
  }
  
  @Command
  public void setClasspath(String classPathDir) 
          throws UnknownHostException, MalformedURLException, IOException
  {
    System.out.println("Setting classpath directory to: "+classPathDir);
    state.getProperties().put(PROP_RCLF_RESPONDER_JAR_DIR, classPathDir);
    reloadConfig();
  }
  
  private ClassLoader getClassLoader() throws MalformedURLException, IOException
  {
    String classPathDir = state.getProperties().get(PROP_RCLF_RESPONDER_JAR_DIR);
    ClassLoader cl = ResponderClassLoaderFactory.createClassLoader(new File(classPathDir));
    return cl;
  }
  
  @Command
  public void reloadConfig() 
          throws UnknownHostException, MalformedURLException, IOException
  {
    String classPathDir = state.getProperties().get(PROP_RCLF_RESPONDER_JAR_DIR);
    System.out.println("(Re)loading config from: "+classPathDir);
    ClassLoader cl = getClassLoader();

    try {
      System.out.println("Reading resource: "+NOTIFICATION_CONFIG_RESOURCE);
      DatabaseConfiguration dbConf = DistributedJdbcPoolFactory.
            readConfigurationFromPropertyResource(cl, NOTIFICATION_CONFIG_RESOURCE);
      DistributedJdbcPool pool = DistributedJdbcPoolFactory.createSingleWriteMultipleReplicaPool(dbConf);
      notif = new MicrobaseNotificationSQLImpl(nodeId+".MicrobaseShell", pool);
      System.out.println("Successfully configured the notification system.");
      
      if (runtime != null) {
        System.out.println("Shutting down previous Runtime");
        runtime.shutdown();
      }
      
      System.out.println("Creating new runtime");
      responderClassloader = cl;
      GlobalConfiguration config = GlobalConfiguration.loadDefaultConfig(cl);
      config.configureAll();
      runtime = config.getRuntime();
      
      System.out.println("Runtime is now configured");
    }
    catch(Exception e) {
      System.out.println("Failed to configure the Notification system based on "
              + "the configuration files in this directory: "+e.getMessage());
    }
    
  }
  
  @Command
  public void listMessageGenerators() throws MalformedURLException, IOException {
    ClassLoader  cl = getClassLoader();
    ServiceLoader<ShellMsgGenerator> generatorLoader = 
            ServiceLoader.load(ShellMsgGenerator.class, cl);
    generatorLoader.reload();
    StringBuilder sb = new StringBuilder();
    for (ShellMsgGenerator msgGen : generatorLoader) {
      sb.append(msgGen.getClass().getName()).append(": Generates messages for responder: ");
      Map<String, String> propToDesc = msgGen.getReqPropertyNames();
      sb.append(msgGen.getMessageProcessorClassname()).append(":\n");
      for (String propName : propToDesc.keySet()) {
        String descr = propToDesc.get(propName);
        sb.append("  + ").append(propName).append(": ").append(descr).append("\n");
      }
      sb.append("\n");
    }
    System.out.println(sb);
  }
  
  @Command
  public String runMessageContentGenerator(String className) 
          throws ShellMsgGeneratorException, MalformedURLException, IOException
  {
    ClassLoader  cl = getClassLoader();
    ServiceLoader<ShellMsgGenerator> generatorLoader = 
            ServiceLoader.load(ShellMsgGenerator.class, cl);
    generatorLoader.reload();
    boolean useSimpleName = false;
    if (!className.contains(".")) {
      System.out.println("'Classname' "+className+" doesn't contain any dots. "
              + "Using 'simple name' matching.");
      useSimpleName = true;
    }
    ShellMsgGenerator chosen = null;
    for (ShellMsgGenerator msgGen : generatorLoader) {
      if (msgGen.getClass().getName().equals(className)) {
        chosen = msgGen;
        break;
      } else if (useSimpleName && msgGen.getClass().getName().endsWith(className)) {
        chosen = msgGen;
        break;
      }
    }
    
    if (chosen == null) {
      System.out.println("No message generator was found with the name: "+className);
      return null;
    }
    
    System.out.println("Running generator: "+className);
    String json = chosen.generateJson(state.getProperties());
    return json;
  }
  
  @Command
  public void runMessageContentGeneratorToFile(String className, File output) 
          throws ShellMsgGeneratorException, IOException
  {
    String json = runMessageContentGenerator(className);
    if (json == null) {
      return;
    }
    
    System.out.println("Writing JSON encoded message content to file: "+output.getAbsolutePath());
    try (FileWriter fw = new FileWriter(output)) {
      fw.append(json).append("\n");
      fw.flush();
    }
  }
  
  @Command
  public void runMessageContentGeneratorToObject(String className, String objectName) 
          throws ShellMsgGeneratorException, IOException
  {
    String json = runMessageContentGenerator(className);
    if (json == null) {
      return;
    }
    
    System.out.println("Writing JSON encoded message content to in-memory object: "+objectName);
    storedObjects.put(objectName, json);
  }
  
  @Command
  public void msgPublish(String topic, String... content) throws NotificationException {
    if (notif == null) {
      System.out.println("Notification system is not currently configured. "
              + "Please configure the system with set-classpath or "
              + "set-manual-notification-configuration.");
      return;
    }
    String publisherGuid = nodeId;
    Map<String, String> contentMap = new HashMap<>();
    for (String contentItem : content) {
      String[] tokens = contentItem.split("=");
      contentMap.put(tokens[0], tokens[1]);
    }
    System.out.println("Publishing message:\n"
            + "  * Topic: "+topic+"\n"
            + "  * Content map: "+contentMap);
    
    notif.publish(publisherGuid, topic, contentMap, null);
    System.out.println("Published message successfully.");
  }
  
  @Command
  public void msgPublishFromObj(String topic, String objectName)
          throws NotificationException {
    if (notif == null) {
      System.out.println("Notification system is not currently configured. "
              + "Please configure the system with set-classpath or "
              + "set-manual-notification-configuration.");
      return;
    }
    String publisherGuid = nodeId;
    Object object = storedObjects.get(objectName);
    Map<String, String> contentMap = new HashMap<>();
    contentMap.put(PROP__MAIN_MSG_CONTENT, object.toString());
    
    System.out.println("Publishing message:\n"
            + "  * Topic: "+topic+"\n"
            + "  * Content map: "+contentMap);
    
    notif.publish(publisherGuid, topic, contentMap, null);
    System.out.println("Published message successfully.");
  }
  
  @Command
  public void msgPublishFromObj(String topic, String objectName, int numCopiesToPublish)
          throws NotificationException {
    for (int i=0; i<numCopiesToPublish; i++) {
      System.out.println("Publishing message "+i+" of "+numCopiesToPublish);
      msgPublishFromObj(topic, objectName);
    }
  }
  
  @Command
  public void msgListAll() throws NotificationException {
    if (notif == null) {
      System.out.println("Notification system is not currently configured. "
              + "Please configure the system with set-classpath or "
              + "set-manual-notification-configuration.");
      return;
    }
    
    List<Message> msgs = notif.listMessagesInTimestampOrder(0, Integer.MAX_VALUE);
    System.out.println("Listing all messages:");
    for (Message msg : msgs) {
      StringBuilder sb = new StringBuilder("* ");
      appendMessageContent(sb, msg);
      sb.append("\n");
      System.out.print(sb);
    }
    System.out.println("Total messages: "+msgs.size());
  }
  
  @Command
  public void msgListRoots(boolean includeChildren) throws NotificationException {
    if (notif == null) {
      System.out.println("Notification system is not currently configured. "
              + "Please configure the system with set-classpath or "
              + "set-manual-notification-configuration.");
      return;
    }
    
    List<Message> msgs = notif.listRootMessages(false, 0, Integer.MAX_VALUE);
    System.out.println("Message tree:");
    for (Message rootMsg : msgs) {
      StringBuilder sb = new StringBuilder("* ");
      appendMessageContent(sb, rootMsg);
      sb.append("\n");
      if (includeChildren) {
        printChildMessagesFor(sb, rootMsg, 2, true);
      }
      
      System.out.println(sb);
    }
    System.out.println("Total root messages: "+msgs.size());
  }
  
  private void appendMessageContent(StringBuilder sb, Message msg)
  {
    sb.append(msg.getTopicGuid()).append(": ");
    sb.append(msg.getWorkflowExeId()).append("/");
    sb.append(msg.getWorkflowStepId());
    sb.append(", ID: ").append(msg.getGuid());
    sb.append(" ").append(new Date(msg.getPublishedTimestamp()));
    sb.append(" by ").append(msg.getPublisherGuid());
  }
  
  private void _appendIndent(StringBuilder sb, int indent)
  {
    for (int i=0; i<indent; i++) {
      sb.append(indent);
    }
  }
  private void printChildMessagesFor(StringBuilder sb, Message parent, int indent, boolean recursive) throws NotificationException
  {
    for (Message childMsg : notif.listChildMessages(parent.getGuid(), false, 0, Integer.MAX_VALUE))
    {
      _appendIndent(sb, indent);
      appendMessageContent(sb, childMsg);
      
      if (recursive) {
        printChildMessagesFor(sb, parent, indent+2, recursive);
      }
    }
  }
  
  @Command
  public void msgListInState(String responderGuid, String msgStateStr) throws NotificationException {
    if (notif == null) {
      System.out.println("Notification system is not currently configured. "
              + "Please configure the system with set-classpath or "
              + "set-manual-notification-configuration.");
      return;
    }
    Message.State msgState = Message.State.valueOf(msgStateStr);
    Set<String> msgIds = notif.findMessagesInState(responderGuid, msgState, Integer.MAX_VALUE);
    System.out.println("Messages in state: "+msgState);
    Map<String, Message> idToMsg = notif.getMessages(msgIds);
    
    for (Message msg : idToMsg.values()) {
      StringBuilder sb = new StringBuilder("* ");
      appendMessageContent(sb, msg);
      sb.append("\n");
      System.out.println(sb);
    }
    System.out.println("Total messages displayed: "+msgIds.size());
  }
  
  @Command
  public void msgChangeState(String messageGuid, String responderGuid, String newMsgStateStr) throws NotificationException 
  {
    if (notif == null) {
      System.out.println("Notification system is not currently configured. "
              + "Please configure the system with set-classpath or "
              + "set-manual-notification-configuration.");
      return;
    }
    Message.State newMsgState = Message.State.valueOf(newMsgStateStr);
    System.out.println("Setting the state of message: "+messageGuid+" to "
            + newMsgState+" for responder: "+responderGuid);
    notif.setMessageState(messageGuid, responderGuid, newMsgState);
  }
  
  @Command
  public void msgListPossibleStates()
  {
    StringBuilder sb = new StringBuilder("Available message states:\n");
    
    for (Message.State state : Message.State.values())
    {
      sb.append("  * ").append(state.name()).append("\n");
    }
    System.out.println(sb);
  }
  
  
  /*
   * Process monitor
   */
  @Command
  public void responderQueues() 
          throws MalformedURLException, IOException, ConfigurationException, 
          MessageQueueException, RegistrationException
  {
    if (runtime == null) {
      System.out.println("You must first run reload-config");
      return;
    }
    StringBuilder sb = new StringBuilder("Responder queue sizes:\n");
    for (ResponderInfo info : runtime.getRegistration().getRegisteredResponders()) {
      String responderId = info.getResponderId();
      sb.append(responderId).append("\n");
      int qSize = runtime.getMessageQueues().getResponderJobQueueSize(responderId);
      sb.append("    - queue size: ").append(qSize).append("\n");
    }
    
    System.out.println(sb);
  }
  
  @Command
  public void responderCores() 
          throws MalformedURLException, IOException, ConfigurationException, 
          MessageQueueException, RegistrationException, ProcessListException
  {
    if (runtime == null) {
      System.out.println("You must first run reload-config");
      return;
    }
    DistributedProcessList procList = runtime.getProcessList();
    
    StringBuilder sb = new StringBuilder("Responder allocated CPUs:\n");
    for (ResponderInfo info : runtime.getRegistration().getRegisteredResponders()) {
      String responderId = info.getResponderId();
      sb.append(responderId).append("\n");
      int qSize = procList.countDistributedCoresForResponder(responderId);
      sb.append("    - total cores in use: ").append(qSize).append("\n");
    }
    
    System.out.println(sb);
  }
  
  @Command
  public void activeProcesses() 
          throws MalformedURLException, IOException, ConfigurationException, 
          MessageQueueException, RegistrationException, ProcessListException
  {
    if (runtime == null) {
      System.out.println("You must first run reload-config");
      return;
    }
    DistributedProcessList procList = runtime.getProcessList();
    
    StringBuilder sb = new StringBuilder("Active processes:\n");
    Collection<ProcessInfo> active = procList.getAllProcesses();
    
    for (ProcessInfo ps : active) {
      sb.append(" * ");
      sb.append(ps.getProcessGuid()).append(" ");
      sb.append(new Date(ps.getWorkStartedAtMs())).append(" ");
      sb.append(ps.getTopicId()).append(" ");
      sb.append(ps.getResponderId()).append(" ");
      sb.append(ps.getPercentComplete()).append(" ");
      sb.append(ps.getAssignedLocalCores()).append(" ");
      sb.append(ps.getMessageId()).append(" ");
      sb.append(ps.getHostname()).append(" ");
      sb.append("\n");
    }
    
    System.out.println(sb);
  }
  
  
  
  /*
   * ---------------------------------------------------------------------------
   * Playing around
   * ---------------------------------------------------------------------------
   */
  
  @Command
  public String startSubshell() throws IOException {
    Shell shell = ShellFactory.createConsoleShell(
            "microbase|notification", "Microbase 2.0 notification system subshell", new SubShell());
    shell.setDisplayTime(true);
    shell.commandLoop();
    return "Done.";
  }
  
  
  /*
   * ---------------------------------------------------------------------------
   * Compute client commands
   * ---------------------------------------------------------------------------
   */
  
  @Command
  public String listResponders() throws IOException, MicrobaseClientException {
    return listResponders(state.getProperties().get(PROP_RCLF_RESPONDER_JAR_DIR));
  }
  
  @Command
  public String listResponders(String baseDirName) 
          throws IOException, MicrobaseClientException {
    StringBuilder txt = new StringBuilder();

    File baseDir = new File(baseDirName);
    ClassLoader cl = ResponderClassLoaderFactory.createClassLoader(baseDir);
    ResponderLoader loader = new ResponderLoader(cl);
    Set<MessageProcessor> msgsProcs = loader.getMessageProcessorProviders();
    
    txt.append("Available MessageProcessor implementations:\n");
    for (MessageProcessor msgProc : msgsProcs) {
      txt.append("  * ").append(msgProc.getClass().getName()).append("\n");
    }
    return txt.toString();
  }

  
  @Command
  public void printResponderInfo(String msgProcessorClassname)
          throws RegistrationException, Exception
  {
    ClassLoader cl = getClassLoader();
    
    int cpus = Integer.parseInt(state.getProperties().get(PROP_MP_CPU_PERMITS));
    Semaphore dummyPermits = new Semaphore(cpus);
    
    GlobalConfiguration config = GlobalConfiguration.loadDefaultConfig(cl);
    config.configureAll();
    ClientRuntime runtime = config.getRuntime();
    
    logger.info("Loading class: "+msgProcessorClassname);
    Class msgProcessorClass = cl.loadClass(msgProcessorClassname);
    logger.info("Creating new responder instance");
    MessageProcessor processor = (MessageProcessor) msgProcessorClass.newInstance();
    logger.info("Configuring processor ...");
    processor.setClientRuntime(runtime);
    System.out.println("Runtime: "+runtime);
    processor.setCpuPermits(dummyPermits);
    processor.initialise();
    
    logger.info("Instructing local responder to create a ResponderInfo");
    ResponderInfo localInfo = processor.acquireResponderInfo();
    System.out.println(localInfo);
    
    logger.info("Also, determining if there's already a remote ResponderInfo in the cluster");
    ResponderInfo remoteInfo = runtime.getRegistration().getResponderInfo(localInfo.getResponderId());
    System.out.println(remoteInfo);
    
//    logger.info("Calling MessageProcessor");
//    processor.call();
    
    logger.info("Shutting down the Microbase Runtime");
    runtime.shutdown();
    
   
    System.out.println("Done.");
  }
  
  @Command
  public void startClient(
          @Param(name="msgProcessorClassname", description="Fully qualified classname of a MessageProcessor implementation")
          String msgProcessorClassname)
          throws RegistrationException, Exception
  {
    ClassLoader cl = getClassLoader();
    
    int cpus = Integer.parseInt(state.getProperties().get(PROP_MP_CPU_PERMITS));
    Semaphore dummyPermits = new Semaphore(cpus);
    
    GlobalConfiguration config = GlobalConfiguration.loadDefaultConfig(cl);
    config.configureAll();
    ClientRuntime runtime = config.getRuntime();
    
    logger.info("Loading class: "+msgProcessorClassname);
    Class msgProcessorClass = cl.loadClass(msgProcessorClassname);
    logger.info("Creating new responder instance");
    MessageProcessor processor = (MessageProcessor) msgProcessorClass.newInstance();
    logger.info("Configuring processor ...");
    processor.setClientRuntime(runtime);
    processor.setCpuPermits(dummyPermits);
    processor.initialise();
    
    logger.info("Calling MessageProcessor");
    processor.call();
    
    logger.info("Shutting down the Microbase Runtime");
    runtime.shutdown();
    
   
    System.out.println("Done.");
  }
  
  @Command
  public void startClient(
          @Param(name="msgProcessorClassname", description="Fully qualified classname of a MessageProcessor implementation")
          String msgProcessorClassname, 
          @Param(name="iterations", description="The number of times to execute the responder before returning to the shell")
          int iterations)
          throws RegistrationException, Exception
  {
    ClassLoader cl = getClassLoader();
    
    int cpus = Integer.parseInt(state.getProperties().get(PROP_MP_CPU_PERMITS));
    Semaphore dummyPermits = new Semaphore(cpus);
    
    GlobalConfiguration config = GlobalConfiguration.loadDefaultConfig(cl);
    config.configureAll();
    ClientRuntime runtime = config.getRuntime();
    
    for (int i=0; i<iterations; i++) {
      logger.info("Loading class: "+msgProcessorClassname);
      Class msgProcessorClass = cl.loadClass(msgProcessorClassname);
      logger.info("Creating new responder instance");
      MessageProcessor processor = (MessageProcessor) msgProcessorClass.newInstance();
      logger.info("Configuring processor ...");
      processor.setClientRuntime(runtime);
      processor.setCpuPermits(dummyPermits);
      processor.initialise();

      logger.info("Calling MessageProcessor");
      processor.call();
    }
    
    logger.info("Shutting down the Microbase Runtime");
    runtime.shutdown();
    
   
    System.out.println("Done.");
  }
}
