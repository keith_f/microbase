/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-Aug-2010, 15:43:16
 */

package uk.org.microbase.configuration;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.runtime.NodeInfo;

/**
 *
 * @author Keith Flanagan
 */
public class ComputeNodeUtils
{
  static NodeInfo obtainNodeInformation()
      throws ConfigurationException
  {
    try
    {
      String hostname = getHostname();
      String osName = System.getProperty("os.name");
      String osArch = System.getProperty("os.arch");
      String osVersion = System.getProperty("os.version");
      int numCpus = 1;
      numCpus = getCpus();
      long jvmTotalRam = getRam();
      String javaVersion = System.getProperty("java.version");
      String javaVmVersion = System.getProperty("java.vm.version");
      String javaVmVendor = System.getProperty("java.vm.vendor");

      long availDisk = getFreeDisk();

      NodeInfo info = new NodeInfo();
      info.setCores(numCpus);
      info.setHostname(hostname);
      info.setJavaVersion(javaVersion);
      info.setJavaVmVendor(javaVmVendor);
      info.setJavaVmVersion(javaVmVersion);
      info.setJvmTotalRamSize(jvmTotalRam);
      info.setOsArch(osArch);
      info.setOsName(osName);
      info.setOsVersion(osVersion);

      return info;
    }
    catch (Exception e)
    {
      throw new ConfigurationException(
          "Failed to retreive node hardware properties", e);
    }
  }

  public static int getCpus()
  {
    return Runtime.getRuntime().availableProcessors();
  }

  public static long getRam()
  {
    return Runtime.getRuntime().maxMemory();
  }

  public static long getFreeDisk()
  {
    String tmpSpace = System.getProperty("java.io.tmpdir");
    File tmpDir = new File(tmpSpace);
    return tmpDir.getUsableSpace();
  }

  public static String getHostname() throws UnknownHostException
  {
    InetAddress local = InetAddress.getLocalHost();
    return local.getHostName();
  }
}
