/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.org.microbase.configuration;

import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.logging.DistributedLogger;
import uk.org.microbase.dist.logging.hz_psql.DistributedLoggerHzImpl;
import uk.org.microbase.dist.processes.DistributedProcessList;
import uk.org.microbase.dist.processes.ExecutionResultLog;
import uk.org.microbase.dist.processes.hz_psql.DistributedProcessListHzSqlImpl;
import uk.org.microbase.dist.processes.hz_psql.DistributedProcessListSqlConfiguration;
import uk.org.microbase.dist.processes.hz_psql.ExecutionResultLogSqlImpl;
import uk.org.microbase.dist.queues.hz_psql.ResponderMessageQueuesHzImpl;
import uk.org.microbase.dist.queues.ResponderMessageQueues;
import uk.org.microbase.filesystem.spi.MicrobaseFSFactory;
import uk.org.microbase.notification.db.MicrobaseNotificationSQLImpl;
import uk.org.microbase.notification.db.NotificationSqlConfiguration;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.dist.responder.MicrobaseRegistration;
import uk.org.microbase.dist.responder.MicrobaseRegistrationHzImpl;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.runtime.NodeInfo;

/**
 * Utility class for accessing Microbase installation specific configuration
 * options, such as core database hosts, database names, etc.
 *
 * This class configures a ClientRuntime object which may then be used by
 * a microbase client.
 * 
 * @author Keith Flanagan
 */
public class GlobalConfiguration
{
  private static final Logger logger =
      Logger.getLogger(GlobalConfiguration.class.getName());

  private static final String CONFIG_RESOURCE =
      //"/" + GlobalConfiguration.class.getSimpleName() + ".properties";
          GlobalConfiguration.class.getSimpleName() + ".properties";
  
  private static final String HAZELCAST_CONFIG_RESOURCE = "microbase-hazelcast.xml";


//  private static final String PROP_DOWNLOAD_DIR = "download_directory";
//  private static final String PROP_JOB_INSTALL_DIR = "job_installation_directory";

  private static final String PROP_NUM_CONCURRENT_JOBS = "max_concurrent_jobs";
  private static final String PROP_MAX_RESPONDER_QUEUE_SIZE = "max_job_queue_size";

  
  private static final String PROP_DEFAULT_MBFS_IMPL = "default_mbfs_implementation";
  
  private static final String ENV_MICROBASE_HOME = "MICROBASE_HOME";
  private static final String ENV_MICROBASE_LOG = "MICROBASE_LOG";
  private static final String ENV_MICROBASE_CONFIG = "MICROBASE_CONFIG";
  private static final String ENV_MICROBASE_SCRATCH = "MICROBASE_SCRATCH";
  private static final String ENV_RESPONDER_DATA = "RESPONDER_DATA";
  private static final String ENV_RESPONDER_JARS = "RESPONDER_JARS";
  
  private static final String JOB_DIRNAME = "jobs_tmp";
  

  private static GlobalConfiguration singleton;

  private Properties config;
  private final ClientRuntime runtime;

  public GlobalConfiguration(ClassLoader classLoader)
  {
    /* 
     * See here for notes on loading configuration:
     * http://www.hazelcast.com/docs/1.9.4/manual/multi_html/ch11.html
     */
    
    Config hzConfig = new ClasspathXmlConfig(classLoader, HAZELCAST_CONFIG_RESOURCE);
    HazelcastInstance hazelcast = Hazelcast.newHazelcastInstance(hzConfig);
    
    runtime = new ClientRuntime(classLoader, hazelcast);
  }

  /**
   * Utility method for creating a Microbase runtime object initialised using
   * the default configuration files found on the CLASSPATH.
   * @return a singleton ClientRuntime instance. Once configured, will always
   * return the same instance.
   * @throws ConfigurationException
   */
  public static synchronized ClientRuntime getDefaultRuntime()
      throws ConfigurationException
  {
    if (singleton == null)
    {
      GlobalConfiguration config = loadDefaultConfig();
      config.configureAll();
      singleton = config;

      logger.info("Microbase runtime configuration loaded / initialised");
    }
    return singleton.getRuntime();
  }



  /**
   * Loads configuration options from a file in the default location.
   * Various configuration are set in the specified client runtime
   * @param runtime
   * @throws ConfigurationException
   */
  public static GlobalConfiguration loadDefaultConfig()
      throws ConfigurationException
  {
    return loadDefaultConfig(GlobalConfiguration.class.getClassLoader());
  }
  
  public static GlobalConfiguration loadDefaultConfig(ClassLoader loader)
      throws ConfigurationException
  {
    try
    {
      /*
       * Obtain InputStream to read the configuration file
       */
      InputStream configStream = loader.getResourceAsStream(CONFIG_RESOURCE);
      if (configStream == null)
      {
        throw new ConfigurationException("Configuration file: "
            + CONFIG_RESOURCE + " could not be found on the CLASSPATH");
      }

      logger.log(Level.INFO,
          "Loading filesystem provider config from "
          + "CLASSPATH entry {0}", CONFIG_RESOURCE);
      Properties config = new Properties();
      config.load(configStream);

      GlobalConfiguration nodeConfig = new GlobalConfiguration(loader);
      nodeConfig.setConfig(config);

      return nodeConfig;
    }
    catch (IOException e)
    {
      throw new ConfigurationException(
          "Failed to load configuration from classpath: "
          + CONFIG_RESOURCE, e);
    }
  }

  private static void checkDir(File dir)
      throws ConfigurationException
  {
    if (!dir.exists() || !dir.isDirectory() || !dir.canWrite())
    {
      StringBuilder sb = new StringBuilder("Unable to use the path: ");
      sb.append(dir.getAbsolutePath()).append("\n");
      sb.append("  exists: ").append(dir.exists()).append("\n");
      sb.append("  isDirectory: ").append(dir.isDirectory()).append("\n");
      sb.append("  canWrite: ").append(dir.canWrite()).append("\n");
      throw new ConfigurationException(sb.toString());
    }
  }

  public void configureEnvironment()
      throws ConfigurationException
  {
//    runtime.setEnvMicrobaseHome(System.getenv(ENV_MICROBASE_HOME));
//    runtime.setEnvMicrobaseLog(System.getenv(ENV_MICROBASE_LOG));
//    runtime.setEnvMicrobaseConfig(System.getenv(ENV_MICROBASE_CONFIG));
    runtime.setEnvMicrobaseScratch(System.getenv(ENV_MICROBASE_SCRATCH));
    runtime.setEnvResponderData(System.getenv(ENV_RESPONDER_DATA));
//    runtime.setEnvResponderJars(System.getenv(ENV_RESPONDER_JARS));
    
//    if (runtime.getEnvMicrobaseHome() == null)
//      throw new ConfigurationException("Environment variable MICROBASE_HOME not set");
//    if (runtime.getEnvMicrobaseLog() == null)
//      throw new ConfigurationException("Environment variable MICROBASE_LOG not set");
//    if (runtime.getEnvMicrobaseConfig() == null)
//      throw new ConfigurationException("Environment variable MICROBASE_CONFIG not set");
    if (runtime.getEnvMicrobaseScratch() == null)
      throw new ConfigurationException("Environment variable MICROBASE_SCRATCH not set");
    if (runtime.getEnvResponderData() == null)
      throw new ConfigurationException("Environment variable RESPONDER_DATA not set");
//    if (runtime.getEnvResponderJars() == null)
//      throw new ConfigurationException("Environment variable RESPONDER_JARS not set");
  }
  
  /*
   * Copies all properties from the configuration file into a Map for use by
   * Microbase or responders.
   */
  public void configureRawProperties()
  {
    Map<String, String> nodeProperties = new HashMap<>();
    for (String propKey : config.stringPropertyNames()) {
      nodeProperties.put(propKey, config.getProperty(propKey));
    }
    runtime.getRawConfigProperties().putAll(nodeProperties);
  }
  
  public void configureNodeInfo()
      throws ConfigurationException
  {
    //By default, concurrency is set to the number of CPUs in the machine
    NodeInfo nodeInfo = ComputeNodeUtils.obtainNodeInformation();

    //However, a configuration property can be used to override the default
    String concurrentJobsStr = config.getProperty(PROP_NUM_CONCURRENT_JOBS);
    if (concurrentJobsStr != null)
    {
      int concurrency = Integer.parseInt(concurrentJobsStr);
      if (concurrency > 0)
      {
        nodeInfo.setCores(concurrency);
      }
    }
    runtime.setNodeInfo(nodeInfo);


    //Configure the maximum allowed in-memory per-responder job queues
    String maxQueueSizeStr = config.getProperty(PROP_MAX_RESPONDER_QUEUE_SIZE);
    if (maxQueueSizeStr != null)
    {
      runtime.setMaxResponderQueueSize(Integer.parseInt(maxQueueSizeStr));
    }
  }

  public void configureFilesystem()
      throws ConfigurationException
  {
    try
    {
      //Root directory for holding job working directories (temp files)
//      String tmpDirectoryRoot = config.getProperty(PROP_JOB_INSTALL_DIR);
      String scratchDir = runtime.getEnvMicrobaseScratch();
      if (scratchDir != null)
      {
        File jobTmpDirectoryRoot = new File(scratchDir, JOB_DIRNAME);
        jobTmpDirectoryRoot.mkdirs();
        checkDir(jobTmpDirectoryRoot);
        runtime.setJobTmpDirectoryRoot(jobTmpDirectoryRoot);
      }
      else
      {
        throw new ConfigurationException(
            "Can't find a value for the Microbase temporary directory. "
            + "Check required environment variables are set correctly");
      }


      //Configure the Microbase filesystem
      String mbfsClassname = config.getProperty(PROP_DEFAULT_MBFS_IMPL);
      if (mbfsClassname == null)
      {
        throw new ConfigurationException("Config file " + CONFIG_RESOURCE
            + " had no value for property: " + PROP_DEFAULT_MBFS_IMPL);
      }
      MicrobaseFS fsImpl = MicrobaseFSFactory.getProviderByClassName(mbfsClassname);
      fsImpl.configure(runtime.getClassLoader());
      runtime.setMicrobaseFS(fsImpl);

    }
    catch(Exception e)
    {
      throw new ConfigurationException(
          "Failed to configure Microbase filesystem and/or local "
          + "software installation directories.", e);
    }
  }


  public void configureLogger(NodeInfo info)
      throws ConfigurationException
  {
    DistributedLogger distLog = new DistributedLoggerHzImpl(
        runtime, info.getHostname());
    runtime.setDistLog(distLog);
  }

  public void configureRegistration()
      throws ConfigurationException
  {
    try
    {
      //Could choose a different implementation here, if necessary?

      MicrobaseRegistration registration =
          new MicrobaseRegistrationHzImpl(runtime);
      runtime.setRegistration(registration);
    }
    catch(Exception e)
    {
      throw new ConfigurationException(
          "Failed to configure responder registration system", e);
    }
  }

  public void configureNotification()
      throws ConfigurationException
  {
    try
    {
      DistributedJdbcPool ds = NotificationSqlConfiguration.
          createDataSourceFromDefaultConfigFile(runtime.getClassLoader());

      MicrobaseNotification notification = 
          new MicrobaseNotificationSQLImpl(
            runtime.getNodeInfo().getHostname(), ds);
//          new MicrobaseNotificationAtomicPSQLImpl(runtime, ds);
      runtime.setNotification(notification);
    }
    catch(Exception e)
    {
      throw new ConfigurationException(
          "Failed to configure Notification System", e);
    }
  }

  public void configureDistributedDataStructures()
      throws ConfigurationException
  {
    try
    {
      //Could choose a different implementation here, if necessary
      ResponderMessageQueues messageQueues =
          new ResponderMessageQueuesHzImpl(runtime);

      runtime.setMessageQueues(messageQueues);
      
      DistributedJdbcPool procListStoragePool = 
          DistributedProcessListSqlConfiguration.
              createDataSourceFromDefaultConfigFile(runtime.getClassLoader());
      DistributedProcessList procList = 
          new DistributedProcessListHzSqlImpl(runtime, procListStoragePool);
      runtime.setProcessList(procList);
      
      ExecutionResultLog resultLog = 
          new ExecutionResultLogSqlImpl(runtime, procListStoragePool);
      runtime.setExecutionResultLog(resultLog);
    }
    catch(Exception e)
    {
      throw new ConfigurationException(
          "Failed to configure Notification System", e);
    }
  }



  public void configureAll()
      throws ConfigurationException
  {
    configureEnvironment();
    configureRawProperties();
    configureNodeInfo();
    configureLogger(runtime.getNodeInfo());
    
    configureRegistration();
    configureNotification();
    configureDistributedDataStructures();
    configureFilesystem();
    
//    configureContainerProperties();
  }

  public Properties getConfig()
  {
    return config;
  }

  public void setConfig(Properties config)
  {
    this.config = config;
  }

  public ClientRuntime getRuntime()
  {
    return runtime;
  }
}
