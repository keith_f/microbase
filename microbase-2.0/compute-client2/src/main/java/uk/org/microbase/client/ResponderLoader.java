/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 28-Nov-2011, 23:05:12
 */
package uk.org.microbase.client;

import java.util.HashSet;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.Set;
import uk.org.microbase.responder.spi.MessageProcessor;
import uk.org.microbase.responder.spi.MessageQueuePopulator;

/**
 *
 * @author Keith Flanagan
 */
public class ResponderLoader
{
  
  private final ServiceLoader<MessageProcessor> processorLoader;
      
  
  public ResponderLoader()
  {
    processorLoader = ServiceLoader.load(MessageProcessor.class);
  }
  
  public ResponderLoader(ClassLoader cl)
  {
    processorLoader = ServiceLoader.load(MessageProcessor.class, cl);
  }
  
  public Set<MessageProcessor> getMessageProcessorProviders() 
      throws MicrobaseClientException
  {
    processorLoader.reload();
    Iterator<MessageProcessor> impls = processorLoader.iterator();
    Set<MessageProcessor> enabledImpls = new HashSet<MessageProcessor>();
    while(impls.hasNext())
    {
      MessageProcessor impl = impls.next();
//      if (impl.isEnabled())
//      {
      System.out.println("================================ Instance loaded: "+impl);
        enabledImpls.add(impl);
//      }
    }
    
    return enabledImpls;
  }
}
