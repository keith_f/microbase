/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 28-Nov-2011, 22:58:45
 */
package uk.org.microbase.client;

import com.torrenttamer.concurrent.AbstractPausableThread;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.dist.processes.ExecutionResult;
import uk.org.microbase.responder.spi.MessageProcessor;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.runtime.ConfigurationException;

/**
 *
 * @author Keith Flanagan
 */
public class Client
    extends AbstractPausableThread
{
  private static final Logger logger = 
      Logger.getLogger(Client.class.getName());
  private static final long DEFAULT_CLIENT_SCHEDULER_RATE_MS = 1000;
  
  
  private final ClientRuntime runtime;
//  private final ClientStatusPrinter statusPrinter;
  private final FutureProcessor futureProcessor;
  
  
  private final ScheduledThreadPoolExecutor processorPool;
  private final List<MicrobaseProcessFuture> processFutures;
  
  private final Semaphore cpuPermits;
  
  
  public Client(ClientRuntime runtime)
  {
    setName(getClass().getSimpleName()+"-"+getId());
    this.runtime = runtime;
    
    int cores = runtime.getNodeInfo().getCores();
    System.out.println("****** Number of cores available: "+cores);
    cpuPermits = new Semaphore(cores, true);
    processorPool = new ScheduledThreadPoolExecutor(cores);
    processFutures = new LinkedList<MicrobaseProcessFuture>();
    
//    statusPrinter = new ClientStatusPrinter(processFutures);
//    statusPrinter.setDaemon(true);
//    statusPrinter.start();
    
    futureProcessor = new FutureProcessor(runtime, processFutures);
    futureProcessor.setDaemon(true);
    futureProcessor.start();
    
    this.setWakeAtLeastEveryMs(DEFAULT_CLIENT_SCHEDULER_RATE_MS);
  }

  @Override
  public void setPaused(boolean paused)
  {
    super.setPaused(paused);
//    statusPrinter.setPaused(paused);
    futureProcessor.setPaused(paused);
  }

  @Override
  public void kill()
  {
    super.kill();
//    statusPrinter.kill();
    futureProcessor.kill();
  }
  
  
  @Override
  protected void _doWork(long currentRunTimeMs, long previousRunTimeMs)
  {
    Set<MessageProcessor> processors = new HashSet<MessageProcessor>();
    try
    {
      processors.addAll(locateResponderProcessors());
    }
    catch(MicrobaseClientException e)
    {
      logger.log(Level.INFO,"Failed to find responder "
          +"MessageProcessor implementations: {0}", e.getMessage());
      e.printStackTrace();
    }
    if (processors.isEmpty())
    {
      throw new RuntimeException("No responder implementations could be found.");
    }
    
    //TODO send this info as part of a distributed node status checker? Log to DB?
    System.out.println("*****************************************************");
    System.out.println("* Attempting to schedule more responder processes");
    System.out.println("* Responder implementations loaded: "+processors.size());
    System.out.println("* Current pool size: "+processorPool.getCorePoolSize()+", "+processorPool.getPoolSize());
    System.out.println("* Queue size: "+processorPool.getQueue().size()+", remaining capacity: "+processorPool.getQueue().remainingCapacity());
    System.out.println("* Active count: "+processorPool.getActiveCount());
    System.out.println("* Completed task count: "+processorPool.getCompletedTaskCount());
    System.out.println("* Process Futures list size: "+processFutures.size());
    
    //Block here until the queue size falls below X times the number of CPUs
    waitUntilQueueSizeFalls(4);
    
    synchronized(processFutures)
    {
      for (MessageProcessor processor : processors)
      {
        try
        {
          processor.setClientRuntime(runtime);
          processor.setCpuPermits(cpuPermits);
          processor.initialise();
          Future<ExecutionResult> future = processorPool.submit(processor);
          if (future == null)
          {
            throw new RuntimeException("Future returned from processorPool was NULL!");
          }
          MicrobaseProcessFuture mbFuture = 
              new MicrobaseProcessFuture(processor, future);
          processFutures.add(mbFuture);
        }
        catch(Exception e) 
        {
          logger.info("Failed to initialise responder: "+processor.getClass().getName());
          e.printStackTrace();
        }
      }
    }
  }
  
  private void waitUntilQueueSizeFalls(int coresMultiplier)
  {
    while(processorPool.getQueue().size() > 
        runtime.getNodeInfo().getCores() * coresMultiplier)
    {
      try
      {
        Thread.sleep(1000);
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
  }


  private Set<MessageProcessor> locateResponderProcessors()
      throws MicrobaseClientException
  {
    ResponderLoader loader = new ResponderLoader();
    Set<MessageProcessor> processors = loader.getMessageProcessorProviders();
    return processors;
  }
  
  private static ClientRuntime loadConfiguration() throws ConfigurationException   
  {
    logger.info("Creating runtime based on default global configuration");

    ///Configure
    ClientRuntime runtime = GlobalConfiguration.getDefaultRuntime();

    logger.info("Client configuration loaded: "+runtime.toString());
    return runtime;
  }
  
  public static void main(String args[]) 
      throws MicrobaseClientException
  {
//    Set<MessageQueuePopulator> populators =
//        ResponderLoader.getQueuePopulatorProviders();
//    logger.info("Found the following message queue populator implementations: "+populators);
    
    
    ResponderLoader loader = new ResponderLoader();
    Set<MessageProcessor> processors = loader.getMessageProcessorProviders();
    logger.info("Found the following message processor implementations: "+processors);
    
    
    if (processors.isEmpty())
    {
      System.err.println("No responders were installed. Exiting.");
      System.exit(1);
    }
    
    
    logger.info("Creating / starting comput client");
    
    try
    {
      Client client = new Client(loadConfiguration());
      System.out.println("Client configured.");
      
      client.start();
      client.setPaused(false);
      System.out.println("Client started. Press CTRL-C to exit.");
    }
    catch(ConfigurationException e)
    {
      logger.info("Failed to load Microbase client configuration");
      e.printStackTrace();
    }
    
  }
}
