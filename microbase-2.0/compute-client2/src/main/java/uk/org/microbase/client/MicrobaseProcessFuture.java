/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 6, 2011, 4:34:16 PM
 */
package uk.org.microbase.client;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import uk.org.microbase.dist.processes.ExecutionResult;
import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.responder.spi.MessageProcessor;

/**
 *
 * @author Keith Flanagan
 */
public class MicrobaseProcessFuture
    implements Future<ExecutionResult>
{

  private final MessageProcessor processor;
  private final Future<ExecutionResult> delegate;
  private long startedAtMs;

//  private String responderId;
//  private String responderName;
//  private String responderVersion;
  public MicrobaseProcessFuture(MessageProcessor processor,
      Future<ExecutionResult> future)
  {
    this.processor = processor;
    delegate = future;
    if (delegate == null)
    {
      throw new NullPointerException("Future cannot be NULL!");
    }
    startedAtMs = System.currentTimeMillis();
  }

  @Override
  public boolean cancel(boolean bln)
  {
    return delegate.cancel(bln);
  }

  @Override
  public boolean isCancelled()
  {
    return delegate.isCancelled();
  }

  @Override
  public boolean isDone()
  {
    return delegate.isDone();
  }

  @Override
  public ExecutionResult get()
      throws InterruptedException, ExecutionException
  {
    return delegate.get();
  }

  @Override
  public ExecutionResult get(long l, TimeUnit tu)
      throws InterruptedException, ExecutionException, TimeoutException
  {
    return delegate.get(l, tu);
  }

  public Future<ExecutionResult> getDelegate()
  {
    return delegate;
  }

  public ResponderInfo getResponderInfo()
  {
    return processor.getResponderInfo();
  }

//  public ProcessInfo getProcessInfo()
//  {
//    return processor.getProcessInfo();
//  }
  public long getStartedAtMs()
  {
    return startedAtMs;
  }

  public void setStartedAtMs(long startedAtMs)
  {
    this.startedAtMs = startedAtMs;
  }
}
