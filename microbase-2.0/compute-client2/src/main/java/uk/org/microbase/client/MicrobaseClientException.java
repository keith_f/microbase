/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 28-Nov-2011, 23:18:17
 */
package uk.org.microbase.client;

/**
 *
 * @author Keith Flanagan
 */
public class MicrobaseClientException
    extends Exception
{

  public MicrobaseClientException(Throwable thrwbl)
  {
    super(thrwbl);
  }

  public MicrobaseClientException(String string, Throwable thrwbl)
  {
    super(string, thrwbl);
  }

  public MicrobaseClientException(String string)
  {
    super(string);
  }

  public MicrobaseClientException()
  {
  }
}
