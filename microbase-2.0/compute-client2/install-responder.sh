#!/bin/bash

# Copies a responder and its dependencies to responder-cp
# Author: Keith Flanagan

if [ -z $1 ] ; then
  echo "Usage: /path/to/responder"
  exit 1
fi

if [ -z $MICROBASE_HOME ] ; then
    echo "Error: you must set MICROBASE_HOME to point at a directory containing your Microbase installation"
    exit 1
fi


if [ -z $RESPONDER_JARS ] ; then
    echo "Error: you must set RESPONDER_JARS to point at a directory containing compiled responder jar files."
    exit 1
fi


TARGET_DIR=$RESPONDER_JARS


# Get the filename of this script
SCRIPT_NAME=$0
SCRIPT_PATH=`which $0`
PROG_HOME=`dirname $SCRIPT_PATH`
#echo "Program home directory: $PROG_HOME"


#MVN_OPTS="-o"

MVN="mvn $MVN_OPTS"


RESPONDER_HOME=$1
mkdir -p $TARGET_DIR

if [ ! -d $RESPONDER_HOME ] ; then
  echo "Directory: $RESPONDER_HOME could not be found!"
  exit 1
fi

( cd $RESPONDER_HOME ; mvn dependency:copy-dependencies )
cp $RESPONDER_HOME/target/*.jar $TARGET_DIR
cp $RESPONDER_HOME/target/dependency/*.jar $TARGET_DIR

echo "Done."

