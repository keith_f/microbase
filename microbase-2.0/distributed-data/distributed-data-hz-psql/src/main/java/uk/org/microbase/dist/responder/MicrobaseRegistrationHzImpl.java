/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 10, 2011, 10:49:38 PM
 */

package uk.org.microbase.dist.responder;

import com.hazelcast.core.EntryListener;
import com.hazelcast.core.IMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.concurrent.DistributedLock;

/**
 * A Hazelcast implementation of <code>MicrobaseRegistration<.code>. This
 * implementation stores entirely transient instances of <code>ResponderInfo</code>.
 * The first responder instance to start up populates the initial configuration
 * from default values. Subsequent responder instances reuse this existing
 * <code>ResponderInfo</code> object. Subsequent changes to the ResponderInfo
 * object are obeyed by all responder instances on their next execution.
 * 
 * 
 * @author Keith Flanagan
 */
public class MicrobaseRegistrationHzImpl
  implements MicrobaseRegistration
{
  private static final String HZ_CONFIG_SET =
      MicrobaseRegistration.class.getName()+"_reponder_info";
  private static final String HZ_CONFIG_SET_LOCK =
      MicrobaseRegistration.class.getName()+"_reponder_info_lock";
  
  private final IMap<String, ResponderInfo> responderIdToInfo;
  private final DistributedLock infoSetLock;
  
  public MicrobaseRegistrationHzImpl(ClientRuntime runtime)
  {
    responderIdToInfo = runtime.getHazelcast().getMap(HZ_CONFIG_SET);
    infoSetLock = new DistributedLock(runtime.getHazelcast(), HZ_CONFIG_SET_LOCK);
  }
  
  private void lock()
  {
    infoSetLock.acquireLock();
  }
  
  private void unlock()
  {
    infoSetLock.releaseLock();
  }

  @Override
  public ResponderInfo registerResponder(ResponderInfo responderInfo)
      throws RegistrationException
  {
    lock();
    try
    {
      ResponderInfo existing = responderIdToInfo.get(responderInfo.getResponderId());
      if (existing != null)
      {
        return existing;
      }
      responderIdToInfo.put(responderInfo.getResponderId(), responderInfo);
      return responderInfo;
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public void deregisterResponder(String responderGuid)
      throws RegistrationException
  {
    lock();
    try
    {
      responderIdToInfo.remove(responderGuid);
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public List<ResponderInfo> getRegisteredResponders()
      throws RegistrationException
  {
    lock();
    try
    {
      List<ResponderInfo> responders = new ArrayList<ResponderInfo>();
      responders.addAll(responderIdToInfo.values());
      return responders;
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public List<ResponderInfo> getEnabledResponders()
      throws RegistrationException
  {
    lock();
    try
    {
      List<ResponderInfo> responders = getRegisteredResponders();
      Iterator<ResponderInfo> responderItr = responders.iterator();
      while(responderItr.hasNext())
      {
        ResponderInfo info = responderItr.next();
        if (!info.isEnabled())
        {
          responderItr.remove();
        }
      }
      return responders;
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public void setResponderEnabled(String responderGuid, boolean enabled)
      throws RegistrationException
  {
    lock();
    try
    {
      ResponderInfo info = getResponderInfo(responderGuid);
      info.setEnabled(enabled);
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public boolean isResponderEnabled(String responderGuid)
      throws RegistrationException
  {
    lock();
    try
    {
      ResponderInfo info = getResponderInfo(responderGuid);
      return info.isEnabled();
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public ResponderInfo getResponderInfo(String responderGuid)
      throws RegistrationException
  {
    lock();
    try
    {
      ResponderInfo info = responderIdToInfo.get(responderGuid);
//      if (info == null)
//      {
//        throw new RegistrationException(
//            "No ResponderInfo instance exists for: "+responderGuid);
//      }
      return info;
    }
    finally
    {
      unlock();
    }
  }
  
  @Override
  public void updateQueueLastPopulatedTime(String responderGuid, long timestamp)
      throws RegistrationException
  {
    lock();
    try
    {
      ResponderInfo info = responderIdToInfo.get(responderGuid);
      if (info == null)
      {
        throw new RegistrationException(
            "No ResponderInfo instance exists for: "+responderGuid);
      }
      info.setQueuePopulatorLastRunTimestamp(timestamp);
      
      //Now we need to re-put to enable collection listeners to be notified
      responderIdToInfo.put(responderGuid, info);
    }
    finally
    {
      unlock();
    }
  }

  @Override
  public void addResponderInfoListener(EntryListener<String, ResponderInfo> listener)
  {
    responderIdToInfo.addEntryListener(listener, true);
  }

}
