/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.dist.logging.hz_psql;

import com.hazelcast.core.IList;
import com.hazelcast.core.ItemListener;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import uk.org.microbase.dist.logging.DistributedLogger;
import uk.org.microbase.runtime.ClientRuntime;

/**
 * Provides distributed logging via hazelcast
 *
 * @author Keith Flanagan
 */
public class DistributedLoggerHzImpl
  implements DistributedLogger
{
  private static final int DEFAULT_MAX_LOG_SIZE = 2000;
  private static final String HZ_LOG_NAME =
      DistributedLogger.class.getName()+"_shared_log";

  private final String hostname;
  private final IList<String> log;
  private int maxLogSize;

  public DistributedLoggerHzImpl(ClientRuntime runtime, String hostname)
  {
    this.hostname = hostname;
    log = runtime.getHazelcast().getList(HZ_LOG_NAME);
    maxLogSize = DEFAULT_MAX_LOG_SIZE;
  }

  @Override
  public void log(String message)
  {
    String now = new Date(System.currentTimeMillis()).toString();
    Thread thisThread = Thread.currentThread();
    

    StringBuilder sb = new StringBuilder();
    sb.append(now).append(": ").
        append(hostname).append(": ").
        append("thread: ").append(thisThread.getId()).append(": ");
    sb.append(message);
    addLogItem(sb.toString());
  }

  @Override
  public void log(String... strings)
  {
    String now = new Date(System.currentTimeMillis()).toString();
    Thread thisThread = Thread.currentThread();

    StringBuilder sb = new StringBuilder();
    sb.append(now).append(": ").
        append(hostname).append(": ").
        append("thread: ").append(thisThread.getId()).append(": ");
    for (String string : strings)
    {
      sb.append(": ").append(string);
    }
    addLogItem(sb.toString());
  }
  
  private void addLogItem(String item)
  {
    //TODO do we need to lock this?
    log.add(item);
    if (log.size() > maxLogSize)
    {
      log.remove(0);
    }
  }
  
  @Override
  public List<String> getAllLogItems()
  {
    return new LinkedList<String>(log);
  }
  
  @Override
  public void addLogListener(ItemListener<String> listener)
  {
    log.addItemListener(listener, true);
  }
  
  
  
}
