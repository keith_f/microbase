/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 13, 2012, 3:15:04 PM
 */

package uk.org.microbase.dist.processes.hz_psql;

import com.hazelcast.core.EntryListener;
import com.hazelcast.core.IList;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ItemListener;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import java.util.Collection;
import uk.org.microbase.dist.processes.DistributedProcessList;
import uk.org.microbase.dist.processes.ExecutionResult;
import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.processes.ProcessListException;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.concurrent.DistributedLock;

/**
 *
 * @author Keith Flanagan
 */
public class DistributedProcessListHzSqlImpl
    implements DistributedProcessList
{
  private static final String HZ_PROCESSLIST =
      DistributedProcessList.class.getSimpleName() +"_processes";
  private static final String HZ_PROCESSLIST_LOCK =
      DistributedProcessList.class.getSimpleName() +"_processes_lock";
  
  
  
  private static final String HZ_EXECUTION_RESULT_LIST =
      DistributedProcessList.class.getSimpleName() +"_execution_results";

  private final ClientRuntime runtime;
  private final ExecutionResultLogSqlImpl executionResultLog;
  
  private IMap<String, ProcessInfo> processList;
  private IList<ExecutionResult> executionResultList;
  
  
        

  public DistributedProcessListHzSqlImpl(
      ClientRuntime runtime, DistributedJdbcPool pool)
  {
    this.runtime = runtime;
    executionResultLog = new ExecutionResultLogSqlImpl(runtime, pool);
    
    processList = runtime.getHazelcast().getMap(HZ_PROCESSLIST);
    executionResultList = runtime.getHazelcast().getList(HZ_EXECUTION_RESULT_LIST);
  }
  
  protected void lockProcessList()
  {
    DistributedLock processListLock = new DistributedLock(
        runtime.getHazelcast(), HZ_PROCESSLIST_LOCK);
    processListLock.acquireLock();
  }

  protected void unlockProcessList()
  {
    DistributedLock processListLock = new DistributedLock(
        runtime.getHazelcast(), HZ_PROCESSLIST_LOCK);
    processListLock.releaseLock();
  }

  
  
  @Override
  public void createProcessEntry(ProcessInfo processInfo)
      throws ProcessListException
  {
    lockProcessList();
    try
    {
      if (processList.containsKey(processInfo.getProcessGuid()))
      {
        throw new ProcessListException("Attempting to add a ProcessInfo entry "
            + "with GUID: "+processInfo.getProcessGuid()+", but an entry "
            + "with that GUID already exists!");
      }
      processList.put(processInfo.getProcessGuid(), processInfo);
    }
    finally
    {
      unlockProcessList();
    }
  }

  @Override
  public void removeProcessEntry(String processGuid)
      throws ProcessListException
  {
    lockProcessList();
    try
    {
      processList.remove(processGuid);
    }
    finally
    {
      unlockProcessList();
    }
  }

  @Override
  public void updateProcessEntry(ProcessInfo processInfo)
      throws ProcessListException
  {
    lockProcessList();
    try
    {
      if (!processList.containsKey(processInfo.getProcessGuid()))
      {
        throw new ProcessListException("Attempting to update a ProcessInfo "
            + "entry with GUID: "+processInfo.getProcessGuid()+", but no "
            + " entry with that GUID exists!");
      }
      processList.put(processInfo.getProcessGuid(), processInfo);
    }
    finally
    {
      unlockProcessList();
    }
  }
  
  @Override
  public int countTotalProcesses()
      throws ProcessListException
  {
    lockProcessList();
    try
    {
      return processList.size();
    }
    finally
    {
      unlockProcessList();
    }
  }
  
  @Override
  public int countDistributedCoresForResponder(String responderId)
      throws ProcessListException
  {
    lockProcessList();
    try
    {
      int count = 0;
      for (ProcessInfo info : processList.values())
      {
        if (info.getResponderId().equals(responderId))
        {
          count += info.getAssignedLocalCores();
        }
      }
      return count;
    }
    finally
    {
      unlockProcessList();
    }
  }
  
  @Override
  public Collection<ProcessInfo> getAllProcesses()
  {
    lockProcessList();
    try
    {
      return processList.values();
    }
    finally
    {
      unlockProcessList();
    }
  }

  @Override
  public void addProcessListListener(EntryListener<String, ProcessInfo> listener)
  {
    processList.addEntryListener(listener, true);
  }

  @Override
  public void removeProcessListListener(EntryListener<String, ProcessInfo> listener)
  {
    processList.removeEntryListener(listener);
  }
  
  
  /*
   * Methods dealing with execution reports
   */

  @Override
  public void reportExecutionResult(ExecutionResult result)
      throws ProcessListException
  {
    try
    {
      //Store ExecutionResult persistently.
      executionResultLog.reportExecutionResult(result);
      
      //Also store ExecutionResult temporarilly in distributed memory
      executionResultList.add(result);
    }
    catch(Exception e)
    {
      throw new ProcessListException(
          "Failed to record ExecutionResult: " + result.getProcessGuid(), e);
    }
  }

  
  

  @Override
  public void addExecutionResultListener(ItemListener<ExecutionResult> listener)
  {
    executionResultList.addItemListener(listener, true);
  }

  @Override
  public void removeExecutionResultListener(ItemListener<ExecutionResult> listener)
  {
    executionResultList.removeItemListener(listener);
  }

}
