/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 13, 2012, 3:15:04 PM
 */

package uk.org.microbase.dist.processes.hz_psql;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import com.hazelcast.core.*;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import uk.org.microbase.dist.processes.*;
import uk.org.microbase.dist.queues.ResponderMessageQueues;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.concurrent.DistributedLock;

/**
 * Plots overall job throughput
 * 
 * @author Keith Flanagan
 */
public class ExecutionResultLogSqlImpl
    implements ExecutionResultLog
{

  private final ClientRuntime runtime;
  private DistributedJdbcPool pool;
  private final ExecutionResultDAO executionResultDao;
  

  public ExecutionResultLogSqlImpl(
      ClientRuntime runtime, DistributedJdbcPool pool)
  {
    this.runtime = runtime;
    this.pool = pool;
    executionResultDao = new ExecutionResultDAO();
    
  }
  

  @Override
  public void reportExecutionResult(ExecutionResult result)
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      //Store ExecutionResult persistently.
      txn = pool.getWritableConnection();
      executionResultDao.save(txn, result);
      txn.commit();
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to record ExecutionResult: " + result.getProcessGuid(), e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public int countExecutionResults()
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.count(txn);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run countExecutionResults()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  /*
   * Queries over process start times 
   */
  
  @Override
  public long getFirstProcessStartTimestamp()
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.getFirstProcessStartTimestamp(txn);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run getFirstProcessStartTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public long getLastProcessStartTimestamp()
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.getLastProcessStartTimestamp(txn);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run getLastProcessStartTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public int countResultsBetweenProcStartTimestamps(
      long startTimestamp, long endTimestamp)
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.
          countExecutionResultsBetweenProcStartTimestamps(
          txn, startTimestamp, endTimestamp);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run countExecutionResultsBetweenProcStartTimestamps()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  /*
   * Queries over work completed times
   */
  @Override
  public long getFirstWorkCompletedTimestamp()
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.getFirstWorkCompletedTimestamp(txn);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run getFirstWorkCompletedTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public long getLastWorkCompletedTimestamp()
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.getLastWorkCompletedTimestamp(txn);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run getLastWorkCompletedTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public int countResultsBetweenWorkCompletedTimestamps(
      long startTimestamp, long endTimestamp)
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.
          countExecutionResultsBetweenWorkCompletedTimestamps(
          txn, startTimestamp, endTimestamp);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run countExecutionResultsBetweenWorkCompletedTimestamps()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  
  /*
   * Queries for retrieving execution results
   */

  @Override
  public List<ExecutionResult> getExecutionResults(int offset, int limit)
      throws ProcessListException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return executionResultDao.list(txn, offset, limit);
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new ProcessListException(
          "Failed to run countExecutionResults()", e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  

}
