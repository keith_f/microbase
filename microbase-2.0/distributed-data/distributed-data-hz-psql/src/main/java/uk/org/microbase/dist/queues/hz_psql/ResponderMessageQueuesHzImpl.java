/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.dist.queues.hz_psql;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.queues.MessageQueueException;
import uk.org.microbase.dist.queues.ResponderMessageQueues;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.data.Message.State;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.concurrent.DistributedLock;

/**
 * Implements ResponderMessageQueues using Hazelcast
 * 
 * @author Keith Flanagan
 */
public class ResponderMessageQueuesHzImpl
    implements ResponderMessageQueues
{
  private static final Logger logger =
      Logger.getLogger(ResponderMessageQueuesHzImpl.class.getName());

  private static final String QUEUE_LOCK_PREFIX =
      ResponderMessageQueuesHzImpl.class.getName() + "_";
  private static final int MSG_QUEUE_POPULATION_BLOCK = 200;

  private static final String HZ_RESPONDER_QUEUE_PREFIX =
      ResponderMessageQueues.class.getName() +
      "_message_queue_";

  private final ClientRuntime runtime;

  public ResponderMessageQueuesHzImpl(ClientRuntime runtime)
  {
    this.runtime = runtime;
  }

//  @Override
  protected void addJobsToQueue(
      String responderGuid, Collection<Message> messages)
      throws MessageQueueException
  {
    BlockingQueue<Message> queue =
        runtime.getHazelcast().getQueue(HZ_RESPONDER_QUEUE_PREFIX+responderGuid);
    queue.addAll(messages);

    logger.log(Level.INFO,
        "Responder: {0} added {1} items to its queue. Queue size is now: {2}",
        new Object[]{responderGuid, messages.size(), queue.size()});
  }

  protected void lockQueue(String responderGuid)
  {
    DistributedLock queueLock = new DistributedLock(
        runtime.getHazelcast(),
        QUEUE_LOCK_PREFIX+responderGuid);
    queueLock.acquireLock();
  }

  protected void unlockQueue(String responderGuid)
  {
    DistributedLock queueLock = new DistributedLock(
        runtime.getHazelcast(),
        QUEUE_LOCK_PREFIX+responderGuid);
    queueLock.releaseLock();
  }

  @Override
  public int getResponderJobQueueSize(String responderGuid)
      throws MessageQueueException
  {
    BlockingQueue<Message> queue =
        runtime.getHazelcast().getQueue(HZ_RESPONDER_QUEUE_PREFIX+responderGuid);
    return queue.size();
  }

  @Override
  public Message takeNextQueueItem(String responderGuid, String topicGuid)
      throws MessageQueueException
  {
    /*
     * This method returns a message for this the caller to process.
     * The following actions are performed:
     * 1) If the distributed queue for this responder has an available message,
     *    then this message is removed from the queue and returned for processing.
     * 2) If the distributed queue is empty, then the notification system will
     *    be queried for messages in the READY state in order to
     *    re-populate the distributed message queue.
     * 3) The message queue is re-polled for a message. If there are still no
     *    available messages, then <code>null</code> is returned. Otherwise, a
     *    message is returned for processing.
     *
     * If a message is chosen to be returned from this message, then its state
     * will first be changed to PROCESSING, and a suitable log message will be
     * published.
     *
     */
    try
    {
      String queueName = HZ_RESPONDER_QUEUE_PREFIX+responderGuid;
      logger.log(Level.INFO, "Attempting to lock distributed message queue "
          + "for: {0}, queue name: {1}", new Object[] {responderGuid, queueName});
      //Acquire a responder queue specific lock
      lockQueue(responderGuid);
      logger.log(Level.INFO, "Acquired lock for distributed message queue "
          + "of: {0}", responderGuid);
      BlockingQueue<Message> queue = runtime.getHazelcast().getQueue(queueName);

      // Step 1 - Retreive and remove head of queue. If queue empty, then null.
      Message nextMessage = queue.poll();

      // Step 2 - the queue was empty, attempt to repopulate.
      boolean repopulatedRecently = false;
      if (nextMessage == null)
      {
        ResponderInfo info = 
            runtime.getRegistration().getResponderInfo(responderGuid);
        repopulatedRecently = 
            (info.getQueuePopulatorLastRunTimestamp() 
            + info.getQueuePopulatorRunEveryMs()) > System.currentTimeMillis();
        logger.info("Queue for "+responderGuid
            + " was repopulated recently? "+repopulatedRecently);
        

        if (!repopulatedRecently)
        {
          logger.info("RAM-based message queue was empty, attempting to "
              + "repopulate queue for: "+responderGuid);
          //Find newly-arrived messages and mark them as READY for this responder

          int newMsgs = runtime.getNotification().
              prepareNewMessagesForResponder(responderGuid, topicGuid);
          logger.log(Level.INFO, "Found and generated initial '"
              +Message.State.READY+"' state entries for {0} new messages", newMsgs);

          //Repopulate in-memory queue from the notification system database
          logger.log(Level.INFO,"Copying up to {0} ''{1}"
              + "'' messages into RAM-based message "+"queue for responder: {2}", 
              new Object[]{MSG_QUEUE_POPULATION_BLOCK, 
                Message.State.READY, responderGuid});
          Set<String> msgIds  = runtime.getNotification().
              findMessagesInState(responderGuid,
              State.READY, MSG_QUEUE_POPULATION_BLOCK);
          logger.log(Level.INFO, "Adding {0} {1} messages to the in-memory "
              + "distributed queue for responder: {2}", new Object[]{
            msgIds.size(), State.READY, responderGuid});

          Map<String, Message> msgs = runtime.
              getNotification().getMessages(msgIds);
          addJobsToQueue(responderGuid, msgs.values());

          //Step 3 - attempt to get next available message from the queue
          nextMessage = queue.poll();
          
          //Update repopulation timestamp
          runtime.getRegistration().updateQueueLastPopulatedTime(
              responderGuid, System.currentTimeMillis());
        }
      }

      if (nextMessage == null)
      {
        if (!repopulatedRecently)
        {
          logger.info("Still no messages, even after attempting to "
              + "re-populate the queue. Giving up.");
        }
        else
        {
          logger.info("No available messages from in-memory queue. However, we "
              + "didn't attempt repopulation because last re-population "
              + "attempt was recent.");
        }
        return null;
      }

      //Update message state
      logger.log(Level.INFO, "Setting message state of: {0} to: {1}",
          new Object[]{nextMessage.getGuid(), State.BLOCKED_AWAITING_LOCKS});
      runtime.getNotification().setMessageState(
          nextMessage.getGuid(), responderGuid, State.BLOCKED_AWAITING_LOCKS);

      //Return message
      logger.log(Level.INFO, "Responder {0} assigned message: {1}",
          new Object[]{responderGuid, nextMessage.getGuid()});
      return nextMessage;
    }
    catch(Exception e)
    {
      logger.log(Level.INFO, "Failed to acquire a new message for responder: "
          + "{0} to process", responderGuid);
      throw new MessageQueueException("Failed to acquire a new message for responder: "
          + responderGuid+" to process", e);
    }
    finally
    {
      logger.log(Level.INFO, "Releasing distributed message queue lock "
          + "for: {0}", responderGuid);
      unlockQueue(responderGuid);
    }
  }
}
