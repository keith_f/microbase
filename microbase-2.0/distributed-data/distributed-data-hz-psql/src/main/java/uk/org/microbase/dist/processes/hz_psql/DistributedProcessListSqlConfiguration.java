/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.org.microbase.dist.processes.hz_psql;

import com.torrenttamer.jdbcutils.DatabaseConfiguration;
import com.torrenttamer.jdbcutils.DatabaseConfigurationException;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.DistributedJdbcPoolFactory;
import java.io.IOException;
import java.util.logging.Logger;
import uk.org.microbase.notification.spi.NotificationException;

/**
 * This class allows the creation of an SQL DataSource from a properties
 * file present on the classpath.
 * 
 * The configuration file should have the following properties:
 *   - username
 *   - password
 *   - db_url_writable
 *   - db_url_readonly
 * See DistributedJdbcPoolFactory for more information.
 * 
 * @author Keith Flanagan
 */
public class DistributedProcessListSqlConfiguration
{
  private static final Logger logger =
      Logger.getLogger(DistributedProcessListSqlConfiguration.class.getName());

  private static final String DEFAULT_CONFIG_RESOURCE =
      DistributedProcessListSqlConfiguration.class.getSimpleName() + ".properties";


  public static DistributedJdbcPool createDataSourceFromResource(
      ClassLoader cl, String resourcePath)
      throws NotificationException
  { 
    try
    {
      DatabaseConfiguration config = DistributedJdbcPoolFactory.
          readConfigurationFromPropertyResource(cl, resourcePath);
      DistributedJdbcPool pool = 
          DistributedJdbcPoolFactory.
          createSingleWriteMultipleReplicaPool(config);
      return pool;
    }
    catch(IOException e)
    {
      throw new NotificationException(
          "Failed to read database configuration resource: "+resourcePath, e);
    }
    catch(DatabaseConfigurationException e)
    {
      throw new NotificationException(
          "Failed to configure database based on resource: "+resourcePath, e);
    }
  }

  public static DistributedJdbcPool createDataSourceFromDefaultConfigFile(ClassLoader cl)
      throws NotificationException
  {
    return createDataSourceFromResource(cl, DEFAULT_CONFIG_RESOURCE);
  }

}
