/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 7, 2011, 12:30:17 PM
 */

package uk.org.microbase.dist.processes;

import java.io.Serializable;

/**
 * Stores information about a process that is currently running on a compute
 * client instance. A set of ProcessInfo instances in a distributed data
 * structure is a useful means for clients to communicate which types of
 * messages they are currently processing. This information can be used:
 *   - for monitoring debugging a running cluster
 *   - to influence the types of job that other computer clients in the cluster
 *     execute next.
 * 
 * Related to MicrobaseProcessFuture that are internal to a compute client.
 * @author Keith Flanagan
 */
public class ProcessInfo
    implements Serializable
{
  private String processGuid;
  private String hostname;
  private int assignedLocalCores;
  private long processStartedAtMs;
  private long workStartedAtMs;
  
  private String responderId;
  private String responderName;
  private String responderVersion;
  private String topicId;
  private String messageId;
  

  private float percentComplete;

  public ProcessInfo()
  {
  }

  public String getResponderId()
  {
    return responderId;
  }

  public void setResponderId(String responderId)
  {
    this.responderId = responderId;
  }

  public String getResponderName()
  {
    return responderName;
  }

  public void setResponderName(String responderName)
  {
    this.responderName = responderName;
  }

  public String getResponderVersion()
  {
    return responderVersion;
  }

  public void setResponderVersion(String responderVersion)
  {
    this.responderVersion = responderVersion;
  }

  public long getProcessStartedAtMs()
  {
    return processStartedAtMs;
  }

  public void setProcessStartedAtMs(long processStartedAtMs)
  {
    this.processStartedAtMs = processStartedAtMs;
  }

  public long getWorkStartedAtMs()
  {
    return workStartedAtMs;
  }

  public void setWorkStartedAtMs(long workStartedAtMs)
  {
    this.workStartedAtMs = workStartedAtMs;
  }

  public float getPercentComplete()
  {
    return percentComplete;
  }

  /**
   * Sets a value between 0 and 100 that indicate the 'completeness' of the
   * work being performed by this process. Setting this value is entirely
   * optional and is used in user interfaces to indicate an estimate of when
   * a process's work is likely to be done.
   * 
   * @param percentComplete 
   */
  public void setPercentComplete(float percentComplete)
  {
    this.percentComplete = percentComplete;
  }

  public String getProcessGuid()
  {
    return processGuid;
  }

  public void setProcessGuid(String processGuid)
  {
    this.processGuid = processGuid;
  }

  public int getAssignedLocalCores()
  {
    return assignedLocalCores;
  }

  public void setAssignedLocalCores(int assignedLocalCores)
  {
    this.assignedLocalCores = assignedLocalCores;
  }

  public String getHostname()
  {
    return hostname;
  }

  public void setHostname(String hostname)
  {
    this.hostname = hostname;
  }

  public String getMessageId()
  {
    return messageId;
  }

  public void setMessageId(String messageId)
  {
    this.messageId = messageId;
  }

  public String getTopicId()
  {
    return topicId;
  }

  public void setTopicId(String topicId)
  {
    this.topicId = topicId;
  }
  
}
