/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 05-Nov-2010, 15:23:23
 */

package uk.org.microbase.dist.responder;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Holds configuration information for a responder that has been installed and 
 * registered with a Microbase cluster. There is only one ResponderInfo instance
 * per responder per cluster. ResponderInfo instances are stored within a 
 * distributed Hazelcast datastucture. When responder instance start up, they
 * read this distributed object to configure themselves. The first responder
 * instance in a given cluster is responsible for creating the ResponderInfo
 * instance, either from a configuration file or from default hard-coded values.
 *
 * Many properties stored by this bean are essential to the operation of your
 * responder. Topic names, for instance, inform Microbase about which type of
 * notification message your responder should receive.
 * 
 * Microbase uses a number of the fields stored by this data bean for 
 * determining how to schedule jobs in the system. For example, the you can 
 * configure limits on the number of local or global processes here which can
 * be useful if your responder relies on a shared resource (such as a relational
 * database server).
 * 
 * @author Keith Flanagan
 */
public class ResponderInfo
    implements Serializable
{
  private static final long DEFAULT_QUEUE_POP_RUN_EVERY_MS = 1000 * 60;
  private static final int DEFAULT_MAX_LOCAL_CORES = 1;
  private static final int DEFAULT_MAX_DIST_CORES = Integer.MAX_VALUE;
  
  private String responderId;
  private String responderName;
  private String responderVersion;

  private String topicOfInterest;
  private String topicOut;
  private String topicErr;
  
  private int preferredCpus;

  /*
   * Parallelism limits
   */

//  private int maxInstances;
  private int maxLocalCores;
  private int maxDistributedCores;

  private boolean enabled;
  
  private long queuePopulatorRunEveryMs;
  private long queuePopulatorLastRunTimestamp;
  
  /*
   * A set of convenience properties that you can use for storing MBFS locations
   * to be used for storing result files / log files. These properties are
   * provided for convenience only - your responder implementation is permitted
   * to store files elsewhere, if required.
   */
  
  private String resultDestinationBucket;
  private String resultDestinationBasePath;
  private String logDestinationBucket;
  private String logDestinationBasePath;
  
  /**
   * An additional (and optional) set of named configuration properties. Your
   * responder can use this field to store responder/domain-specific
   * configuration properties, such as specific file locations, server names,
   * values that influence how  instances of your responder execute, or any
   * other responder-specific configuration items.
   * 
   * If your responder configures itself from a standard Properties file
   * (for example via <code>ResponderInfoPropertiesParser</code>), it may
   * be useful to store the original key-value pairs here.
   */
  private Map<String, String> configProperties;

  public ResponderInfo()
  {
    maxLocalCores = DEFAULT_MAX_LOCAL_CORES;
    maxDistributedCores = DEFAULT_MAX_DIST_CORES;
    queuePopulatorRunEveryMs = DEFAULT_QUEUE_POP_RUN_EVERY_MS;
    configProperties = new HashMap<>();
  }

  public String getResponderId()
  {
    return responderId;
  }

  public void setResponderId(String responderId)
  {
    this.responderId = responderId;
  }

  public String getResponderName()
  {
    return responderName;
  }

  public void setResponderName(String responderName)
  {
    this.responderName = responderName;
  }

  public String getResponderVersion()
  {
    return responderVersion;
  }

  public void setResponderVersion(String responderVersion)
  {
    this.responderVersion = responderVersion;
  }
  
  public boolean isEnabled()
  {
    return enabled;
  }

  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  public int getMaxDistributedCores()
  {
    return maxDistributedCores;
  }

  public void setMaxDistributedCores(int maxDistributedCores)
  {
    this.maxDistributedCores = maxDistributedCores;
  }

  public int getMaxLocalCores()
  {
    return maxLocalCores;
  }

  public void setMaxLocalCores(int maxLocalCores)
  {
    this.maxLocalCores = maxLocalCores;
  }

  public long getQueuePopulatorLastRunTimestamp()
  {
    return queuePopulatorLastRunTimestamp;
  }

  public void setQueuePopulatorLastRunTimestamp(long queuePopulatorLastRunTimestamp)
  {
    this.queuePopulatorLastRunTimestamp = queuePopulatorLastRunTimestamp;
  }

  public long getQueuePopulatorRunEveryMs()
  {
    return queuePopulatorRunEveryMs;
  }

  public void setQueuePopulatorRunEveryMs(long queuePopulatorRunEveryMs)
  {
    this.queuePopulatorRunEveryMs = queuePopulatorRunEveryMs;
  }

  public String getTopicOfInterest()
  {
    return topicOfInterest;
  }

  public void setTopicOfInterest(String topicOfInterest)
  {
    this.topicOfInterest = topicOfInterest;
  }

  public int getPreferredCpus()
  {
    return preferredCpus;
  }

  public void setPreferredCpus(int preferredCpus)
  {
    this.preferredCpus = preferredCpus;
  }

  public String getTopicErr()
  {
    return topicErr;
  }

  public void setTopicErr(String topicErr)
  {
    this.topicErr = topicErr;
  }

  public String getTopicOut()
  {
    return topicOut;
  }

  public void setTopicOut(String topicOut)
  {
    this.topicOut = topicOut;
  }

  public Map<String, String> getConfigProperties()
  {
    return configProperties;
  }

  public void setConfigProperties(Map<String, String> configProperties)
  {
    this.configProperties = configProperties;
  }

  public String getResultDestinationBucket()
  {
    return resultDestinationBucket;
  }

  public void setResultDestinationBucket(String resultDestinationBucket)
  {
    this.resultDestinationBucket = resultDestinationBucket;
  }
  
  public String getLogDestinationBucket()
  {
    return logDestinationBucket;
  }

  public void setLogDestinationBucket(String logDestinationBucket)
  {
    this.logDestinationBucket = logDestinationBucket;
  }

  public String getResultDestinationBasePath()
  {
    return resultDestinationBasePath;
  }

  public void setResultDestinationBasePath(String resultDestinationBasePath)
  {
    this.resultDestinationBasePath = resultDestinationBasePath;
  }

  public String getLogDestinationBasePath()
  {
    return logDestinationBasePath;
  }

  public void setLogDestinationBasePath(String logDestinationBasePath)
  {
    this.logDestinationBasePath = logDestinationBasePath;
  }

  
  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder(getClass().getName());
    sb.append("[");
    sb.append("responderId: ").append(responderId);
    sb.append(", responderName: ").append(responderName);
    sb.append(", responderVersion: ").append(responderVersion);
    sb.append(", topic in: ").append(topicOfInterest);
    sb.append(", topic out: ").append(topicOut);
    sb.append(", topic err: ").append(topicErr);
    sb.append(", preferred CPUs: ").append(preferredCpus);
    sb.append(", maxLocalCores: ").append(maxLocalCores);
    sb.append(", maxDistributedCores: ").append(maxDistributedCores);
    sb.append(", enabled: ").append(enabled);
    sb.append(", queuePopulatorRunEveryMs: ").append(queuePopulatorRunEveryMs);
    sb.append(", queuePopulatorLastRunTimestamp: ").append(
        new Date(queuePopulatorLastRunTimestamp).toString());
    sb.append(", resultDestinationBucket: ").append(resultDestinationBucket);
    sb.append(", resultDestinationPath: ").append(resultDestinationBasePath);
    sb.append(", logDestinationBucket: ").append(logDestinationBucket);
    sb.append(", logDestinationPath: ").append(logDestinationBasePath);
    sb.append(", configProperties: ").append(configProperties);
    sb.append("]");
    return sb.toString();
  }

  
}
