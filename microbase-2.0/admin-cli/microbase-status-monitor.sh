#!/bin/bash

# Microbase launcher script
# Author: Keith Flanagan

# The Java class to run
EXE_CLASSNAME="uk.org.microbase.cli.MicrobaseStatusMonitor"
RAM="-Xmx768M"

#MVN_OPTS="-o"
MVN="mvn $MVN_OPTS"

# Get the filename of this script
SCRIPT_NAME=$0
SCRIPT_PATH=`which $0`
PROG_HOME=`dirname $SCRIPT_PATH`
#echo "Program home directory: $PROG_HOME"

if [ -z $MICROBASE_HOME ] ; then
    echo "Error: you must set MICROBASE_HOME to point at a directory containing your Microbase installation"
    exit 1
fi

if [ -z $MICROBASE_LOG ] ; then
    echo "Error: you must set MICROBASE_LOG to point at a directory for containing Microbase/responder log files."
    exit 1
fi
if [ -z $MICROBASE_CONFIG ] ; then
    echo "Error: you must set MICROBASE_CONFIG to point at a directory containing Microbase/responder configuration files."
    exit 1
fi
if [ -z $MICROBASE_SCRATCH ] ; then
    echo "Error: you must set MICROBASE_SCRATCH to point at a directory to be used as temporary space for job processing."
    exit 1
fi
if [ -z $RESPONDER_DATA ] ; then
    echo "Error: you must set RESPONDER_DATA to point at a directory to be used for holding large responder resources such as executables and file-based databases."
    exit 1
fi
if [ -z $RESPONDER_JARS ] ; then
    echo "Error: you must set RESPONDER_JARS to point at a directory containing compiled responder jar files."
    exit 1
fi


if [ ! -d $PROG_HOME/target/dependency ] ; then
    # Install dependencies into the program's target directory if necessary
    ( cd $PROG_HOME ; $MVN dependency:copy-dependencies )
fi

# Configure CLASSPATH
# Include program's compiled classes
CLASSPATH=$PROG_HOME/target/classes
# Include .jar dependencies
for LIB in `find $PROG_HOME/target/dependency -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done


# Add configuration file directory to the CLASSPATH
CLASSPATH=$CLASSPATH:$MICROBASE_CONFIG

# Add a directory to which responder classes and jars can be added
CLASSPATH=$CLASSPATH:$RESPONDER_JARS
# Include responder .jar files
for LIB in `find $RESPONDER_JARS -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done


# Finally, start the application
java $RAM -Dhazelcast.config=$MICROBASE_HOME/config/hazelcast.xml -cp $CLASSPATH $EXE_CLASSNAME $@



