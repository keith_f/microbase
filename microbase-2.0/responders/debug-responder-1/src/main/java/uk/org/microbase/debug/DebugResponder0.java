/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 16, 2012, 4:52:03 PM
 */

package uk.org.microbase.debug;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;

/**
 * This class implements a simple responder that simply busy-loops
 * in order to simulate computational work.
 * 
 * @author Keith Flanagan
 */
public class DebugResponder0
    extends AbstractMessageProcessor
{
  private static final int DEBUG_MESSAGE_LEVEL = 0;
  private static final int INTERATIONS_PER_BLOCK = 1000000;
  private static final double FAILURE_RATE = 0.0000001;
  private static final long MAX_JOB_LENGTH_MS = 1000 * 60;
  
  
  private static final Logger l = Logger.
      getLogger(DebugResponder0.class.getSimpleName());
  
  private static final String TOPIC_IN = "debug-test-topic-level:"+DEBUG_MESSAGE_LEVEL;
  private static final String TOPIC_OUT = TOPIC_IN+"-COMPLETED";
  

  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(3000);
    info.setMaxLocalCores(1);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(DebugResponder0.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(TOPIC_IN);
    return info;
  }
  
  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    l.info("Doing preRun");
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    l.info("Doing cleanup");
  }
  


  @Override
  public Set<Message> processMessage(Message incomingMessage)
      throws ProcessingException
  {
    l.info("Starting to process message");
    /*
     * Randomise job work time from between 0 and 60 seconds.
     */
    long startTime = System.currentTimeMillis();
    double msToWork = MAX_JOB_LENGTH_MS * Math.random();
    long finishTime = System.currentTimeMillis() + (long) msToWork;
    try
    {
      /*
       * Busy-loop to simulate CPU usage
       */
      long iterations = 0;
      while(System.currentTimeMillis() < finishTime)
      {
        for (int i=0; i<INTERATIONS_PER_BLOCK; i++)
        {
          iterations++;
        }
        if (Math.random() < FAILURE_RATE)
        {
          throw new Exception("Simulated job failure!");
        }
      }
      
      double durationSec = (System.currentTimeMillis() - startTime) / 1000;
      double itsPerSec = iterations / durationSec;
      double targetDurationSec = msToWork / 1000;
      System.out.println("Performed: "+iterations+" pointless iterations in "
          + durationSec + " seconds. Target was " + targetDurationSec + " seconds.\n"
          + "Iterations per second: "+itsPerSec);

      /*
       * All 'work' has now been completed, so the final action to perform is
       * to prepare a notification message to send on job completion. You could
       * send multiple messages of different types here.
       */
      Set<Message> toPublish = new HashSet<Message>();

      /*
       * The 'createMessage' method is provided for convenience. It takes an 
       * existing message -- in this case 'incomingMessage' -- and creates a
       * child message. The new message 'done' has 'incomingMessage' set as its
       * parent. Various other fields such as the workflow execution ID are also
       * set automatically.
       */
      Message done = createMessage(
          incomingMessage,                     //The message to use as a parent
          TOPIC_OUT,                           //The topic of the new message
          incomingMessage.getWorkflowStepId(),   //The workflow step ID
          //An optional human-readable description
          getResponderInfo().getResponderId()
          +" - Performed: "+iterations+" pointless iterations in "
          + durationSec + " seconds. Target was " + targetDurationSec 
          + " seconds.\nIterations per second: "+itsPerSec);
      
      done.getContent().put("Iterations", String.valueOf(iterations));
      done.getContent().put("Duration", String.valueOf(durationSec));
      done.getContent().put("Iterations per second", String.valueOf(itsPerSec));


      toPublish.add(done);
      return toPublish;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run for some reason"
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
    finally
    {
      l.info("Finished processing");
    }
  }
  
}
