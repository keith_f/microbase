/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.responder.simulation;

import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * This utility is a notification message content generator for use with the
 * Microbase Shell application. This class generates the sample message content.
 * 
 * @author Keith Flanagan
 */
public class SimulatedWorkResponderMsgGen
    implements ShellMsgGenerator<SimulatedWorkDescription>
{
  
  @Override
  public String getMessageProcessorClassname() {
    return SimulatedWorkProcessor.class.getName();
  }

  @Override
  public Map<String, String> getReqPropertyNames() {
    Map<String, String> nameToDesc = new HashMap<>();
    
    nameToDesc.put("simulator.busy_wait", "Set 'true' to busy-wait; set 'false' to use Thread.sleep()");
    nameToDesc.put("simulator.outgoing_message_topic", "The topic name of "
            + "the outgoing 'success' message. If you set this to the same as "
            + "the incoming topic name, you can set up infinate loops of work "
            + "(useful for load testing/debugging).");
    nameToDesc.put("simulator.work_for_ms", "The approximate length of time "
            + "that the job should execute for.");

    return nameToDesc;
  }

  @Override
  public String generateJson(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      SimulatedWorkDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
  }

  @Override
  public SimulatedWorkDescription generateMsgBean(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      SimulatedWorkDescription bean = new SimulatedWorkDescription();
      bean.setBusyWait(Boolean.parseBoolean(properties.get("simulator.busy_wait")));
      bean.setResultMessageTopic(properties.get("simulator.outgoing_message_topic"));
      bean.setWorkForMs(Long.parseLong(properties.get("simulator.work_for_ms")));

      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }

}
