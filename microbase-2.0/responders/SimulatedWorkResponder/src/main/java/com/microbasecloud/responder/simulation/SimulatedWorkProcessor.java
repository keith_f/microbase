/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.responder.simulation;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;

/**
 * This class implements a simple Microbase responder. It is configured to
 * react to messages with the topic 'HelloTopic'. 
 * 
 * @author Keith Flanagan
 */
public class SimulatedWorkProcessor
    extends AbstractMessageProcessor
{
  private static final Logger l =
          Logger.getLogger(SimulatedWorkProcessor.class.getName());
  private static final String CONFIG_RESOURCE = 
          SimulatedWorkProcessor.class.getSimpleName() + ".properties";
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
          throws RegistrationException
  {
    try
    {
      Properties config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }

      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    System.out.println("Doing preRun");
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    System.out.println("Doing cleanup");
  }

  @Override
  public Set<Message> processMessage(Message incomingMessage)
      throws ProcessingException
  {
    System.out.println("Parsing message");
    SimulatedWorkDescription descr;
    try {
      descr = parseMessageWithJsonContent(
                    incomingMessage, SimulatedWorkDescription.class);
    } catch (IOException ex) {
      throw new ProcessingException(
              "Failed to parse incoming message: "+incomingMessage, ex);
    }
    
    System.out.println("Processing message");
    
    long startTime = System.currentTimeMillis();
    long endTime = startTime + descr.getWorkForMs();
    
    double totalExpectedDuration = endTime - startTime;
    double duration = 0;
    double pointlessCountForBusyWait = 0;
    while (duration < totalExpectedDuration)
    {
      try
      {
        if (descr.isBusyWait()) {
          /*
           * Simulate work by busy-looping for a while
           */
          
          for (int i=0; i<10000000; i++) {
            double rnd = Math.random();
            pointlessCountForBusyWait = pointlessCountForBusyWait + rnd;
            pointlessCountForBusyWait = pointlessCountForBusyWait / 2;
          }
          System.out.print("@");
        } else {
          /*
           * Simulate work by pausing for a while.
           */
          Thread.sleep(1000);
          System.out.print(".");          
        }
        
        duration = System.currentTimeMillis() - startTime;
        
        
        /*
         * We can optionally notify Microbase of this job's current progress.
         * You should set a value between 0 and 100.
         */
        float percentComplete = (float) (duration / totalExpectedDuration * 100);
        notifyProgress(percentComplete);
      }
      catch(InterruptedException e)
      {
        e.printStackTrace();
        // Nothing to do here. Just continue until the time limit is reached
      }
    }
    System.out.println("");

    
    /*
     * All 'work' has now been completed, so the final action to perform is
     * to prepare a notification message to send on job completion. You could
     * send multiple messages of different types here.
     */
    Set<Message> toPublish = new HashSet<>();
    
    /*
     * The 'createMessageWithJsonContent' method is provided for convenience. 
     * It takes an existing message -- in this case 'incomingMessage' -- and 
     * creates a child message. The new message 'done' has 'incomingMessage' set
     * as its parent. Various other fields such as the workflow execution ID are 
     * also set automatically.
     * 
     * In addition, we are able to pass in a Java bean (in this case 'descr', 
     * which happens to be the same bean as the content that was received in
     * the 'incomingMessage'). This bean can be any JSON-serializable type.
     * The only reason we re-publish the same content bean in this example is
     * to allow for 
     */
    try {
      Message done = createMessageWithJsonContent(
          incomingMessage, 
          descr.getResultMessageTopic(),
          "HelloWorld-"+Math.random(),   //The workflow step ID
          descr,                         //Re-pass the job description as the content!
          //An optional human-readable description
          "User description of how the job completed successfully. "
          + "This is optional and can be NULL");

      //Arbitrary message content items can be added like this:
      done.getContent().put("some_other_key", "some value");
      done.getContent().put("duration", String.valueOf(duration));
      done.getContent().put("totalExpectedDuration", String.valueOf(totalExpectedDuration));

      if (descr.isBusyWait()) {
        done.getContent().put(
                "pointless_busy_wait_value", String.valueOf(pointlessCountForBusyWait));
      }

      done.getContent().put("completed_at_timestamp", 
          new Date(System.currentTimeMillis()).toString());


      toPublish.add(done);
      return toPublish;
    }
    catch(Exception e) {
      throw new ProcessingException(
              "Failed to create 'success' notification message for the "
              + "successful processing of: "+incomingMessage.getGuid(), e);
    }
  }
  
}
