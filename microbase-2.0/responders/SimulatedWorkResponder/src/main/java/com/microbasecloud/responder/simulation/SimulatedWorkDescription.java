/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.responder.simulation;

/**
 * Represents a description of a simulated job. The work performed is simply a
 * busy-wait. Jobs of this type are useful for performance/load testing. 
 * Objects of this type are intended to be attached as a notification
 * message content item.
 * 
 * @author Keith Flanagan
 */
public class SimulatedWorkDescription
{
  /**
   * Specifies how long a job should be 'processed' for.
   */
  private long workForMs;
  
  /**
   * Specifies whether CPU resources will be consumed while the responder executes.
   * If true, the responder will busy-loop until the specified <code>workForMs</code>
   * has elapsed, causing significant CPU usage. If false, the responder will
   * simply block until the specified amount of time has passed, with minimal
   * impact on the CPU.
   */
  private boolean busyWait;
  
  /**
   * The topic name to use for the outgoing 'success' notification that signifies
   * job completion. You can set this to the same as the incoming topic name in
   * order to cause an 'infinite loop' of job descriptions that may be useful
   * for testing purposes.
   */
  private String resultMessageTopic;

  public SimulatedWorkDescription() {
  }

  public long getWorkForMs() {
    return workForMs;
  }

  public void setWorkForMs(long workForMs) {
    this.workForMs = workForMs;
  }

  public boolean isBusyWait() {
    return busyWait;
  }

  public void setBusyWait(boolean busyWait) {
    this.busyWait = busyWait;
  }

  public String getResultMessageTopic() {
    return resultMessageTopic;
  }

  public void setResultMessageTopic(String resultMessageTopic) {
    this.resultMessageTopic = resultMessageTopic;
  }
  
  
}
