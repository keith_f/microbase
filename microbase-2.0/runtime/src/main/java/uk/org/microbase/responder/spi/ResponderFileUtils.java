/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 02-Dec-2012, 01:17:40
 */

package uk.org.microbase.responder.spi;

import com.hazelcast.core.ILock;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.file.DirectoryUtils;
import uk.org.microbase.util.file.FileUtils;
import uk.org.microbase.util.file.zip.ZipUtils;

/**
 *
 * @author Keith Flanagan
 */
public class ResponderFileUtils
{
  private static final Logger logger = 
      Logger.getLogger(ResponderFileUtils.class.getName());

  /**
   * Creates a distributed Lock for use with a specified file in the
   * directory returned by <code>getSharedWorkingDirectory</code>. The
   * <code>filename</code> used does not actually have to be the same as the
   * actual path/file used, but it should be unique to the file being used.
   * All responders that might use the same file in the shared directory should
   * use the same lock name.
   * 
   * Even though operations to files in the shared working directory are
   * inherently machine-local, a Hazelcast lock is used here to guard against
   * the case that two Microbase compute clients might be running on the same
   * machine.
   * 
   * Note: you <b>must</b> use <code>lockFileInSharedDir</code> in a 
   * try/finally block, with <code>unlockFileInSharedDir</code> in the 
   * 'finally' part in order to prevent Exceptions in your responder from 
   * blocking other responders attempting to access the same file on this machine.
   * 
   * @param filename a string that represents a file in the shared filespace.
   */
  protected static ILock getLockForFileInSharedDir(ClientRuntime runtime, String filename)
  {
    //Use a lock name that is machine-specific, but not process/thread-specific
    String lockName = runtime.getNodeInfo().getHostname()+":"+filename;
    ILock fileLock = runtime.getHazelcast().getLock(lockName);
    return fileLock;
  }
  
  
  /**
   * A convenience method that can be used to safely download and 
   * extract the contents of a zip file to the shared directory space. 
   * This method takes a file descriptor of a zip file in the Microbase 
   * filesystem. The file is then downloaded if it is not already present in the 
   * local cache. Finally, the file contents are extracted to a directory of 
   * the same name in the shared responder filespace. For example, if your
   * file is named 'foo.zip', then the contents will be extracted to a 
   * directory named 'foo.zip' in the directory returned by
   * <code>getSharedWorkingDirectory</code>.
   * 
   * This method uses the <code>lockFileInSharedDir</code> and 
   * <code>unlockFileInSharedDir</code> methods to ensure that only a single
   * local process can download/write the file at any one time. Therefore, if
   * you have several local processes that all require at the same time, one of
   * the processes will perform the download/extract operation and the others
   * will block until the operation is complete.
   * 
   * If a directory 'foo.zip' already exists, then we make the assumption that
   * the required content already exists and return without performing any
   * download/extraction operation.
   * 
   * @param remoteFile the remote zip file to download and extract.
   * @return the destination directory that the specified file was extracted
   * into.
   * 
   * @throws IOException if something went wrong, for example, if the 
   * remote file could not be found, or if an item 'foo.zip' existed in the
   * shared filespace, but was NOT a directory. In this case, we assume a 
   * naming conflict with another responder.
   */
  static File downloadAndUnzipFileToSharedSpace(
      ClientRuntime runtime, File sharedWorkingDir, MBFile remoteFile) 
      throws IOException
  { 
    /*
     * Although this is technically the filename of the remote file, we will 
     * use the same name as a directory to unzip the file to (since we can
     * assume that the file is a .zip)
     */
    File unzipToDir = new File(sharedWorkingDir, remoteFile.getName());
    
    ILock fileLock = getLockForFileInSharedDir(runtime, remoteFile.getName());
    logger.info("Attempting to lock shared file: "+remoteFile.getName());
    fileLock.lock();
    try {
      logger.info("Obtained lock for shared file: "+remoteFile.getName()
          + ", lock ID: " + fileLock.getId());
      if (unzipToDir.exists() && unzipToDir.isDirectory()) {
        logger.info("Directory "+unzipToDir.getAbsolutePath()+" already exists, "
            + "therefore assuming that the shared filespace is already "
            + "populated with the required (extracted) files.");
        return unzipToDir;
      } else if (!unzipToDir.exists()) {
        logger.info("Downloading and extracting files to: "+unzipToDir);        
        InputStream fileStream  = runtime.getMicrobaseFS().downloadToCache(
          remoteFile, false);
        unzipToDir.mkdirs(); //Create destination directory
        ZipUtils.extractZip(fileStream, unzipToDir);
        return unzipToDir;
      } else {
        logger.info("An object on the filesystem already exists, but is not the "
            + "expected directory: "+unzipToDir.getAbsolutePath()
            +". Not proceeding until you resolve this.");
        throw new ProcessingException("We were expecting "
            +unzipToDir.getAbsolutePath() + " to either not exist, or be a"
            + "directory containing files of interest. Instead we found it "
            + "to exist, but it didn't appear to be a directory. Perhaps a "
            + "naming conflict in the responder shared filespace?");
      }
    }
    catch(Exception e) {
      logger.info("Something went wrong while downloading file: "+remoteFile.toString()
          + ". Attempting to remove the directory: "+unzipToDir.getAbsolutePath());
      DirectoryUtils.deleteDir(unzipToDir);
      throw new IOException("Something went wrong while downloading file: "
          +remoteFile.toString()+". The intended destination was: "
          +unzipToDir.getAbsolutePath()+", which has hopefully now been removed", e);
    }
    finally {
      fileLock.unlock();
    }
  }
  
 protected static void unzipArchive(File zipFile, File destinationDir)
      throws IOException
  {
    FileInputStream is = null;
    try
    {
      is = new FileInputStream(zipFile);
      ZipUtils.extractZip(is, destinationDir);
    }
    catch(Exception e)
    {
      throw new IOException("Failed to unzip file: "+zipFile, e);
    }
    finally
    {
      FileUtils.closeInputStreams(is);
    }
  }
  
}
