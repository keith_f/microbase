/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 01-Dec-2012, 21:27:55
 */

package uk.org.microbase.responder.util;

import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

/**
 * Implementations of this interface are useful to generate remote file paths 
 * and names for result files generated by a responder.
 * 
 * Responders typically generate at last one result file. This file has a
 * particular name and resides in a particular directory on the compute node.
 * When the compute job has completed, these files may be uploaded to a
 * location within the Microbase filesystem for permanent storage.
 * 
 * However, often, we may want the permanent result file to have a different 
 * name and/or location to where it was stored on the compute node.
 * This interface defines several methods that responders can call to obtain
 * the names of the remote bucket, path or filename to use for a given
 * result file.
 * 
 * Different implementations of this interface can be used to rewrite these 
 * strings appropriately for different responders' requirements. You may either
 * write your own responder-specific implementations, or make use of the set of
 * generic implementations provided as part of Microbase utilities artifact.
 * 
 * @author Keith Flanagan
 */
public interface OutputPathReformatter
{
  public String getFormattedBucketName();
  public String getFormattedPath();
  public String getFormattedFilename();
  public MBFile getFormattedRemoteFile();

  public void setRuntime(ClientRuntime runtime);
  public void setProcessInfo(ProcessInfo processInfo);
  public void setResponderInfo(ResponderInfo responderInfo);
  public void setMessage(Message message);
  public void configure(ClientRuntime runtime, ResponderInfo responderInfo,
          ProcessInfo processInfo, Message message);
  
}
