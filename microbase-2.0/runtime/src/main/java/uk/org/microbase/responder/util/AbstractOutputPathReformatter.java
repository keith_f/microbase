  /*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package uk.org.microbase.responder.util;

import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author Keith Flanagan
 */
abstract public class AbstractOutputPathReformatter
    implements OutputPathReformatter
{
  protected ClientRuntime runtime;
  protected ProcessInfo processInfo;
  protected ResponderInfo responderInfo;
  protected Message message;

  public AbstractOutputPathReformatter() {
  }

  public AbstractOutputPathReformatter(
          ClientRuntime runtime, ResponderInfo responderInfo,
          ProcessInfo processInfo, Message message) {
    this.runtime = runtime;
    this.processInfo = processInfo;
    this.responderInfo = responderInfo;
    this.message = message;
  }
  
  @Override
  public void configure(ClientRuntime runtime, ResponderInfo responderInfo,
          ProcessInfo processInfo, Message message) {

    this.runtime = runtime;
    this.processInfo = processInfo;
    this.responderInfo = responderInfo;
    this.message = message;
  }
  
  public ClientRuntime getRuntime() {
    return runtime;
  }

  @Override
  public void setRuntime(ClientRuntime runtime) {
    this.runtime = runtime;
  }

  public ProcessInfo getProcessInfo() {
    return processInfo;
  }

  @Override
  public void setProcessInfo(ProcessInfo processInfo) {
    this.processInfo = processInfo;
  }

  public ResponderInfo getResponderInfo() {
    return responderInfo;
  }

  @Override
  public void setResponderInfo(ResponderInfo responderInfo) {
    this.responderInfo = responderInfo;
  }

  public Message getMessage() {
    return message;
  }

  @Override
  public void setMessage(Message message) {
    this.message = message;
  }

}
