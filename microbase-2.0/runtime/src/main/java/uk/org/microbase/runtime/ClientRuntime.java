/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 11-Nov-2010, 23:06:29
 */

package uk.org.microbase.runtime;

import com.hazelcast.core.HazelcastInstance;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import uk.org.microbase.dist.logging.DistributedLogger;
import uk.org.microbase.dist.processes.DistributedProcessList;
import uk.org.microbase.dist.processes.ExecutionResultLog;
import uk.org.microbase.dist.queues.ResponderMessageQueues;
import uk.org.microbase.dist.responder.MicrobaseRegistration;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.spi.MicrobaseNotification;

/**
 *
 * @author Keith Flanagan
 */
public class ClientRuntime
{
  /*
   * --------------------------------------------------------------------------
   * Environment variables and properties
   * --------------------------------------------------------------------------
   */
//  private String envMicrobaseHome;
//  private String envMicrobaseLog;
//  private String envMicrobaseConfig;
  private String envMicrobaseScratch;
  private String envResponderData;
//  private String envResponderJars;
  
  /**
   * Raw String properties copied from the node's 'global' configuration file
   */
  private final Map<String, String> rawConfigProperties;
  
  /*
   * Either the default classloader, or a custom loader passed to Microbase
   * from a superprocess.
   */
  private ClassLoader classLoader;
  
  
  
  /*
   * --------------------------------------------------------------------------
   * Microbase components
   * --------------------------------------------------------------------------
   */
  //Distributed message log
  private DistributedLogger distLog;
  
  //Distributed process list
  private DistributedProcessList processList;
  private ExecutionResultLog executionResultLog;
  //Responder registration
  private MicrobaseRegistration registration;
  //Notification
  private MicrobaseNotification notification;
  private ResponderMessageQueues messageQueues;
  //Filesystem
  private MicrobaseFS microbaseFS;
  
  /*
   * --------------------------------------------------------------------------
   * Config properties
   * --------------------------------------------------------------------------
   */
  private int maxResponderQueueSize;
  

  /*
   * --------------------------------------------------------------------------
   * Machine-specific properties
   * --------------------------------------------------------------------------
   */
  private NodeInfo nodeInfo;
  private File jobTmpDirectoryRoot;
  private final HazelcastInstance hazelcast;


  public ClientRuntime(ClassLoader classLoader, HazelcastInstance hzInstance)
  {    
    this.classLoader = classLoader;
    this.hazelcast = hzInstance;
    this.rawConfigProperties = new HashMap<>();
  }

  @Override
  public String toString()
  {
    return "ClientRuntime{" + "nodeInfo=" + nodeInfo
        + "JobTmpDirectoryRoot=" + jobTmpDirectoryRoot
        + "microbaseFS=" + microbaseFS + '}';
  }

  public void shutdown()
  {
    //Hazelcast.shutdownAll();
    if (hazelcast != null)
    {
      hazelcast.shutdown();
    }
  }

  public MicrobaseFS getMicrobaseFS()
  {
    return microbaseFS;
  }

  public void setMicrobaseFS(MicrobaseFS microbaseFS)
  {
    this.microbaseFS = microbaseFS;
  }

  public File getJobTmpDirectoryRoot()
  {
    return jobTmpDirectoryRoot;
  }

  public void setJobTmpDirectoryRoot(File jobTmpDirectoryRoot)
  {
    this.jobTmpDirectoryRoot = jobTmpDirectoryRoot;
  }

  public NodeInfo getNodeInfo()
  {
    return nodeInfo;
  }

  public void setNodeInfo(NodeInfo nodeInfo)
  {
    this.nodeInfo = nodeInfo;
  }

  public HazelcastInstance getHazelcast()
  {
    return hazelcast;
  }

  public int getMaxResponderQueueSize()
  {
    return maxResponderQueueSize;
  }

  public void setMaxResponderQueueSize(int maxResponderQueueSize)
  {
    this.maxResponderQueueSize = maxResponderQueueSize;
  }

  public ResponderMessageQueues getMessageQueues()
  {
    return messageQueues;
  }

  public void setMessageQueues(ResponderMessageQueues messageQueues)
  {
    this.messageQueues = messageQueues;
  }

  public MicrobaseNotification getNotification()
  {
    return notification;
  }

  public void setNotification(MicrobaseNotification notification)
  {
    this.notification = notification;
  }

  public MicrobaseRegistration getRegistration()
  {
    return registration;
  }

  public void setRegistration(MicrobaseRegistration registration)
  {
    this.registration = registration;
  }

  public DistributedLogger getDistLog()
  {
    return distLog;
  }

  public void setDistLog(DistributedLogger distLog)
  {
    this.distLog = distLog;
  }

  public DistributedProcessList getProcessList()
  {
    return processList;
  }

  public void setProcessList(DistributedProcessList processList)
  {
    this.processList = processList;
  }

  public ExecutionResultLog getExecutionResultLog()
  {
    return executionResultLog;
  }

  public void setExecutionResultLog(ExecutionResultLog executionResultLog)
  {
    this.executionResultLog = executionResultLog;
  }

  public String getEnvMicrobaseScratch()
  {
    return envMicrobaseScratch;
  }

  public void setEnvMicrobaseScratch(String envMicrobaseScratch)
  {
    this.envMicrobaseScratch = envMicrobaseScratch;
  }

  public String getEnvResponderData()
  {
    return envResponderData;
  }

  public void setEnvResponderData(String envResponderData)
  {
    this.envResponderData = envResponderData;
  }

  public ClassLoader getClassLoader() {
    return classLoader;
  }

  public void setClassLoader(ClassLoader classLoader) {
    this.classLoader = classLoader;
  }

  public Map<String, String> getRawConfigProperties() {
    return rawConfigProperties;
  }

}
