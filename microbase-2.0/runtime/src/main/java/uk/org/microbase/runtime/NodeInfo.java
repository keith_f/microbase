/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-Aug-2010, 15:46:40
 */

package uk.org.microbase.runtime;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Keith Flanagan
 */
public class NodeInfo
    implements Serializable
{
//  private String uid;
  private Date timestamp;
//  private Date lastSeen;

  private String hostname;
  private long jvmTotalRamSize;
  private int cores;
  private String osName;
  private String osArch;
  private String osVersion;
  private String javaVersion;
  private String javaVmVersion;
  private String javaVmVendor;

  public NodeInfo()
  {
    timestamp = new Date(System.currentTimeMillis());
  }

  public String toUniqueString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append(hostname).append("|").
        append(jvmTotalRamSize).append("|").
        append(cores).append("|").
        append(osName).append("|").
        append(osArch).append("|").
        append(osVersion).append("|").
        append(javaVersion).append("|").
        append(javaVmVersion).append("|").
        append(javaVmVendor).append("|");
    return sb.toString();
  }

  @Override
  public String toString()
  {
    return toUniqueString();
  }



  public int getCores()
  {
    return cores;
  }

  public void setCores(int cores)
  {
    this.cores = cores;
  }

  public String getHostname()
  {
    return hostname;
  }

  public void setHostname(String hostname)
  {
    this.hostname = hostname;
  }

  public String getJavaVersion()
  {
    return javaVersion;
  }

  public void setJavaVersion(String javaVersion)
  {
    this.javaVersion = javaVersion;
  }

  public String getJavaVmVendor()
  {
    return javaVmVendor;
  }

  public void setJavaVmVendor(String javaVmVendor)
  {
    this.javaVmVendor = javaVmVendor;
  }

  public String getJavaVmVersion()
  {
    return javaVmVersion;
  }

  public void setJavaVmVersion(String javaVmVersion)
  {
    this.javaVmVersion = javaVmVersion;
  }

  public long getJvmTotalRamSize()
  {
    return jvmTotalRamSize;
  }

  public void setJvmTotalRamSize(long jvmTotalRamSize)
  {
    this.jvmTotalRamSize = jvmTotalRamSize;
  }

  public String getOsArch()
  {
    return osArch;
  }

  public void setOsArch(String osArch)
  {
    this.osArch = osArch;
  }

  public String getOsName()
  {
    return osName;
  }

  public void setOsName(String osName)
  {
    this.osName = osName;
  }

  public String getOsVersion()
  {
    return osVersion;
  }

  public void setOsVersion(String osVersion)
  {
    this.osVersion = osVersion;
  }

  public Date getTimestamp()
  {
    return timestamp;
  }

  public void setTimestamp(Date timestamp)
  {
    this.timestamp = timestamp;
  }

  


}
