/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 12-Nov-2010, 13:13:03
 */

package uk.org.microbase.scratch;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.ILock;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple test of HazelCast locks. The following appear to be true:
 *  - Locks are assigned on a per-thread basis. i.e., if two threads request
 *     the same lock, only one thread will acquire it at once.
 *  - A single thread may acquire the same lock multiple times, but it must
 *     then release that lock the same number of times.
 * @author Keith Flanagan
 */
public class TestHazelcastLocks
{
  public static void main(String[] args)
  {
    ScheduledThreadPoolExecutor exe = new ScheduledThreadPoolExecutor(30);
    long waitTime = 1000;
    String lockName = "foo";
    for (int i=0; i<10; i++)
    {
      Worker worker = new Worker("Thread "+i, waitTime * (i+1), lockName);
      exe.schedule(worker, 0, TimeUnit.MILLISECONDS);
    }
    
  }

  private static class Worker
      implements Runnable
  {
    private final Logger logger =
        Logger.getLogger(Worker.class.getName());
    private String threadName;
    private long waitTime;
    private String lockName;

    public Worker(String threadName, long waitTime, String lockName)
    {
      this.threadName = threadName;
      this.waitTime = waitTime;
      this.lockName = lockName;
    }

    @Override
    public void run()
    {
      logger.info(threadName+"   ---   Acquiring lock ...");
      ILock lock = Hazelcast.getLock(lockName);
      lock.unlock();
      lock.lock();
      lock.lock();
      logger.info(threadName+"   ---   Acquired lock.");
      logger.info(threadName+"   ---   Waiting: "+waitTime);
      try
      {
        Thread.sleep(waitTime);
      }
      catch (InterruptedException ex)
      {
        Logger.getLogger(TestHazelcastLocks.class.getName()).log(Level.SEVERE, null, ex);
      }
      logger.info(threadName+"   ---   Wait done.");
      logger.info(threadName+"   ---   Releasing lock ...");
      ILock lock2 = Hazelcast.getLock(lockName);
      lock2.unlock();
      lock.unlock();
      logger.info(threadName+"   ---   Unlocked");
    }

  }
}
