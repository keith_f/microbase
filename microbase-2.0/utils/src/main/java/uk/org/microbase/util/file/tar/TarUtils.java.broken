/*
 * This program may only be used, distributed and/or modified under the terms 
 * of the license found in LICENSE.TXT in the project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package uk.org.microbase.util.file.tar;

import com.ice.tar.InvalidHeaderException;
import com.ice.tar.TarEntry;
import com.ice.tar.TarInputStream;
import com.ice.tar.TarOutputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import uk.org.microbase.util.file.FileUtils;

/**
 *
 ** @author Sirintra Nakjang
 */
public class TarUtils
{

  private final static int BUFFER_SIZE = 4096;

  private static InputStream getInputStream(String tarFileName) throws Exception
  {
    if (tarFileName.substring(tarFileName.lastIndexOf(".") + 1, tarFileName.lastIndexOf(".") + 3).equalsIgnoreCase("gz"))
    {
      System.out.println("Creating an GZIPInputStream for the file");
      return new GZIPInputStream(new FileInputStream(new File(tarFileName)));
    }
    else
    {
      System.out.println("Creating an InputStream for the file");
      return new FileInputStream(new File(tarFileName));
    }
  }
  
  public static void extractTar(InputStream in, File untarDir) throws IOException
  {
    try
    {
      System.out.println("Reading TarInputStream...)");
      TarInputStream tin = new TarInputStream(in);
      TarEntry tarEntry = tin.getNextEntry();
      if (!untarDir.exists())
      {
        boolean success = untarDir.mkdirs();
        if (!success)
        {
          throw new IOException("Target directory: "+untarDir.getAbsolutePath()+
                  " did not exist, and could not be created.");
        }
      }
      
      while (tarEntry != null)
      {
        File destPath = new File(untarDir, tarEntry.getName());
        System.out.println("Processing " + destPath.getAbsoluteFile());
        if (!tarEntry.isDirectory())
        {
          FileOutputStream fout = new FileOutputStream(destPath);
          tin.copyEntryContents(fout);
          fout.close();
          destPath.setExecutable(true); //FIXME Ugly hack required for microbase job enactment!
        }
        else
        {
          destPath.mkdirs();
        }
        tarEntry = tin.getNextEntry();
      }
      tin.close();
    }
    catch(Throwable e)
    {
      throw new IOException("Failed to untar file: "+e.getMessage()+
          "\nPossible common causes include: incomplete file? " +
          "passing a gzipped file without first unzipping?", e);
    }
  }

  public static void createTar(List<File> fileList, String archiveFileName)
      throws FileNotFoundException, InvalidHeaderException, IOException
  {
    byte[] buffer = new byte[BUFFER_SIZE];
    // Open archive file

    if (fileList == null)
    {
      throw new NullPointerException("fileList was null!");
    }
    
    File archiveFile = new File(archiveFileName);
    FileOutputStream stream = new FileOutputStream(archiveFile);
    TarOutputStream out = new TarOutputStream(stream);

    for (File file : fileList)
    {
      //String filename = (String) fileList.get(i);
      //File file = new File(filename);
      if (file == null || !file.exists() || file.isDirectory())
      {
        continue;
      }
      System.out.println("> Added to the archive...");

      // Add archive entry
      TarEntry tarAdd = new TarEntry(file);
      tarAdd.setModTime(file.lastModified());

      tarAdd.setName(file.getName());
      out.putNextEntry(tarAdd);
      // Write file to archive
      FileInputStream in = new FileInputStream(file);
      //FIXME this will be slooow
      while (true)
      {
        int nRead = in.read(buffer, 0, buffer.length);

        if (nRead <= 0)
        {
          break;
        }
        out.write(buffer, 0, nRead);

      }
      out.closeEntry();
      in.close();

      out.close();

      stream.close();
      System.out.println(archiveFile + "> Tar Archive created successfully.");

    }
  }

  
  public static void createTarFile(Map<String, InputStream> data,
      File outTarFile) throws IOException
  {
    TarOutputStream zos = null;
    try
    {
      zos = new TarOutputStream(
          new BufferedOutputStream(new FileOutputStream(outTarFile)));
      byte[] buffer = new byte[BUFFER_SIZE];

      for (String entryName : data.keySet())
      {
        InputStream in = data.get(entryName);
        if (in == null)
        {
          throw new IOException("Input stream for entry: " + entryName + " was null!");
        }

        zos.putNextEntry(new TarEntry(entryName));

        int read;
        while ((read = in.read(buffer)) != -1)
        {
          zos.write(buffer, 0, read);
        }
        zos.closeEntry();
        in.close();
      }
    }
    finally
    {
      IOException failureOs = FileUtils.closeOutputStreams(zos);
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }
  
  public static void main(String args[]) throws Throwable
  {
    System.out.println("Testing extractTar()");
    File input = new File("/tmp/mb-sharkhunt-1.0.06-linux-ia32.tar.gz");
    File destDir = new File("/tmp/test-tar");
    
    extractTar(new GZIPInputStream(new FileInputStream(input)), destDir);
  }

}
