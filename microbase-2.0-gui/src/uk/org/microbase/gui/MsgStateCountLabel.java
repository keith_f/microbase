/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 11, 2012, 5:03:02 PM
 */
package uk.org.microbase.gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import uk.org.microbase.gui.services.MessageStateCounts;
import uk.org.microbase.gui.services.GlobalState;
import uk.org.microbase.notification.data.Message;

/**
 * For a particular Microbase message state, displays the name of the state and
 * the number of messages in that state. This class is aware of the various
 * Message.State values and displays the appropriate human-readable description.
 * The background colour of the text is set according to the State. 
 * 
 * Each instance of this class displays the number of messages for a particular
 * Message.State. You could have multiple instances of this class in a VBox to
 * show all state counts.
 * 
 * Example:
 *    Processing  [20] 
 *    Successful  [123]
 * 
 * @author Keith Flanagan
 */
public class MsgStateCountLabel
    extends HBox
    implements PropertyChangeListener
{

  private final MessageStateCounts globalMessageCountState;
//  private String responderName;
//  private int count;
  
  private final Message.State state;
  private final IntegerProperty value;
  
  private final Label stateLbl;
  
//  private final StringValue countText;
//  private final StringProperty countText;
//  private final ReadOnlyStringProperty countText;
  private final StackPane stack;
  private final Rectangle countIcon;
  private final Label countTextLbl;

  public MsgStateCountLabel(Message.State state, IntegerProperty value)
  {
    this.state = state;
    this.value = value;
    
//    countText = new SimpleStringProperty();
    setPadding(new Insets(2, 2, 2, 2));
    setSpacing(10);

    //Label for holding responder name
    stateLbl = new Label();
    stateLbl.setAlignment(Pos.CENTER_LEFT);
    
    //Create right-aligned box for numerical count
    stack = new StackPane();
    countIcon = new Rectangle(35.0, 25.0);
    countIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
        new Stop[]
        {
          new Stop(0, Color.web("#4977A3")), 
          new Stop(0.5, Color.web("#B0C6DA")), 
          new Stop(1, Color.web("#9CB6CF")),
        }));
    countIcon.setStroke(Color.web("#D0E6FA"));
    countIcon.setArcHeight(3.5);
    countIcon.setArcWidth(3.5);

    countTextLbl = new Label();
    countTextLbl.setAlignment(Pos.CENTER);
    countTextLbl.setFont(Font.font("Amble Cn", FontWeight.BOLD, 18));
//    countText.setFill(Color.WHITE);
//    countText.setStroke(Color.web("#7080A0"));
    stack.getChildren().addAll(countIcon, countTextLbl);
    
    stack.setAlignment(Pos.CENTER_RIGHT);   // Right-justify nodes in stack 
    HBox.setHgrow(stack, Priority.ALWAYS);  // Give stack any extra space 
    
//    countTextLbl.textProperty().bind(countText);
    
    getChildren().addAll(stateLbl, stack);

    
    setState(state);
    setCount(-1);
    
    globalMessageCountState = GlobalState.instance().globalMessageCountState2;
    globalMessageCountState.addPropertyChangeListener(this);
    
    countTextLbl.textProperty().bind(value.asString());
    
//    value.addListener(new ChangeListener<Number>() {
//      @Override
//      public void changed(ObservableValue<? extends Number> property, 
//          Number oldVal, Number newVal)
//      {
//        System.out.println("*****: prop: "+property+", old: "+oldVal+", new: "+newVal);
//        setCount(newVal.intValue());
//        System.out.println("-----: Handled property changed event correctly!");
//      }
//    });
  }



  protected void setCount(int count)
  {
    if (count < 0)
    {
      countTextLbl.setText("N/A");
    }
    else
    {
      System.out.println("Setting value: "+count);
//      countText.setValue(String.valueOf(count));
      countTextLbl.setText(String.valueOf(count));
    }
    System.out.println("New value set.");
    System.out.println("New label text is: "+ countTextLbl.getText());
    System.out.println("Successfully got new value");
    System.out.println(countTextLbl.getBoundsInParent().getWidth()+", "+countTextLbl.getWidth());
//    System.out.println(stack.getBoundsInParent().getWidth()+", "+stack.getWidth()+", "+stack.getPrefWidth()+", "+stack.getMaxWidth()+", "+stack.getMinWidth());
    countIcon.setWidth(countTextLbl.getBoundsInParent().getWidth() );
  }



  protected void setState(Message.State state)
  {
    switch(state)
    {
      case READY :
        setStyle("-fx-background-color: #81F0EB");  //Turcoise
        stateLbl.setText("Ready");
        break;
      case PROCESSING :
        //Mac OSX text highlight blue
        setStyle("-fx-background-color: #BAD7FD");
        stateLbl.setText("Processing");
        break;
      case BLOCKED_AWAITING_LOCKS :
        setStyle("-fx-background-color: #E4B7FF"); //Purple
        stateLbl.setText("Blocked");
        break;
      case ERROR_FAILED :
        setStyle("-fx-background-color: #EA4540");  // Red
        stateLbl.setText("Error (failed)");
        break;
      case ERROR_TIMEOUT :
        setStyle("-fx-background-color: #F2A03B");  //Orange
        stateLbl.setText("Error (timeout)");
        break;
      case FINISHED_SKIPPED :
        setStyle("-fx-background-color: #FFEA5C");  //Dark yellow
        stateLbl.setText("Finished (skipped)");
        break;
      case FINISHED_SUCCEEDED :
        setStyle("-fx-background-color: #84D45D");  //Dark-ish green
        stateLbl.setText("Finished (succeeded)");
        break;
      default:
        stateLbl.setText("UNKNOWN");
        setStyle("-fx-background-color: #EDEDED");  //Grey
    }
    
  }

  @Override
  public void propertyChange(PropertyChangeEvent pce)
  {
//    System.out.println("received event: "+pce.getPropertyName()+", "+pce.getOldValue()+", "+pce.getNewValue());
//    if (pce.getOldValue().equals(pce.getNewValue()))
//    {
//      return;
//    }
    if (state == Message.State.READY)
    {
      System.out.println("Stting new count: "+globalMessageCountState.getReady());
      setCount(globalMessageCountState.getReady());
      
    }
  }
  
  
}
